package com.example.models;

import java.io.Serializable;

public class FriendRequest implements Serializable{

	private Long requester;
	
	private String requester_name;
	
	private boolean status; //true=yes, false = pending

	public FriendRequest(Long requester, String requester_name, boolean status) {
		super();
		this.requester = requester;
		this.requester_name = requester_name;
		this.status = status;
	}
	
	public FriendRequest(){
		
	}

	public Long getRequester() {
		return requester;
	}

	public void setRequester(Long requester) {
		this.requester = requester;
	}

	public String getRequester_name() {
		return requester_name;
	}

	public void setRequester_name(String requester_name) {
		this.requester_name = requester_name;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
