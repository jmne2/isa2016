package com.example.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RestaurantOrderItem implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Double amount;
	
	@Column(nullable = false)
	private String metrics;
	
	@ManyToOne
	@JoinColumn(name="item_from_restOrder")
	private RestaurantOrder restaurantOrder;

	public RestaurantOrderItem(){
		
	}
	
	public RestaurantOrderItem(String name, Double amount, String metrics) {
		super();
		this.name = name;
		this.amount = amount;
		this.metrics = metrics;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}

	public RestaurantOrder getRestaurantOrder() {
		return restaurantOrder;
	}

	public void setRestaurantOrder(RestaurantOrder restaurantOrder) {
		this.restaurantOrder = restaurantOrder;
	}
	
}
