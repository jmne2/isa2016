package com.example.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InvitationReservation implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Long reservation;
	
	@Column(nullable = false)
	private Long tableOrderId;
	
	@Column(nullable = false)
	private Long invited;
	
	@Column(nullable = false)
	private String status; //"accepted", "cancelled", "pending", "passed"
	
	public InvitationReservation() {
	}

	public InvitationReservation(Long reservation, Long invited, String status) {
		super();
		this.reservation = reservation;
		this.invited = invited;
		this.status = status;
	}

	public Long getReservation() {
		return reservation;
	}

	public void setReservation(Long reservation) {
		this.reservation = reservation;
	}

	public Long getInvited() {
		return invited;
	}

	public void setInvited(Long invited) {
		this.invited = invited;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public Long getTableId() {
		return tableOrderId;
	}

	public void setTableId(Long tableId) {
		this.tableOrderId = tableId;
	}
}
