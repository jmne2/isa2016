package com.example.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class FriendshipRequest implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Long requester;
	
	@Column(nullable = false)
	private Long responser;
	
	@Column(nullable = false)
	private String status; //"pending", "accepted"
	
	public FriendshipRequest(){
		super();
	}

	public FriendshipRequest(Long requester, Long responser, String status) {
		super();
		this.requester = requester;
		this.responser = responser;
		this.status = status;
	}

	public Long getRequester() {
		return requester;
	}

	public void setRequester(Long requester_id) {
		this.requester = requester_id;
	}

	public Long getResponser() {
		return responser;
	}

	public void setResponser(Long responser_id) {
		this.responser = responser_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}
}
