package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;




@Entity
public class Restaurant implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	//ocena
	@Column(nullable = true)
	private double grade;
	
	@Column(nullable = true)
	private Integer numberOfGrades;
	
	@Column(nullable = true)
	private Integer sumOfGrades;
	
	//opis vrste
	@Column(nullable = false)
	private String descr;
	
	//lokacija
	@Column(nullable = true)
	private double locX;
	
	@Column(nullable = true)
	private double locY;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="restaurant")
	private Set<Dish> dishes = new HashSet<Dish>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="restaurant")
	private Set<Drink> drinks = new HashSet<Drink>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restaurant")
	private Set<LunchTable> tables = new HashSet<LunchTable>();
	
	@OneToOne
    @JoinColumn(name = "rest_manager")
	private RestaurantManager restManager;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restaurant")
	private Set<Bartender> bartenders = new HashSet<Bartender>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restaurant")
	private Set<Cook> cooks = new HashSet<Cook>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restaurant")
	private Set<Waiter> waiters = new HashSet<Waiter>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restaurant")
	private List<Bill> bills = new ArrayList<Bill>();
	
	public Restaurant(){
		grade =0.0;
		numberOfGrades = 0;
		sumOfGrades = 0;
	}
	
	public Restaurant(String name, String descr){
		this.name = name;
		this.descr = descr;
	}
	
	public Restaurant(String name, int grade, String descr, double locX, double locY, RestaurantManager restManager) {
		super();
		this.name = name;
		this.grade = grade;
		this.descr = descr;
		this.locX = locX;
		this.locY = locY;
		this.restManager = restManager;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public double getLocX() {
		return locX;
	}

	public void setLocX(double locX) {
		this.locX = locX;
	}

	public double getLocY() {
		return locY;
	}

	public void setLocY(double locY) {
		this.locY = locY;
	}

	@Override
	public String toString() {
		return "Restaurant [id=" + id + ", name=" + name + ", grade=" + grade + ", descr=" + descr + ", locX=" + locX
				+ ", locY=" + locY + "]";
	}

	public Set<Dish> getDishes() {
		return dishes;
	}

	public void setDishes(Set<Dish> dishes) {
		this.dishes = dishes;
	}

	public void addDish(Dish dish){
		this.dishes.add(dish);
		dish.setRestaurant(this);
	}
	
	public void removeDish(Dish dish){
		this.dishes.remove(dish);
	}
	
	public Set<Drink> getDrinks() {
		return drinks;
	}

	public void setDrinks(Set<Drink> drinks) {
		this.drinks = drinks;
	}	
	
	public void addDrink(Drink drink){
		this.drinks.add(drink);
		drink.setRestaurant(this);
	}
	
	public void removeDrink(Drink drink){
		this.drinks.remove(drink);
	}
	
	public Set<LunchTable> getTables() {
		return tables;
	}

	public void setTables(Set<LunchTable> tables) {
		this.tables = tables;
	}

	public void addTable(LunchTable table){
		this.tables.add(table);
		table.setRestaurant(this);
	}
	
	public void removeTable(LunchTable table){
		this.tables.remove(table);
	}
	
	public RestaurantManager getRestManager() {
		return restManager;
	}

	public void setRestManager(RestaurantManager restManager) {
		this.restManager = restManager;
		restManager.setRestaurant(this);
	}

	public Set<Bartender> getBartenders() {
		return bartenders;
	}

	public void setBartenders(Set<Bartender> bartenders) {
		this.bartenders = bartenders;
	}

	public void addBartender(Bartender bartender){
		this.bartenders.add(bartender);
		bartender.setRestaurant(this);
	}
	
	public void removeBartender(Bartender bartender){
		this.bartenders.remove(bartender);
	}
	
	public Set<Cook> getCooks() {
		return cooks;
	}

	public void setCooks(Set<Cook> cooks) {
		this.cooks = cooks;
	}
	
	public void addCook(Cook cook){
		this.cooks.add(cook);
		cook.setRestaurant(this);
	}

	public void removeCook(Cook cook){
		this.cooks.remove(cook);
	}
	
	public Set<Waiter> getWaiters() {
		return waiters;
	}

	public void setWaiters(Set<Waiter> waiters) {
		this.waiters = waiters;
	}

	public void addWaiter(Waiter waiter){
		this.waiters.add(waiter);
		waiter.setRestaurant(this);
	}
	
	public void removeWaiter(Waiter waiter){
		this.waiters.remove(waiter);
	}

	public List<Bill> getBills() {
		return bills;
	}

	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}
	
	public void addBill(Bill bill){
		this.bills.add(bill);
		bill.setRestaurant(this);
	}

	public Integer getNumberOfGrades() {
		return numberOfGrades;
	}

	public void setNumberOfGrades(Integer numberOfGrades) {
		this.numberOfGrades = numberOfGrades;
	}

	public Integer getSumOfGrades() {
		return sumOfGrades;
	}

	public void setSumOfGrades(Integer sumOfGrades) {
		this.sumOfGrades = sumOfGrades;
	}
}
