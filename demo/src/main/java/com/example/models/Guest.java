package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("GUEST")
public class Guest extends User implements Serializable{

	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = true)
	private Integer accountState; //0 - deactivated, 1 - waiting for confirmation, 2 - active
	
	@Column(nullable = true)
	private double locationX;
	
	@Column(nullable = true)
	private double locationY;
	
	//ubaci ref na listu restorana
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="reservator")
	private Set<Reservation> reserved = new HashSet<Reservation>();
	
	public Guest() {
		super();
	}

	public Guest(String firstName, String lastName, String email, String password, Integer accountState,
			Double locationX, Double locationY) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.accountState = accountState;
		this.locationX = locationX;
		this.locationY = locationY;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAccountState() {
		return accountState;
	}

	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}

	public Double getLocationX() {
		return locationX;
	}

	public void setLocationX(Double locationX) {
		this.locationX = locationX;
	}

	public Double getLocationY() {
		return locationY;
	}

	public void setLocationY(Double locationY) {
		this.locationY = locationY;
	}

	public Set<Reservation> getReserved() {
		return reserved;
	}

	public void setReserved(Set<Reservation> reserved) {
		this.reserved = reserved;
	}
	
	public void addReserved(Reservation resr){
		this.reserved.add(resr);
	}

}
