package com.example.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ReservationModel implements Serializable{

	private boolean prePrepared = false;
	
	private String restaurantId;
	
	private ArrayList<String> dishes = new ArrayList<String>();
	
	private ArrayList<String> drinks = new ArrayList<String>();
	
	private ArrayList<String> friends = new ArrayList<String>();
	
	private String reserverId;
	
	private ArrayList<String> tableId;
	
	private String start;
	
	private String end;
	
	private Date date;

	public ReservationModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getDishes() {
		return dishes;
	}

	public void setDishes(ArrayList<String> dishes) {
		this.dishes = dishes;
	}

	public ArrayList<String> getDrinks() {
		return drinks;
	}

	public void setDrinks(ArrayList<String> drinks) {
		this.drinks = drinks;
	}

	public ArrayList<String> getFriends() {
		return friends;
	}

	public void setFriends(ArrayList<String> friends) {
		this.friends = friends;
	}

	public String getReserverId() {
		return reserverId;
	}

	public void setReserverId(String reserverId) {
		this.reserverId = reserverId;
	}

	public ArrayList<String> getTableId() {
		return tableId;
	}

	public void setTableId(ArrayList<String> tableId) {
		this.tableId = tableId;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isPrePrepared() {
		return prePrepared;
	}

	public void setPrePrepared(boolean prePrepared) {
		this.prePrepared = prePrepared;
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}
}
