package com.example.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class OrderForTable implements Serializable{

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="orderForTable_id", unique=true, nullable=false)
	private Long id;
	
	//ukupna cena
	@Column(nullable = false)
	private double totalCount;
	
	//treba li biti spremna porudzbina pri dolasku gosta
	@Column(nullable = false)
	private boolean readyOnArrival;
	
	@Column(nullable = false)
	private Date date;
	
	@Column(nullable = false)
	private Time time;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="order")
	private Set<OrderedFood> foodOrders = new HashSet<OrderedFood>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="order")
	private Set<OrderedDrinks> drinkOrders = new HashSet<OrderedDrinks>();

	@ManyToOne
	@JoinColumn(name="lunch_table_id", referencedColumnName="lunch_table_id")
	private LunchTable lunchTable;
	
	public OrderForTable(){
		totalCount = 0;
	}
	
	public OrderForTable(double totalCount, boolean readyOnArrival, Set<OrderedFood> foodOrders,
			Set<OrderedDrinks> drinkOrders, LunchTable table) {
		super();
		this.totalCount = totalCount;
		this.readyOnArrival = readyOnArrival;
		this.foodOrders = foodOrders;
		this.drinkOrders = drinkOrders;
		this.lunchTable = table;
	}

	public void addOrderDrinks(OrderedDrinks od){
		if (od.getOrder()!=null)
			od.getOrder().getDrinkOrders().remove(od);
		od.setOrder(this);
		getDrinkOrders().add(od);
	}
	
	public void addOrderFood(OrderedFood of){
		if (of.getOrder()!=null)
			of.getOrder().getFoodOrders().remove(of);
		of.setOrder(this);
		getFoodOrders().add(of);
	}
	
	public double getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(double totalCount) {
		this.totalCount = totalCount;
	}

	public boolean isReadyOnArrival() {
		return readyOnArrival;
	}

	public void setReadyOnArrival(boolean readyOnArrival) {
		this.readyOnArrival = readyOnArrival;
	}

	public Set<OrderedFood> getFoodOrders() {
		return foodOrders;
	}

	public void setFoodOrders(Set<OrderedFood> foodOrders) {
		this.foodOrders = foodOrders;
	}

	public Set<OrderedDrinks> getDrinkOrders() {
		return drinkOrders;
	}

	public void setDrinkOrders(Set<OrderedDrinks> drinkOrders) {
		this.drinkOrders = drinkOrders;
	}

	public LunchTable getTable() {
		return lunchTable;
	}

	public void setTable(LunchTable table) {
		this.lunchTable = table;
	}
	
	public Long getId() {
		return id;
	}
	
	public void removeDrink(OrderedDrinks orderedDrinks){
		this.getDrinkOrders().remove(orderedDrinks);
		orderedDrinks.setOrder(null);
	}
	
	public void removeDish(OrderedFood orderedDish){
		this.getFoodOrders().remove(orderedDish);
		orderedDish.setOrder(null);
	}

	
	@Override
	public String toString() {
		return "Order [totalCount=" + totalCount + ", readyOnArrival=" + readyOnArrival + ", foodOrders=" + foodOrders
				+ ", drinkOrders=" + drinkOrders + "]";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public LunchTable getLunchTable() {
		return lunchTable;
	}

	public void setLunchTable(LunchTable lunchTable) {
		this.lunchTable = lunchTable;
	}
	
	
	
}
