package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Bill implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="billl_id", nullable=false)
	private Long id;
	
	@OneToOne
    @JoinColumn(name = "waiter")
	private Waiter waiter;
	
	@Column(nullable = false)
	private double total;
	
	@Column(nullable = false)
	private Date date;

	@ManyToOne(cascade={ALL})
	@JoinColumn(name="restaurant")
	private Restaurant restaurant;
	
	@Column(nullable = false)
	private String dateString;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="billl")
	private Set<OrderedFood> foodOrders = new HashSet<OrderedFood>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.EAGER, mappedBy="billl")
	private Set<OrderedDrinks> drinkOrders = new HashSet<OrderedDrinks>();
	
	@OneToOne
    @JoinColumn(name = "rezervacija")
	private Reservation reservation;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Bill(){}
	

	public Bill(Waiter waiter, double total, Date date, Restaurant restaurant, String dateString,
			Set<OrderedFood> foodOrders, Set<OrderedDrinks> drinkOrders, Reservation reservation) {
		super();
		this.waiter = waiter;
		this.total = total;
		this.date = date;
		this.restaurant = restaurant;
		this.dateString = dateString;
		this.foodOrders = foodOrders;
		this.drinkOrders = drinkOrders;
		this.reservation = reservation;
	}
	

	public Long getId() {
		return id;
	}

	public Waiter getWaiter() {
		return waiter;
	}

	public void setWaiter(Waiter waiter) {
		this.waiter = waiter;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Set<OrderedFood> getFoodOrders() {
		return foodOrders;
	}

	public void setFoodOrders(Set<OrderedFood> foodOrders) {
		this.foodOrders = foodOrders;
		for(OrderedFood of : foodOrders){
			of.setBill(this);
		}
	}

	public void addFoodOrder(OrderedFood foodOrder){
		this.foodOrders.add(foodOrder);
		foodOrder.setBill(this);
	}
	
	public Set<OrderedDrinks> getDrinkOrders() {
		return drinkOrders;
	}

	public void setDrinkOrders(Set<OrderedDrinks> drinkOrders) {
		this.drinkOrders = drinkOrders;
		for(OrderedDrinks od : drinkOrders){
			od.setBill(this);
		}
	}

	public void addDrinkOrder(OrderedDrinks drinkOrder){
		this.drinkOrders.add(drinkOrder);
		drinkOrder.setBill(this);
	}
	
	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
		reservation.setBill(this);
	}


	
}
