package com.example.models;

import java.io.Serializable;

public class Invitation implements Serializable{

	private Long invitation_id;
	
	private Long restaurant_id;
	
	private String restaurant_name;
	
	private Long invited_id;
	
	private Long invited_by;
	
	private String invited_by_name;
	
	private Long table_id;
	
	private String status; //"accepted", "cancelled", "pending", "passed"

	public Invitation() {}
	
	public Invitation(Long restaurant_id, String restaurant_name, Long invited_id, Long invited_by,
			String invited_by_name, Long table_id, String status) {
		super();
		this.restaurant_id = restaurant_id;
		this.restaurant_name = restaurant_name;
		this.invited_id = invited_id;
		this.invited_by = invited_by;
		this.invited_by_name = invited_by_name;
		this.table_id = table_id;
		this.status = status;
	}





	public Long getRestaurant_id() {
		return restaurant_id;
	}

	public void setRestaurant_id(Long restaurant_id) {
		this.restaurant_id = restaurant_id;
	}

	public String getRestaurant_name() {
		return restaurant_name;
	}

	public void setRestaurant_name(String restaurant_name) {
		this.restaurant_name = restaurant_name;
	}

	public Long getInvited_by() {
		return invited_by;
	}

	public void setInvited_by(Long invited_by) {
		this.invited_by = invited_by;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getInvited_id() {
		return invited_id;
	}

	public void setInvited_id(Long invited_id) {
		this.invited_id = invited_id;
	}

	public String getInvited_by_name() {
		return invited_by_name;
	}

	public void setInvited_by_name(String invited_by_name) {
		this.invited_by_name = invited_by_name;
	}

	public Long getTable_id() {
		return table_id;
	}

	public void setTable_id(Long table_id) {
		this.table_id = table_id;
	}

	public Long getInvitation_id() {
		return invitation_id;
	}

	public void setInvitation_id(Long invitation_id) {
		this.invitation_id = invitation_id;
	}
}
