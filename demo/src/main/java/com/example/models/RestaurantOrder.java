package com.example.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class RestaurantOrder implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="restaurantOrder")
	private Set<RestaurantOrderItem> items = new HashSet<RestaurantOrderItem>();
	
	@ManyToOne(cascade={ALL})
    @JoinColumn(name = "order_from_restMan")
	private RestaurantManager restManager;

	@Column(nullable = false)
	private Date expirationDate;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="restOrder")
	private Set<Offer> offers = new HashSet<Offer>();
	
	@Column(nullable = false)
	private String state;
	
	public RestaurantOrder(){
		this.state = "AKTIVAN";
	}
	
	public RestaurantOrder( RestaurantManager restManager, Date expirationDate) {
		super();
		this.restManager = restManager;
		this.expirationDate = expirationDate;
		this.state ="AKTIVAN";
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Set<RestaurantOrderItem> getItems() {
		return items;
	}

	public void setItems(Set<RestaurantOrderItem> items) {
		this.items = items;
		if(items!=null){
			for(RestaurantOrderItem it : items){
				it.setRestaurantOrder(this);
			}
		}
	}

	public void addItem(RestaurantOrderItem item){
		this.items.add(item);
		item.setRestaurantOrder(this);
	}

	public Long getId() {
		return Id;
	}

	public RestaurantManager getRestManager() {
		return restManager;
	}

	public void setRestManager(RestaurantManager restManager) {
		this.restManager = restManager;
	/*	if(!restManager.getRestOrder().getId().equals(this.Id)){
			restManager.setRestOrder(this);
		}*/
		restManager.addRestOrder(this);
	}

	public Set<Offer> getOffers() {
		return offers;
	}

	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}
	
	public void addOffer(Offer o){
		this.offers.add(o);
		o.setRest_order(this);
	}
	public void removeOffer(Offer o){
		this.offers.remove(o);
		o.setRest_order(null);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
