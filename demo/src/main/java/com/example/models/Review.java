package com.example.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Review implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private int foodReview;
	
	@Column(nullable = false)
	private int serviceReview;
	
	@Column(nullable = false)
	private int generalReview;

	//ADD REFERENCE TO VISITOR AND RESTAURANT
	
	public Review() {
	}

	public Review(int foodReview, int serviceReview, int generalReview) {
		super();
		this.foodReview = foodReview;
		this.serviceReview = serviceReview;
		this.generalReview = generalReview;
	}

	public Long getId() {
		return id;
	}


	public int getFoodReview() {
		return foodReview;
	}

	public void setFoodReview(int foodReview) {
		this.foodReview = foodReview;
	}

	public int getServiceReview() {
		return serviceReview;
	}

	public void setServiceReview(int serviceReview) {
		this.serviceReview = serviceReview;
	}

	public int getGeneralReview() {
		return generalReview;
	}

	public void setGeneralReview(int generalReview) {
		this.generalReview = generalReview;
	}
	
	
}
