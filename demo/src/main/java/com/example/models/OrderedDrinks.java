package com.example.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class OrderedDrinks implements Serializable{

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="orderDrinks_id", unique=true, nullable=false)
	private Long id;
	
	//naruceno pice
	@OneToOne
    @JoinColumn(name = "drink_id")
	private Drink drink;
	
	//stanje pripreme
	@Column(nullable = false)
	private String prepState;

	@ManyToOne
	@JoinColumn(name="orderForTable_id", referencedColumnName="orderForTable_id")
	private OrderForTable order;
	
	@ManyToOne
	@JoinColumn(name="billl_id", referencedColumnName="billl_id")
	private Bill billl;
	
	public OrderedDrinks(){
		
	}
	
	public OrderedDrinks(Drink drink, String prepState, OrderForTable order, Bill bill) {
		super();
		this.drink = drink;
		this.prepState = prepState;
		this.order = order;
		this.billl = bill;
	}
	

	public Drink getDrink() {
		return drink;
	}

	public void setDrink(Drink drink) {
		this.drink = drink;
	}

	public String getPrepState() {
		return prepState;
	}

	public void setPrepState(String prepState) {
		this.prepState = prepState;
	}

	public Long getId() {
		return id;
	}

	public OrderForTable getOrder() {
		return order;
	}

	public void setOrder(OrderForTable order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderedDrinks [prepState=" + prepState + "]";
	}

	public Bill getBill() {
		return billl;
	}

	public void setBill(Bill bill) {
		this.billl = bill;
	}


	
	
}
