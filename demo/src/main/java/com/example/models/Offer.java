package com.example.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class Offer implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column(nullable = false)
	private Date start;
	
	@Column(nullable = false)
	private Date end;
	
	@Column(nullable = false)
	private Double price;
	
	//rok isporuke
	@Column(nullable = false)
	private Date deadline;
	
	@Column(nullable = false)
	private Date garanty;
	
	// ODBIJENA / NA CEKANJU / PRIHVACENA
	@Column(nullable = false)
	private String offerState;
	
	@ManyToOne(cascade={ALL})
	@JoinColumn(name="offer_for_rest_order")
	private RestaurantOrder restOrder;
	
	
	@ManyToOne(cascade={ALL})
	@JoinColumn(name="offer_from_supplier")
	private Supplier supplier;
	
	public Offer() {
		this.offerState  = "NA CEKANJU";
	}

	public Long getId() {
		return Id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getGaranty() {
		return garanty;
	}

	public void setGaranty(Date garanty) {
		this.garanty = garanty;
	}
	public String getOfferState() {
		return offerState;
	}

	public void setOfferState(String offerState) {
		this.offerState = offerState;
	}

	public RestaurantOrder getRest_order() {
		return restOrder;
	}

	public void setRest_order(RestaurantOrder restOrder) {
		this.restOrder = restOrder;
	}



}
