package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Dish implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	private String description;
	
	@Column(nullable = false)
	private Double price;
	
	@Column(nullable = true)
	private Double rate;

	@Column(nullable = true)
	private Integer numberOfGrades;
	
	@Column(nullable = true)
	private Integer sumOfGrades;
	
	@Column(nullable = false)
	private String type;
	
	@Column(nullable = true)
	private Integer state;
	
	@OneToOne(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "food")
	private OrderedFood orderedFood;
	
	@ManyToOne(cascade={ALL})
	@JoinColumn(name="dish_from_rest")
	private Restaurant restaurant;



	public Dish(String name, String description, Double price, Double rate, Integer numberOfGrades, String type,
			Integer state, OrderedFood orderedFood, Restaurant restaurant) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.rate = rate;
		this.numberOfGrades = numberOfGrades;
		this.type = type;
		this.state = state;
		this.orderedFood = orderedFood;
		this.restaurant = restaurant;
	}

	public Dish() {
		state = 0;
		numberOfGrades = 0;
		rate = 0.0;
		sumOfGrades = 0;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getRate() {
		return rate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public OrderedFood getOrderedFood() {
		return orderedFood;
	}

	public void setOrderedFood(OrderedFood orderedFood) {
		this.orderedFood = orderedFood;
	}
	
	
	public Integer getNumberOfGrades() {
		return numberOfGrades;
	}

	public void setNumberOfGrades(Integer numberOfGrades) {
		this.numberOfGrades = numberOfGrades;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getSumOfGrades() {
		return sumOfGrades;
	}

	public void setSumOfGrades(Integer sumOfGrades) {
		this.sumOfGrades = sumOfGrades;
	}
}
