package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Drink implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	private String description;
	
	@Column(nullable = false)
	private Double price;
	
	@OneToOne(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "drink")
	private OrderedDrinks orderedDrinks;

	@ManyToOne(cascade={ALL})
	@JoinColumn(name="drink_from_rest")
	private Restaurant restaurant;
	
	public Drink(String name, String description, Double price, OrderedDrinks orderedDrinks, Restaurant restaurant) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.orderedDrinks = orderedDrinks;
		this.restaurant = restaurant;
	}

	public Drink() {
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
}
