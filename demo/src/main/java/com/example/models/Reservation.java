package com.example.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Reservation implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Date date;
	
	@Column(nullable = false)
	private Time start;
	
	@Column(nullable = false)
	private Time end;
	
	@Column(nullable = false)
	private boolean prepareBeforehand;
	
	//OneToMany ref na Table (uz eventualni ref na br stolica)
	
	@Column(nullable = false)
	private String restaurant;
	
	@ManyToOne(cascade={ALL})
	private Guest reservator;
	
	@OneToMany
	private Set<Guest> reservedFor = new HashSet<Guest>();
	
	@OneToOne(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "reservation")
	private Bill bill;

	public Reservation() {
	}

	public Long getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Time getStart() {
		return start;
	}

	public void setStart(Time start) {
		this.start = start;
	}

	public Time getEnd() {
		return end;
	}

	public void setEnd(Time end) {
		this.end = end;
	}

	public String getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(String restaurant) {
		this.restaurant = restaurant;
	}

	public Set<Guest> getReservedFor() {
		return reservedFor;
	}

	public void setReservedFor(Set<Guest> reservedFor) {
		this.reservedFor = reservedFor;
	}

	public Guest getReservator() {
		return reservator;
	}

	public void setReservator(Guest reservator) {
		this.reservator = reservator;
	}

	public boolean isPrepareBeforehand() {
		return prepareBeforehand;
	}

	public void setPrepareBeforehand(boolean prepareBeforehand) {
		this.prepareBeforehand = prepareBeforehand;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
}
