package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("RESTMANAGER")
public class RestaurantManager extends User implements Serializable {

	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false, unique=true)
	private String email;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = true)
	private double clothesSize;
	
	@Column(nullable = true)
	private double shoesSize;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "restManager")
	private List<RestaurantOrder> restOrder;
	
	@OneToOne(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "restManager")
	private Restaurant restaurant;
	
	public RestaurantManager(String firstName, String lastName, String email, String password,
			double clothesSize, double shoesSize) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.clothesSize = clothesSize;
		this.shoesSize = shoesSize;
	}

	public RestaurantManager(){}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public List<RestaurantOrder> getRestOrder() {
		return restOrder;
	}

	public void setRestOrder(List<RestaurantOrder> restOrder) {
		this.restOrder = restOrder;
		if(restOrder!=null){
			for(RestaurantOrder ro : restOrder){
				ro.setRestManager(this);
			}
		}
	}
	
	public void addRestOrder(RestaurantOrder restOrder){
		this.restOrder.add(restOrder);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getClothesSize() {
		return clothesSize;
	}

	public void setClothesSize(double clothesSize) {
		this.clothesSize = clothesSize;
	}

	public double getShoesSize() {
		return shoesSize;
	}

	public void setShoesSize(double shoesSize) {
		this.shoesSize = shoesSize;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	
}
