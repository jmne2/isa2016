package com.example.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class LunchTable implements Serializable{

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="lunch_table_id", unique=true, nullable=false)
	private Long id;
	
	@Column(nullable = false)
	private int tableNum;
	
	@Column(nullable = false)
	private int chairCount;

	@Column(nullable = false)
	private String segment;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="lunchTable")
	private Set<OrderForTable> orders = new HashSet<OrderForTable>();
	
	@ManyToOne(cascade={ALL})
	@JoinColumn(name="table_in_rest")
	private Restaurant restaurant;
	
	public LunchTable(){
		
	}
	
	public LunchTable(int tableNum, int chairCount, Set<OrderForTable> orders, String segment, Restaurant restaurant) {
		super();
		this.tableNum = tableNum;
		this.chairCount = chairCount;
		this.orders = orders;
		this.segment = segment;
		this.restaurant = restaurant;
	}

	public void addOrder(OrderForTable or){
		if(or.getTable()!= null){
			or.getTable().getOrders().remove(or);
		}
		or.setTable(this);
		getOrders().add(or);
	}
	
	public Set<OrderForTable> getOrders() {
		return orders;
	}

	public void setOrders(Set<OrderForTable> orders) {
		this.orders = orders;
	}

	public Long getId() {
		return id;
	}

	public int getTableNum() {
		return tableNum;
	}

	public void setTableNum(int tableNum) {
		this.tableNum = tableNum;
	}

	public int getChairCount() {
		return chairCount;
	}

	public void setChairCount(int chairCount) {
		this.chairCount = chairCount;
	}

	@Override
	public String toString() {
		return "Table [id=" + id + ", tableNum=" + tableNum + ", chairCount=" + chairCount + "]";
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	
}
