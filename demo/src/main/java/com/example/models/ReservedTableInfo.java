package com.example.models;

import java.io.Serializable;
import java.util.Date;

public class ReservedTableInfo implements Serializable {

	private String date;
	
	private int start;
	
	private int end;
	
	private Long table_id;

	public ReservedTableInfo(String date, int start, int end, Long table_id) {
		super();
		this.date = date;
		this.start = start;
		this.end = end;
		this.table_id = table_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public Long getTable_id() {
		return table_id;
	}

	public void setTable_id(Long table_id) {
		this.table_id = table_id;
	}
}
