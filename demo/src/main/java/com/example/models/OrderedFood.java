package com.example.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class OrderedFood implements Serializable{

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="orderFood_id", unique=true, nullable=false)
	private Long id;
	
	//naruceno jelo
	@OneToOne
    @JoinColumn(name = "dish_id")
	private Dish food;
	
	//stanje pripreme
	@Column(nullable = false)
	private String prepState;

	@ManyToOne
	@JoinColumn(name="orderForTable_id", referencedColumnName="orderForTable_id")
	private OrderForTable order;
	
	@ManyToOne
	@JoinColumn(name="billl_id", referencedColumnName="billl_id")
	private Bill billl;
	
	public OrderedFood(){
		
	}
	
	public OrderedFood(Dish food, String prepState, OrderForTable order, Bill bill) {
		super();
		this.food = food;
		this.prepState = prepState;
		this.order = order;
		this.billl = bill;
	}	

	public Dish getFood() {
		return food;
	}

	public void setFood(Dish food) {
		this.food = food;
	}

	public String getPrepState() {
		return prepState;
	}

	public void setPrepState(String prepState) {
		this.prepState = prepState;
	}

	public Long getId() {
		return id;
	}

	public OrderForTable getOrder() {
		return order;
	}

	public void setOrder(OrderForTable order) {
		this.order = order;
	}

	public Bill getBill() {
		return billl;
	}

	public void setBill(Bill bill) {
		this.billl = bill;
	}


	
}
