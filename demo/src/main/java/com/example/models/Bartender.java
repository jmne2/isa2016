package com.example.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("BARTENDER")
public class Bartender extends User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = true)
	private Date birthDate;
	
	@Column(nullable = true)
	private double clothesSize;
	
	@Column(nullable = true)
	private double shoesSize;

	@Column(nullable = true)
	private Date currWeek;
	
	@Column(nullable = true)
	private int currShift;
	
	@Column(nullable = true)
	private Date nextWeek;
	
	@Column(nullable = true)
	private int nextShift;
	
	@ManyToOne(cascade={ALL})
	@JoinColumn(name="restaurant")
	private Restaurant restaurant;
	
	public Bartender() {
	}
	
	public Bartender(String firstName, String lastName, String email, String password, Date birthDate, double clothesSize,
			double shoesSize, Restaurant restaurant) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.birthDate = birthDate;
		this.clothesSize = clothesSize;
		this.shoesSize = shoesSize;
		this.restaurant= restaurant;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public double getClothesSize() {
		return clothesSize;
	}

	public void setClothesSize(double clothesSize) {
		this.clothesSize = clothesSize;
	}

	public double getShoesSize() {
		return shoesSize;
	}

	public void setShoesSize(double shoesSize) {
		this.shoesSize = shoesSize;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Long getId() {
		return id;
	}

	public Date getCurrWeek() {
		return currWeek;
	}

	public void setCurrWeek(Date currWeek) {
		this.currWeek = currWeek;
	}

	public int getCurrShift() {
		return currShift;
	}

	public void setCurrShift(int currShift) {
		this.currShift = currShift;
	}

	public Date getNextWeek() {
		return nextWeek;
	}

	public void setNextWeek(Date nextWeek) {
		this.nextWeek = nextWeek;
	}

	public int getNextShift() {
		return nextShift;
	}

	public void setNextShift(int nextShift) {
		this.nextShift = nextShift;
	}
	
}
