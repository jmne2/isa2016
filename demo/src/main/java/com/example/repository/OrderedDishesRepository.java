package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;
import com.example.models.OrderedFood;

@Repository
public interface OrderedDishesRepository extends CrudRepository<OrderedFood, Long>{
	public List<OrderedFood> findByOrder(OrderForTable orderForTable);
}
