package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.models.Guest;
import com.example.models.Reservation;

public interface ReservationRepository extends CrudRepository<Reservation, Long>{
	
	public List<Reservation> findByReservator(Guest reservator);
	public List<Reservation> findByRestaurant( String name);
}
