package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.Restaurant;
import com.example.models.RestaurantManager;

@Repository
public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {

	public Restaurant findByRestManager(RestaurantManager restManager);
	public List<Restaurant> findByLocXBetweenAndLocYBetween(double locXMin, double locXMax, double locYMin, double locYMax); 
	//findByFromDateBetweenAndToDateBetween(Date departure, Date arrival, Date departure, Date arrival);
	public Restaurant findByName(String name);
}
