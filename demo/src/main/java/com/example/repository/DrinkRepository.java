package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.Drink;
import com.example.models.Restaurant;

@Repository
public interface DrinkRepository extends CrudRepository<Drink, Long>{

	public List<Drink> findByRestaurant(Restaurant restaurant);
	public Drink findByName(String name);
}
