package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.LunchTable;
import com.example.models.Restaurant;

@Repository
public interface LunchTableRepository  extends CrudRepository<LunchTable, Long>{
	public List<LunchTable> findByRestaurant(Restaurant restaurant);
	public LunchTable findByTableNum(int tableNum);
}
