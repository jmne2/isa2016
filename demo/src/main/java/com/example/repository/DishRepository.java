package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.models.Dish;
import com.example.models.Restaurant;

@Repository
public interface DishRepository extends CrudRepository<Dish, Long> {

	public List<Dish> findByRestaurant(Restaurant restaurant);
	public Dish findByName(String name);
	public void delete(Dish dish);
	//@Query("SELECT d.name FROM Dish d WHERE d.name LIKE CONCAT('%',:name,'%')")
	public List<Dish> findByNameContainingIgnoreCase(String name);

}
