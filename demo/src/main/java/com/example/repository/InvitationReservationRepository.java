package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.InvitationReservation;

@Repository
public interface InvitationReservationRepository extends CrudRepository<InvitationReservation, Long>{
	
	public List<InvitationReservation> findByInvitedAndStatusLike(Long invited, String status);
	public InvitationReservation findOne(Long id);
	public void delete(InvitationReservation invitation);
	public InvitationReservation findByTableOrderId(Long id);
	public InvitationReservation findByReservationAndInvited(Long reservation, Long invited);
	public List<InvitationReservation> findByReservation(Long reservation);
}
