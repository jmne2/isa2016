package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.Offer;
import com.example.models.RestaurantOrder;
import com.example.models.Supplier;

@Repository
public interface OfferRepository extends CrudRepository<Offer, Long>{
	
	public List<Offer> findBySupplier(Supplier supplier);
	public List<Offer> findByRestOrder(RestaurantOrder restOrder); 
}
