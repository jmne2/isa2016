package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.FriendshipRequest;

@Repository
public interface FriendshipRequestRepository extends CrudRepository<FriendshipRequest, Long>{

	public List<FriendshipRequest> findByRequesterOrResponser(Long id, Long id2);
	public FriendshipRequest findByRequesterAndResponser(Long id, Long id2);
	public List<FriendshipRequest> findByResponserAndStatusLike(Long id, String status);
	public List<FriendshipRequest> findByRequesterNotOrResponserNot(Long id, Long id2);
}
