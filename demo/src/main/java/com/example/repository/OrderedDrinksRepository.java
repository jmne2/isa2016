package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;

@Repository
public interface OrderedDrinksRepository extends CrudRepository<OrderedDrinks, Long>{
	public List<OrderedDrinks> findByOrder(OrderForTable orderForTable);
}
