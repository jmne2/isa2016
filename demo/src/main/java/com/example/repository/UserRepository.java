package com.example.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.Bartender;
import com.example.models.Cook;
import com.example.models.Guest;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.models.Supplier;
import com.example.models.User;
import com.example.models.Waiter;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	public User findByEmailAndPassword(String email, String password);
	public User findByEmail(String email);
	public Cook findCookByEmail(String email);
	public List<Cook> findCookByRestaurant(Restaurant restaurant);
	public Cook findCookById(Long id);
	public Supplier findSupplierById(Long id);
	public List<Waiter> findWaiterByRestaurant(Restaurant restaurant);
	public Waiter findWaiterByEmail(String email);
	public Waiter findWaiterById(Long id);
	public List<Bartender> findBartenderByRestaurant(Restaurant restaurant);
	public Bartender findBartenderByEmail(String email);
	public Bartender findBartenderById(Long id);
	public RestaurantManager findRestaurantManagerById(Long id);
	public Guest findGuestById(Long id);
	public Guest findGuestByEmail(String email);
	public List<User> findByIdNotIn(Collection<Long> ids);
}
