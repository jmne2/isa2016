package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;

@Repository
public interface RestOrderRepository extends CrudRepository<RestaurantOrder, Long>{
	
	public List<RestaurantOrder> findByRestManager(RestaurantManager restManager);
	public RestaurantOrder findByStateAndRestManager(String state, RestaurantManager restManager);
}
