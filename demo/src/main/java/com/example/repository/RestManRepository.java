package com.example.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.RestaurantManager;
import com.example.models.Supplier;

@Repository
public interface RestManRepository extends CrudRepository<RestaurantManager, Long>{

}
