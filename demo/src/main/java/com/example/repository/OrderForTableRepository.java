package com.example.repository;

import java.sql.Time;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.LunchTable;
import com.example.models.OrderForTable;

@Repository
public interface OrderForTableRepository extends CrudRepository<OrderForTable, Long>{

	public OrderForTable findByLunchTable(LunchTable lunchTable);
	public List<OrderForTable> findByLunchTableAndTime(LunchTable lunchTable, Time time);
}
