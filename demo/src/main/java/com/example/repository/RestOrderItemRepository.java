package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.RestaurantOrder;
import com.example.models.RestaurantOrderItem;

@Repository
public interface RestOrderItemRepository extends CrudRepository<RestaurantOrderItem, Long>{

	public List<RestaurantOrderItem> findByRestaurantOrder(RestaurantOrder restaurantOrder);
	
}
