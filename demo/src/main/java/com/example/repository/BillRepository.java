package com.example.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.models.Bill;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.Waiter;

@Repository
public interface BillRepository extends CrudRepository<Bill, Long> {

	public List<Bill> findByWaiter(Waiter waiter);
	public List<Bill> findByDateBetweenAndRestaurant(Date date1, Date date2, Restaurant restaurant);
	public List<Bill> findByDateAndRestaurant(Date date, Restaurant restaurant);
	public List<Bill> findByDateStringAndRestaurant(String dateString, Restaurant restaurant);
	public Bill findByReservation(Reservation reservation);	
}
