package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;
import com.example.repository.OrderedDrinksRepository;

@Service
@Transactional
public class OrderedDrinksServiceImpl implements OrderedDrinksService {

	@Autowired
	private OrderedDrinksRepository odr;
	
	@Override
	public List<OrderedDrinks> findAll() {
		return (List<OrderedDrinks>) odr.findAll();
	}

	@Override
	public OrderedDrinks saveOrderedDrinks(OrderedDrinks orderedDrinks) {
		return odr.save(orderedDrinks);
	}

	@Override
	public OrderedDrinks findOne(long id) {
		return odr.findOne(id);
	}

	@Override
	public void delete(long id) {
		odr.delete(id);;
	}

	@Override
	public List<OrderedDrinks> findByOrder(OrderForTable orderForTable) {
		return (List<OrderedDrinks>) odr.findByOrder(orderForTable);
	}

	@Override
	public void delete(OrderedDrinks orderedDrinks) {
		odr.delete(orderedDrinks);
	}

}
