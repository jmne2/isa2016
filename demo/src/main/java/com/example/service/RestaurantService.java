package com.example.service;

import java.util.List;

import com.example.models.Restaurant;
import com.example.models.RestaurantManager;

public interface RestaurantService {

	List<Restaurant> findAll();
	Restaurant saveRestaurant(Restaurant restaurant);
	Restaurant findOne(long id);
	void delete(long id);
	Restaurant findByRestManager(RestaurantManager restManager);
	List<Restaurant> findByLocXBetweenAndLocYBetween(double locXMin, double locXMax, double locYMin, double locYMax);
	Restaurant findByName(String name);
}
