package com.example.service;

import java.util.List;

import com.example.models.Dish;
import com.example.models.Restaurant;

public interface DishService {
	List<Dish> findAll();
	Dish saveDish(Dish dish);
	Dish findOne(long id);
	Dish findByName(String name);
	void delete(long id);
	List<Dish> findByRestaurant(Restaurant restaurant);
	void delete(Dish dish);
	List<Dish> findByNameContainingIgnoreCase(String name);
}
