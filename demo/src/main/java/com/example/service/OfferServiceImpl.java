package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Offer;
import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;
import com.example.models.Supplier;
import com.example.repository.OfferRepository;

@Service
@Transactional
public class OfferServiceImpl implements OfferService {

	@Autowired
	private OfferRepository or ;
	
	@Override
	public List<Offer> findAll() {
		return (List<Offer>)or.findAll();
	}

	@Override
	public Offer saveOffer(Offer offer) {
		return or.save(offer);
	}

	@Override
	public Offer findOne(long id) {
		return or.findOne(id);
	}

	@Override
	public void delete(long id) {
		or.delete(id);
	}

	@Override
	public List<Offer> findBySupplier(Supplier supplier) {
		return or.findBySupplier(supplier);
	}

	@Override
	public List<Offer> findByRestOrder(RestaurantOrder restOrder) {
		return or.findByRestOrder(restOrder);
	}

	
}
