package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;
import com.example.repository.RestOrderRepository;

@Service
@Transactional
public class RestOrderServiceImpl implements RestOrderService{
	
	@Autowired
	private RestOrderRepository rr;
	
	@Override
	public List<RestaurantOrder> findAll() {
		return (List<RestaurantOrder>) rr.findAll();
	}

	@Override
	public RestaurantOrder saveRestOrder(RestaurantOrder restOrder) {
		return rr.save(restOrder);
	}

	@Override
	public RestaurantOrder findOne(long id) {
		return rr.findOne(id);
	}

	@Override
	public void delete(RestaurantOrder restOrder) {
		rr.delete(restOrder);
	}

	@Override
	public List<RestaurantOrder> findByRestManager(RestaurantManager restManager) {
		return rr.findByRestManager(restManager);
	}

	@Override
	public RestaurantOrder findByStateAndRestManager(String state, RestaurantManager restManager) {
		return rr.findByStateAndRestManager(state, restManager);
	}
	
}
