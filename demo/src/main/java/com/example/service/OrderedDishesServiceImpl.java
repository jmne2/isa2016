package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.OrderForTable;
import com.example.models.OrderedFood;
import com.example.repository.OrderedDishesRepository;

@Service
@Transactional
public class OrderedDishesServiceImpl implements OrderedDishService{

	@Autowired
	private OrderedDishesRepository odr;
	@Override
	public List<OrderedFood> findAll() {
		return (List<OrderedFood>) odr.findAll();
	}

	@Override
	public OrderedFood saveOrderedFodd(OrderedFood orderedFood) {
		return odr.save(orderedFood);
	}

	@Override
	public OrderedFood findOne(long id) {
		return odr.findOne(id);
	}

	@Override
	public void delete(long id) {
		odr.delete(id);
	}

	@Override
	public List<OrderedFood> findByOrder(OrderForTable orderForTable) {
		return (List<OrderedFood>) odr.findByOrder(orderForTable);
	}

	@Override
	public void delete(OrderedFood orderedFood) {
		odr.delete(orderedFood);
	}

}
