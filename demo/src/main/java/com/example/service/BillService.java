package com.example.service;

import java.util.Date;
import java.util.List;

import com.example.models.Bill;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.Waiter;

public interface BillService {
	List<Bill> findAll();
	Bill saveBill(Bill bill);
	Bill findOne(long id);
	List<Bill> findByWaiter(Waiter waiter);
	void delete(long id);
	void delete(Bill bill);
	List<Bill> findByDateBetweenAndRestaurant(Date date1, Date date2, Restaurant restaurant);
	List<Bill> findByDateAndRestaurant(Date date, Restaurant restaurant);
	List<Bill> findByDateStringAndRestaurant(String dateString, Restaurant restaurant);
	Bill findByReservation(Reservation reservation);
}
