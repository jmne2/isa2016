package com.example.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Bill;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.Waiter;
import com.example.repository.BillRepository;

@Service
@Transactional
public class BillServiceImpl implements BillService{
	@Autowired
	private BillRepository br;
	
	@Override
	public List<Bill> findAll() {
		return (List<Bill>) br.findAll();
	}

	@Override
	public Bill saveBill(Bill bill) {
		return br.save(bill);
	}

	@Override
	public Bill findOne(long id) {
		return br.findOne(id);
	}

	@Override
	public List<Bill> findByWaiter(Waiter waiter) {
		return br.findByWaiter(waiter);
	}

	@Override
	public void delete(long id) {
		br.delete(id);
	}

	@Override
	public void delete(Bill bill) {
		br.delete(bill);
	}

	@Override
	public List<Bill> findByDateBetweenAndRestaurant(Date date1, Date date2, Restaurant restaurant) {
		return br.findByDateBetweenAndRestaurant(date1, date2, restaurant);
	}

	@Override
	public List<Bill> findByDateAndRestaurant(Date date, Restaurant restaurant) {
		return br.findByDateAndRestaurant(date, restaurant);
	}

	@Override
	public List<Bill> findByDateStringAndRestaurant(String dateString, Restaurant restaurant) {
		return br.findByDateStringAndRestaurant(dateString, restaurant);
	}

	@Override
	public Bill findByReservation(Reservation reservation) {
		return br.findByReservation(reservation);
	}

}
