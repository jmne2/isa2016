package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.repository.RestaurantRepository;

@Service
@Transactional
public class RestaurantServiceImpl implements RestaurantService{

	@Autowired
	private RestaurantRepository rr ;

	@Override
	public List<Restaurant> findAll() {
		// TODO Auto-generated method stub
		return (List<Restaurant>) rr.findAll();
	}

	@Override
	public Restaurant saveRestaurant(Restaurant restaurant) {
		// TODO Auto-generated method stub
		return rr.save(restaurant);
	}

	@Override
	public Restaurant findOne(long id) {
		// TODO Auto-generated method stub
		return rr.findOne(id);
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		rr.delete(id);
	}

	@Override
	public Restaurant findByRestManager(RestaurantManager restManager) {
		return rr.findByRestManager(restManager);
	}

	@Override
	public List<Restaurant> findByLocXBetweenAndLocYBetween(double locXMin, double locXMax, double locYMin,
			double locYMax) {
		return rr.findByLocXBetweenAndLocYBetween(locXMin-100, locXMax+100, locYMin-100, locYMax+100);
	}
	public Restaurant findByName(String name) {
		return rr.findByName(name);	}
	
	
	
}
