package com.example.service;

import java.util.List;

import com.example.models.OrderForTable;
import com.example.models.OrderedFood;

public interface OrderedDishService {
	List<OrderedFood> findAll();
	OrderedFood saveOrderedFodd(OrderedFood orderedFood);
	OrderedFood findOne(long id);
	void delete(long id);
	List<OrderedFood> findByOrder(OrderForTable orderForTable);
	void delete(OrderedFood orderedFood);
}
