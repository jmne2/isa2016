package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.models.FriendshipRequest;
import com.example.repository.FriendshipRequestRepository;

@Service
public class FriendshipRequestServiceImpl implements FriendshipRequestService{

	@Autowired
	FriendshipRequestRepository fr;
	
	@Override
	public List<FriendshipRequest> findByRequesterOrResponser(Long id, Long id2) {
		return fr.findByRequesterOrResponser(id, id2);
	}

	@Override
	public FriendshipRequest saveFriendship(FriendshipRequest frs) {
		return fr.save(frs);
	}

	@Override
	public List<FriendshipRequest> findByResponserAndStatusLike(Long id, String status) {
		return fr.findByResponserAndStatusLike(id, status);
	}

	@Override
	public List<FriendshipRequest> findByRequesterNotOrResponserNot(Long id, Long id2) {
		return fr.findByRequesterNotOrResponserNot(id, id2);
	}

	@Override
	public FriendshipRequest findByRequesterAndResponser(Long id, Long id2) {
		return fr.findByRequesterAndResponser(id, id2);
	}

	@Override
	public void delete(FriendshipRequest f) {
		fr.delete(f);
	}
}
