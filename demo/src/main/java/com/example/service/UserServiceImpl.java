package com.example.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Bartender;
import com.example.models.Cook;
import com.example.models.Guest;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.models.Supplier;
import com.example.models.User;
import com.example.models.Waiter;
import com.example.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	
	@Autowired
	private UserRepository ur ;
	
	@Override
	public List<User> findAll() {
		return (List<User>) ur.findAll();
	}

	@Override
	public User saveUser(User user) {
		return ur.save(user);
	}

	@Override
	public User findOne(long id) {
		return ur.findOne(id);
	}

	@Override
	public void delete(User user) {
		ur.delete(user);
	}

	@Override
	public User findByEmailAndPassword(String email, String password) {
		return ur.findByEmailAndPassword(email, password);
	}

	@Override
	public User findByEmail(String email) {
		return ur.findByEmail(email);
	}
	@Override
	public List<Cook> findCookByRestaurant(Restaurant restaurant) {
		return ur.findCookByRestaurant(restaurant);
	}

	@Override
	public Cook findCookById(Long id) {
		return ur.findCookById(id);
	}

	@Override
	public Supplier findSupplierById(Long id) {
		return ur.findSupplierById(id);
	}

	@Override
	public List<Waiter> findWaiterByRestaurant(Restaurant restaurant) {
		return ur.findWaiterByRestaurant(restaurant);
	}

	@Override
	public Waiter findWaiterById(Long id) {
		return ur.findWaiterById(id);
	}

	@Override
	public List<Bartender> findBartenderByRestaurant(Restaurant restaurant) {
		return ur.findBartenderByRestaurant(restaurant);
	}

	@Override
	public Bartender findBartenderById(Long id) {
		return ur.findBartenderById(id);
	}

	@Override
	public RestaurantManager findRestaurantManagerById(Long id) {
		return ur.findRestaurantManagerById(id);
	}

	public Guest findGuestById(Long id) {
		return ur.findGuestById(id);
	}


	@Override
	public Guest findGuestByEmail(String email) {
		return ur.findGuestByEmail(email);
	}

	@Override
	public List<User> findByIdNotIn(Collection<Long> ids) {
		return ur.findByIdNotIn(ids);
	}

	@Override
	public Cook findCookByEmail(String email) {
		return ur.findCookByEmail(email);
	}

	@Override
	public Waiter findWaiterByEmail(String email) {
		return ur.findWaiterByEmail(email);
	}

	@Override
	public Bartender findBartenderByEmail(String email) {
		return ur.findBartenderByEmail(email);
	}

}
