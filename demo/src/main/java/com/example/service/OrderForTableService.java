package com.example.service;

import java.sql.Time;
import java.util.List;

import com.example.models.LunchTable;
import com.example.models.OrderForTable;

public interface OrderForTableService {

	List<OrderForTable> findAll();
	OrderForTable saveOrderForTable(OrderForTable orderForTable);
	OrderForTable findOne(long id);
	OrderForTable findByLunchTable(LunchTable lunchTable);
	void delete(OrderForTable orderForTable);
	List<OrderForTable> findByLunchTableAndTime(LunchTable lunchTable, Time time);
}
