package com.example.service;

import java.util.List;

import com.example.models.FriendshipRequest;


public interface FriendshipRequestService {

	List<FriendshipRequest> findByRequesterOrResponser(Long id, Long id2);
	FriendshipRequest findByRequesterAndResponser(Long id, Long id2);
	FriendshipRequest saveFriendship(FriendshipRequest fr);
	List<FriendshipRequest> findByResponserAndStatusLike(Long id, String status);
	List<FriendshipRequest> findByRequesterNotOrResponserNot(Long id, Long id2);
	void delete(FriendshipRequest f);
}
