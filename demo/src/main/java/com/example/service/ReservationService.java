package com.example.service;

import java.util.List;

import com.example.models.Guest;
import com.example.models.Reservation;

public interface ReservationService {
	List<Reservation> findAll();
	Reservation saveReservation(Reservation reservation);
	Reservation findById(Long id);
	void delete(Long id);
	List<Reservation> findByReservator(Guest reservator);
	List<Reservation> findByRestaurant( String name);
}
