package com.example.service;

import java.sql.Time;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.LunchTable;
import com.example.models.OrderForTable;
import com.example.repository.OrderForTableRepository;

@Service
@Transactional
public class OrderForTableImpl implements OrderForTableService{

	@Autowired	
	private OrderForTableRepository oFTR;
	@Override
	public List<OrderForTable> findAll() {
		return (List<OrderForTable>) oFTR.findAll() ;
	}

	@Override
	public OrderForTable saveOrderForTable(OrderForTable orderForTable) {
		return oFTR.save(orderForTable);
	}

	@Override
	public OrderForTable findOne(long id) {
		return oFTR.findOne(id);
	}

	@Override
	public void delete(OrderForTable orderForTable) {
		oFTR.delete(orderForTable);
	}

	@Override
	public OrderForTable findByLunchTable(LunchTable lunchTable) {
		return oFTR.findByLunchTable(lunchTable);
	}

	@Override
	public List<OrderForTable> findByLunchTableAndTime(LunchTable lunchTable, Time time) {
		return oFTR.findByLunchTableAndTime(lunchTable, time);
	}

}
