package com.example.service;

import java.util.List;

import com.example.models.Drink;
import com.example.models.Restaurant;

public interface DrinkService {
	List<Drink> findAll();
	Drink saveDrink(Drink drink);
	Drink findByName(String name);
	Drink findOne(long id);
	void delete(Drink drink);
	List<Drink> findByRestaurant(Restaurant restaurant);
}
