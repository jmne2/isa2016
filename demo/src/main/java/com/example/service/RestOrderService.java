package com.example.service;

import java.util.List;

import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;

public interface RestOrderService {
	List<RestaurantOrder> findAll();
	RestaurantOrder saveRestOrder(RestaurantOrder restOrder);
	RestaurantOrder findOne(long id);
	void delete(RestaurantOrder restOrder);
	List<RestaurantOrder> findByRestManager(RestaurantManager restManager);
	RestaurantOrder findByStateAndRestManager(String state, RestaurantManager restManager);
}
