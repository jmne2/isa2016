package com.example.service;

import java.util.List;

import com.example.models.Offer;
import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;
import com.example.models.Supplier;

public interface OfferService {

	List<Offer> findAll();
	Offer saveOffer(Offer offer);
	Offer findOne(long id);
	void delete(long id);
	List<Offer> findBySupplier(Supplier supplier);
	List<Offer> findByRestOrder(RestaurantOrder restOrder);
}
