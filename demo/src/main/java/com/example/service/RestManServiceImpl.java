package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.RestaurantManager;
import com.example.repository.RestManRepository;

@Service
@Transactional
public class RestManServiceImpl implements RestManService{

	@Autowired
	private RestManRepository rr ;
	
	
	@Override
	public List<RestaurantManager> findAll() {
		return (List<RestaurantManager>) rr.findAll();
	}

	@Override
	public RestaurantManager saveRestMan(RestaurantManager restMan) {
		return rr.save(restMan);
	}

	@Override
	public RestaurantManager findOne(long id) {
		return rr.findOne(id);
	}

	@Override
	public void delete(long id) {
		rr.delete(id);
	}

}
