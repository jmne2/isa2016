package com.example.service;

import java.util.List;

import com.example.models.RestaurantOrder;
import com.example.models.RestaurantOrderItem;

public interface RestOrderItemService {
	List<RestaurantOrderItem> findAll();
	RestaurantOrderItem saveRestOrderItem(RestaurantOrderItem restOrderItem);
	RestaurantOrderItem findOne(long id);
	void delete(RestaurantOrderItem restOrderItem);
	List<RestaurantOrderItem> findByRestOrder(RestaurantOrder restOrder);
}
