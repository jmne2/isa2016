package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.LunchTable;
import com.example.models.Restaurant;
import com.example.repository.LunchTableRepository;

@Service
@Transactional
public class LunchTableServiceImpl implements LunchTableService{

	@Autowired
	LunchTableRepository lr;
	
	@Override
	public List<LunchTable> findAll() {
		return (List<LunchTable>) lr.findAll();
	}

	@Override
	public LunchTable saveLunchTable(LunchTable lunchTable) {
		return lr.save(lunchTable);
	}

	@Override
	public LunchTable findOne(long id) {
		return lr.findOne(id);
	}

	@Override
	public void delete(LunchTable lunchTable) {
		lr.delete(lunchTable);
	}

	@Override
	public List<LunchTable> findByRestaurant(Restaurant restaurant) {
		return lr.findByRestaurant(restaurant);
	}

	@Override
	public LunchTable findByName(int tableNum) {
		return lr.findByTableNum(tableNum);
	}

}
