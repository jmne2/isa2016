package com.example.service;

import java.util.List;

import com.example.models.LunchTable;
import com.example.models.Restaurant;

public interface LunchTableService {
	List<LunchTable> findAll();
	LunchTable saveLunchTable(LunchTable lunchTable);
	LunchTable findOne(long id);
	LunchTable findByName(int tableNum);
	void delete(LunchTable lunchTable);
	List<LunchTable> findByRestaurant(Restaurant restaurant);
}
