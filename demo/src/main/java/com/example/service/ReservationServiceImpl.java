package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Guest;
import com.example.models.Reservation;
import com.example.repository.ReservationRepository;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService{

	@Autowired
	private ReservationRepository rv;
	
	@Override
	public List<Reservation> findAll() {
		return (List<Reservation>)rv.findAll();
	}

	@Override
	public Reservation saveReservation(Reservation reservation) {
		return (Reservation)rv.save(reservation);
	}

	@Override
	public Reservation findById(Long id) {
		return (Reservation)rv.findOne(id);
	}

	@Override
	public void delete(Long id) {
		rv.delete(id);
	}

	@Override
	public List<Reservation> findByReservator(Guest reservator) {
		return rv.findByReservator(reservator);
	}

	@Override
	public List<Reservation> findByRestaurant(String name) {
		return rv.findByRestaurant(name);
	}

}
