package com.example.service;

import java.util.List;

import com.example.models.RestaurantManager;

public interface RestManService {
	List<RestaurantManager> findAll();
	RestaurantManager saveRestMan(RestaurantManager restMan);
	RestaurantManager findOne(long id);
	void delete(long id);
}
