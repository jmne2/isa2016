package com.example.service;

import java.util.List;

import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;


public interface OrderedDrinksService {
	List<OrderedDrinks> findAll();
	OrderedDrinks saveOrderedDrinks(OrderedDrinks orderedDrinks);
	OrderedDrinks findOne(long id);
	void delete(long id);
	List<OrderedDrinks> findByOrder(OrderForTable orderForTable);
	void delete(OrderedDrinks orderedDrinks);
}
