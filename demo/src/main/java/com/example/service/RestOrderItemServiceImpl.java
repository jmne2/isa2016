package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.RestaurantOrder;
import com.example.models.RestaurantOrderItem;
import com.example.repository.RestOrderItemRepository;

@Service
@Transactional
public class RestOrderItemServiceImpl implements  RestOrderItemService{

	@Autowired
	private RestOrderItemRepository rr;
	
	@Override
	public List<RestaurantOrderItem> findAll() {
		return (List<RestaurantOrderItem>) rr.findAll();
	}

	@Override
	public RestaurantOrderItem saveRestOrderItem(RestaurantOrderItem restOrderItem) {
		return rr.save(restOrderItem);
	}

	@Override
	public RestaurantOrderItem findOne(long id) {
		return rr.findOne(id);
	}

	@Override
	public void delete(RestaurantOrderItem restOrderItem) {
		rr.delete(restOrderItem);
	}

	@Override
	public List<RestaurantOrderItem> findByRestOrder(RestaurantOrder restOrder) {
		return rr.findByRestaurantOrder(restOrder);
	}

}
