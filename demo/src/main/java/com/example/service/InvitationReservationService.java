package com.example.service;

import java.util.List;

import com.example.models.InvitationReservation;

public interface InvitationReservationService {

	InvitationReservation saveInvitationReservation(InvitationReservation invitation);
	List<InvitationReservation> findByInvitedAndStatusLike(Long invited, String status);
	InvitationReservation findOne(Long id);
	void delete(InvitationReservation invitation);
	InvitationReservation findByTableOrderId(Long id);
	InvitationReservation findByReservationAndInvited(Long reservation, Long invited);
	List<InvitationReservation> findByReservation(Long reservation);
}
