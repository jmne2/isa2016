package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.InvitationReservation;
import com.example.repository.InvitationReservationRepository;

@Service
@Transactional
public class InvitationReservationServiceImpl implements InvitationReservationService{

	@Autowired
	InvitationReservationRepository irr;
	
	@Override
	public List<InvitationReservation> findByInvitedAndStatusLike(Long invited, String status) {
		return irr.findByInvitedAndStatusLike(invited, status);
	}

	@Override
	public InvitationReservation saveInvitationReservation(InvitationReservation invitation) {
		return irr.save(invitation);
	}

	@Override
	public InvitationReservation findOne(Long id) {
		return irr.findOne(id);
	}

	@Override
	public void delete(InvitationReservation invitation) {
		irr.delete(invitation.getId());
	}

	@Override
	public InvitationReservation findByTableOrderId(Long id) {
		return irr.findByTableOrderId(id);
	}

	@Override
	public InvitationReservation findByReservationAndInvited(Long reservation, Long invited) {
		return irr.findByReservationAndInvited(reservation, invited);
	}

	@Override
	public List<InvitationReservation> findByReservation(Long reservation) {
		return irr.findByReservation(reservation);
	}

}
