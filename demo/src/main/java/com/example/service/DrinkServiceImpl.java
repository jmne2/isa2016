package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Drink;
import com.example.models.Restaurant;
import com.example.repository.DrinkRepository;

@Service
@Transactional
public class DrinkServiceImpl implements DrinkService{
	@Autowired	
	private DrinkRepository dr;
	
	@Override
	public List<Drink> findAll() {
		return (List<Drink>) dr.findAll();
	}

	@Override
	public Drink saveDrink(Drink drink) {
		return dr.save(drink);
	}

	@Override
	public Drink findOne(long id) {
		return dr.findOne(id);
	}


	@Override
	public List<Drink> findByRestaurant(Restaurant restaurant) {
		return dr.findByRestaurant(restaurant);
	}

	@Override
	public void delete(Drink drink) {
		dr.delete(drink);
	}

	@Override
	public Drink findByName(String name) {
		return dr.findByName(name);
	}
	
}
