package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.models.Dish;
import com.example.models.Restaurant;
import com.example.repository.DishRepository;

@Service
@Transactional
public class DishServiceImpl implements DishService{

	@Autowired	
	private DishRepository dr;
	
	@Override
	public List<Dish> findAll() {
		return (List<Dish>) dr.findAll();
	}

	@Override
	public Dish saveDish(Dish dish) {
		return dr.save(dish);
	}

	@Override
	public Dish findOne(long id) {
		return dr.findOne(id);
	}

	@Override
	public void delete(long id) {
		dr.delete(id);
	}

	@Override
	public List<Dish> findByRestaurant(Restaurant restaurant) {
		return dr.findByRestaurant(restaurant);
	}

	@Override
	public void delete(Dish dish) {
		dr.delete(dish);
	}

	@Override
	public List<Dish> findByNameContainingIgnoreCase(String name) {
		return dr.findByNameContainingIgnoreCase(name);
	}

	@Override
	public Dish findByName(String name) {
		return dr.findByName(name);
	}

}
