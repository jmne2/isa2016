package com.example.service;

import java.util.Collection;
import java.util.List;

import com.example.models.Bartender;
import com.example.models.Cook;
import com.example.models.Guest;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.models.Supplier;
import com.example.models.User;
import com.example.models.Waiter;

public interface UserService {

	List<User> findAll();
	User saveUser(User user);
	User findOne(long id);
	User findByEmailAndPassword(String email, String password);
	User findByEmail(String email);
	Cook findCookByEmail(String email);
	void delete(User user);
	List<Cook> findCookByRestaurant(Restaurant restaurant);
	Cook findCookById(Long id);
	Supplier findSupplierById(Long id);
	List<Waiter> findWaiterByRestaurant(Restaurant restaurant);
	Waiter findWaiterByEmail(String email);
	Waiter findWaiterById(Long id);
	List<Bartender> findBartenderByRestaurant(Restaurant restaurant);
	Bartender findBartenderByEmail(String email);
	Bartender findBartenderById(Long id);
	RestaurantManager findRestaurantManagerById(Long id);
	Guest findGuestById(Long id);
	Guest findGuestByEmail(String email);
	List<User> findByIdNotIn(Collection<Long> ids);
}

