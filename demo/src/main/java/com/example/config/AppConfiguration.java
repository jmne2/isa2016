package com.example.config;

import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
public class AppConfiguration {

/*	@Bean
	 public UserService getUserService(){
		 return new UserServiceImpl();
	 }
*/	
/*	@Bean
    public ViewResolver jspViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
*/	
	@Bean
    ServletRegistrationBean h2servletRegistration(){
        ServletRegistrationBean registrationBean = new ServletRegistrationBean( new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }
	
	 public void configure(WebSecurity web) throws Exception {
         web.ignoring().antMatchers("/**");
    }
	 protected void configure(HttpSecurity http) throws Exception {
	     http.authorizeRequests().antMatchers("/submitEnrollment").permitAll().and().csrf().disable();
	 }
	 
	 @Configuration
	    static class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	        @Override
	        public void configure(WebSecurity web) throws Exception {
	             web.ignoring().antMatchers("/**");
	        }
	    }
	 @Configuration
	 @EnableWebSocketMessageBroker
	 static class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer{

		@Override
		public void registerStompEndpoints(StompEndpointRegistry arg0) {
			System.out.println("===AAAAAAAAAAAAAAAAAAAAAAAAAAAA========");
			arg0.addEndpoint("/stomp").withSockJS();
		}
		
		@Override
	    public void configureMessageBroker(MessageBrokerRegistry registry) {
	       registry.setApplicationDestinationPrefixes("/app");
	       registry.enableSimpleBroker("/topic", "/queue");
	    }
	 }
	 /*
	  * @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean entityManagerFactory =
        new LocalContainerEntityManagerFactoryBean();
    
    entityManagerFactory.setDataSource(dataSource);
    
    // Classpath scanning of @Component, @Service, etc annotated class
    entityManagerFactory.setPackagesToScan(
        env.getProperty("entitymanager.packagesToScan"));
    
    // Vendor adapter
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
    
    // Hibernate properties
    Properties additionalProperties = new Properties();
    additionalProperties.put(
        "hibernate.dialect", 
        env.getProperty("hibernate.dialect"));
    additionalProperties.put(
        "hibernate.show_sql", 
        env.getProperty("hibernate.show_sql"));
    additionalProperties.put(
        "hibernate.hbm2ddl.auto", 
        env.getProperty("hibernate.hbm2ddl.auto"));
    entityManagerFactory.setJpaProperties(additionalProperties);
    
    return entityManagerFactory;
  }
	  * 
	  *  @Bean
  public JpaTransactionManager transactionManager() {
    JpaTransactionManager transactionManager = 
        new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(
        entityManagerFactory.getObject());
    return transactionManager;
  }
  
	  */
/*	 @WebFilter(urlPatterns="/user/loginGuest/*")
	 public class AllowAccessFilter implements Filter {
	     public void doFilter(ServletRequest sRequest, ServletResponse sResponse, FilterChain chain) throws IOException, ServletException {
	         System.out.println("in AllowAccessFilter.doFilter");
	         HttpServletRequest request = (HttpServletRequest)sRequest;
	         HttpServletResponse response = (HttpServletResponse)sResponse;
	         response.setHeader("Access-Control-Allow-Origin", "*");
	         response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT");
	         response.setHeader("Access-Control-Allow-Headers", "Content-Type"); 
	         chain.doFilter(request, response);
	     }

		@Override
		public boolean include(Object arg0) {
			// TODO Auto-generated method stub
			return false;
		}
	 }*/
}
