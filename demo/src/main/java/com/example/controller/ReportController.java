package com.example.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Bill;
import com.example.models.Dish;
import com.example.models.Restaurant;
import com.example.models.Waiter;
import com.example.service.BillService;
import com.example.service.DishService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/reports")
public class ReportController {

	@Autowired
	private RestaurantService rs;
	@Autowired
	private DishService ds;
	@Autowired
	private UserService us;
	@Autowired
	private BillService bs;
	
	
	 @RequestMapping(value="/redirect/", method = RequestMethod.POST)
	 public String redirect( UriComponentsBuilder ucBuilder, HttpSession session){
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", true);
		/* Restaurant r = (Restaurant)session.getAttribute("restaurant");
			Restaurant r2 = rs.findOne(r.getId());
		 Dish d = new Dish();
		 d.setDescription("asdk");
		 d.setName("spageti");
		 d.setPrice((double) 3294);
		 d.setRate(4);
		 d.setRestaurant(r2);
		 d.setType("sdlkaf");
		 Dish d2 = new Dish();
		 d2.setDescription("asdk");
		 d2.setName("spageti bolonjeze");
		 d2.setPrice((double) 151151);
		 d2.setRate(10);
		 d2.setRestaurant(r2);
		 d2.setType("sdlkaf");
		 ds.saveDish(d);
		 ds.saveDish(d2);
		 Waiter w = new Waiter("flsda", "lsaf", "sa@gmail.com", "pass", new Date(), 3,3, r2);
		 Bill b1 = new Bill(w, 400.0, new Date(), r2);
		 Date date = new Date();
		 date.setHours(10);
		 date.setMinutes(20);
		 String dateString = "2017-02-27";
		 b1.setDateString(dateString);
		 Bill b2 = new Bill(w, 300.0, date, r2);
		 b2.setDate(date);
		 b2.setDateString(dateString);
		 us.saveUser(w);
		 bs.saveBill(b1);
		 bs.saveBill(b2);*/
		/* Restaurant r = (Restaurant)session.getAttribute("restaurant");
			Restaurant r2 = rs.findOne(r.getId());
		 Waiter w = us.findWaiterByEmail("sa@gmail.com");
		 Bill b1 = new Bill(w, 700.0, new Date(), r2);
		 Date date = new Date();
		 date.setMonth(1);
		 date.setDate(22);
		 System.out.println("date je " + date.getDate());
		 date.setHours(10);
		 date.setMinutes(20);
		 b1.setDate(date);
		 String dateString = "2017-02-22";
		 b1.setDateString(dateString);
		 us.saveUser(w);
		 bs.saveBill(b1);*/
		 return "Reports.jsp";
	 }
	 
	 @RequestMapping(value="/redirectNazad/", method = RequestMethod.POST)
	 public String redirectNazad( UriComponentsBuilder ucBuilder, HttpSession session){
		 session.setAttribute("restHidden", true);
			session.setAttribute("stoloviHidden", true);
			session.setAttribute("jelovnikHidden", true);
			session.setAttribute("kartaHidden", true);
			session.setAttribute("kuvariHidden", true);
			session.setAttribute("konobariHidden", true);
			session.setAttribute("sankeriHidden", true);
			session.setAttribute("currOrderHidden", true);
			session.setAttribute("newOrderHidden", true);

			session.setAttribute("hiddenJelovnik", true);
			session.setAttribute("hiddenJelovnik2", true);
			session.setAttribute("hiddenKarta", true);
			session.setAttribute("hiddenKarta2", true);
			session.setAttribute("hiddenStolovi", true);
			session.setAttribute("hiddenStolovi2", true);
			session.setAttribute("hiddenKuvari", true);
			session.setAttribute("hiddenKuvari2", true);
			session.setAttribute("hiddenKonobari", true);
			session.setAttribute("hiddenKonobari2", true);
			session.setAttribute("hiddenSankeri", true);
			session.setAttribute("hiddenSankeri2", true);
			session.setAttribute("ponudeHidden", true);
			session.setAttribute("regPonHidden", true);
		 return "RestManager.jsp";
	 }
	 @RequestMapping(value="/restIzvestaj/", method = RequestMethod.GET)
	 public ResponseEntity<Void> restIzvestaj( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("restIzDivHidden", false);
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("selWaiter", null);
		 
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 session.setAttribute("ocenaRest", r2.getGrade());
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 @RequestMapping(value="/jeloIzvestajSet/", method = RequestMethod.GET)
	 public ResponseEntity<Void> jeloIzvestajSet( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("jeloIzDivHidden", false);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("ocenaRest", null);
		 session.setAttribute("dishes1", null);
		 session.setAttribute("selDish", null);
		 session.setAttribute("selWaiter", null);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/konobarIzvestajSet/", method = RequestMethod.GET)
	 public ResponseEntity<Void> konobarIzvestajSet( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", false);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("selWaiter", null);
		 session.setAttribute("ocenaRest", null);
		 List<Waiter> w = us.findWaiterByRestaurant(r2);
		 session.setAttribute("konobari", w);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/grafikSet/", method = RequestMethod.GET)
	 public ResponseEntity<Void> grafikSet( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", false);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("selWaiter", null);
		 session.setAttribute("dishes1", null);
		 session.setAttribute("selDish", null);
		 session.setAttribute("ocenaRest", null);
		 Date d = new Date();
		 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		 String datum = sdf.format(d);	 
		 session.setAttribute("datumDanas", datum);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/prihodiSet/", method = RequestMethod.GET)
	 public ResponseEntity<Void> prihodiSet( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", false);
		 session.setAttribute("prihodiKonDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("ocenaRest", null);
		 session.setAttribute("selWaiter", null);
		 
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/prihodiKonSet/", method = RequestMethod.GET)
	 public ResponseEntity<Void> prihodiKonSet( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", true);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodiKonDivHidden", false);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("selWaiter", null);
		 session.setAttribute("ocenaRest", null);
		 
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 List<Waiter> waiters = us.findWaiterByRestaurant(r2);
		 session.setAttribute("waiters", waiters);
		 
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/prihodiIzvestaj/{d1}/{d2}", method = RequestMethod.GET)
	 public ResponseEntity<Void> prihodiIzvestaj(@PathVariable String d1, @PathVariable String d2, HttpSession session) {
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 Date datum = new Date();
		 Date datum2 = new Date();
		 String[] dates = d1.split("-");
		 datum.setYear(Integer.parseInt(dates[0])-1900);
		 datum.setMonth(Integer.parseInt(dates[1])-1);
		 datum.setDate(Integer.parseInt(dates[2]));
		 dates = d2.split("-");
		 datum2.setYear(Integer.parseInt(dates[0])-1900);
		 datum2.setMonth(Integer.parseInt(dates[1])-1);
		 datum2.setDate(Integer.parseInt(dates[2]));
		 
		 List<Bill> bills = bs.findByDateBetweenAndRestaurant(datum, datum2, r2);
		 Double total = 0.0;
		 for(Bill b : bills){
			 total+= b.getTotal();
		 }
		 session.setAttribute("totalPrihod", total);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/prihodiKonIzvestaj/{d}", method = RequestMethod.GET)
	 public ResponseEntity<Void> prihodiKonIzvestaj(@PathVariable String d, HttpSession session) {
		 Waiter w = us.findWaiterById(Long.parseLong(d));
		 List<Bill>	bills = bs.findByWaiter(w);
		 Double total = 0.0;
		 for(Bill b : bills){
			 total+= b.getTotal();
		 }
		 session.setAttribute("prihodKonobara", total);
		 
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/grafikSet2/", method = RequestMethod.GET)
	 public ResponseEntity<Void> grafikSet2( UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 session.setAttribute("jeloIzDivHidden", true);
		 session.setAttribute("restIzDivHidden", true);
		 session.setAttribute("konobarIzDivHidden", true);
		 session.setAttribute("grafikDivHidden", true);
		 session.setAttribute("grafikDiv2Hidden", false);
		 session.setAttribute("prihodiDivHidden", true);
		 session.setAttribute("prihodKonobara", null);
		 session.setAttribute("totalPrihod", null);
		 session.setAttribute("selWaiter", null);
		 session.setAttribute("dishes1", null);
		 session.setAttribute("ocenaRest", null);
		 session.setAttribute("selDish", null);
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 
		 Date date = new Date();
	    Calendar c = Calendar.getInstance();
	    c.setTime(date);
	    int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
	    c.add(Calendar.DATE, -i - 7);
	    Date start = c.getTime();
	    start.setHours(8);
	    SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
	    String start2 = dt1.format(start);
	    session.setAttribute("startDate", start2);
	    c.add(Calendar.DATE, 6);
	    Date end = c.getTime();
	    end.setHours(21);
	    
	    String end2 = dt1.format(end);
	    session.setAttribute("endDate", end2);
	    
	    Double inc0 = 0.0;
	    Double inc1 = 0.0;
	    Double inc2 = 0.0;
	    Double inc3 = 0.0;
	    Double inc4 = 0.0;
	    Double inc5 = 0.0;
	    Double inc6 = 0.0;
		 List<Bill> bills = bs.findByDateBetweenAndRestaurant(start, end, r2);
		 System.out.println("SIZE OF BILLS" + bills.size());
		 for(Bill b : bills){
			 System.out.println("pare " + b.getTotal());
			 if(b.getDate().getDay()==0){
				 inc0 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==1){
				 inc1 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==2){
				 inc2 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==3){
				 inc3 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==4){
				 inc4 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==5){
				 inc5 += b.getTotal();
			 }
			 else if(b.getDate().getDay()==6){
				 inc6 += b.getTotal();
			 }
			 
		 }

		 session.setAttribute("inc0", inc0);
		 session.setAttribute("inc1", inc1);
		 session.setAttribute("inc2", inc2);
		 session.setAttribute("inc3", inc3);
		 session.setAttribute("inc4", inc4);
		 session.setAttribute("inc5", inc5);
		 session.setAttribute("inc6", inc6);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/jeloForm/", method = RequestMethod.POST)
	    public ResponseEntity<Void> jeloForm(@RequestBody Dish dish,   UriComponentsBuilder ucBuilder, HttpSession session) {
			List<Dish> dishes = ds.findByNameContainingIgnoreCase(dish.getName());
			session.setAttribute("dishes1", dishes);

			return new ResponseEntity<Void>( HttpStatus.OK);
		}
	 
	 @RequestMapping(value = "/jeloForm2/", method = RequestMethod.POST)
	    public ResponseEntity<Void> jeloForm2(@RequestBody Dish dish,   UriComponentsBuilder ucBuilder, HttpSession session) {
			Dish d = ds.findByName(dish.getName());
			session.setAttribute("selDish", d);
			return new ResponseEntity<Void>( HttpStatus.OK);
		}
	 
	 @RequestMapping(value = "/selKonobar/", method = RequestMethod.GET)
		public ResponseEntity<Void> selKonobar(String id, Model model, HttpSession session) {
		 Waiter w = us.findWaiterById(Long.parseLong(id));
		 session.setAttribute("selWaiter", w);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/dayChosen/", method = RequestMethod.GET)
		public ResponseEntity<Void> dayChosen(String date,  HttpSession session) {
		 
		 Restaurant r = (Restaurant)session.getAttribute("restaurant");
		 Restaurant r2 = rs.findOne(r.getId());
		 Date datum = new Date();
		 session.setAttribute("datumDanas", date);
		 String[] dates = date.split("-");
		 datum.setYear(Integer.parseInt(dates[0])-1900);
		 datum.setMonth(Integer.parseInt(dates[1])-1);
		 datum.setDate(Integer.parseInt(dates[2]));

		 List<Bill> bills = bs.findByDateStringAndRestaurant(date, r2);
		 Double inc9 = 0.0;
		 Double inc10 = 0.0;
		 Double inc11= 0.0;
		 Double inc12 = 0.0;
		 Double inc13 = 0.0;
		 Double inc14 = 0.0;
		 Double inc15 = 0.0;
		 Double inc16 = 0.0;
		 Double inc17 = 0.0;
		 Double inc18 = 0.0;
		 Double inc19 = 0.0;
		 Double inc20 = 0.0;
		 Double inc21 = 0.0;
		 for(Bill b : bills){
			 if(b.getDate().getHours()==9){
				 inc9 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==10){
				 inc10 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==11){
				 inc11 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==12){
				 inc12 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==13){
				 inc13 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==14){
				 inc14 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==15){
				 inc15 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==16){
				 inc16 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==17){
				 inc17 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==18){
				 inc18 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==19){
				 inc19 += b.getTotal();
			 }
			 else if(b.getDate().getHours()==20){
				 inc20 += b.getTotal();
			 }
			 
		 }
		 List<Double> inc = new ArrayList<Double>();
		 inc.add(inc9);
		 inc.add(inc10);
		 inc.add(inc11);
		 inc.add(inc12);
		 inc.add(inc13);
		 inc.add(inc14);
		 inc.add(inc15);
		 inc.add(inc16);
		 inc.add(inc17);
		 inc.add(inc18);
		 inc.add(inc19);
		 inc.add(inc20);
		 session.setAttribute("inc", inc);
		 return new ResponseEntity<Void>( HttpStatus.OK);
	 }
}
