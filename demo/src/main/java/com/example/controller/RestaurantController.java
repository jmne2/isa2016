package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/rest")
public class RestaurantController {

	@Autowired
	private RestaurantService rs;
	@Autowired
	private UserService us;
	
	public RestaurantController() {
		
	}
	
	@GetMapping("/allRestaurants")
	public ResponseEntity<List<Restaurant>> listAllRestaurants() {
        List<Restaurant> restaurants = (List<Restaurant>) rs.findAll();
        System.out.println("Listing all restaurants");
        if(restaurants.isEmpty()){
            return new ResponseEntity<List<Restaurant>>(HttpStatus.NO_CONTENT);
        }
        for (int i = 0; i < restaurants.size(); i++){
        	System.out.println("Restoran je :"+restaurants.get(i).getName());
        }
        return new ResponseEntity<List<Restaurant>>(restaurants, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addRestaurant", method = RequestMethod.POST)
    public ResponseEntity<Void> createRestaurant(@RequestBody Restaurant r,   UriComponentsBuilder ucBuilder, HttpSession session) {
		if(r.getName().isEmpty() || r.getDescr().isEmpty())
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		session.setAttribute("savedRest", r);
		if(session.getAttribute("p1")!=null && session.getAttribute("p2")!=null){
		 	Double p1 = Double.parseDouble( (String) session.getAttribute("p1"));
		 	r.setLocX(p1);
		 	Double p2 = Double.parseDouble((String)session.getAttribute("p2"));
		 	r.setLocY(p2);

		 	session.setAttribute("mapaHidden", "true");
		 	
		}
        Restaurant res = (Restaurant) rs.saveRestaurant(r);
        List<Restaurant> rest = rs.findAll();
	 	session.setAttribute("restaurants", rest);
        return new ResponseEntity<Void>( HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/changePosition/{p1}/{p2}/", method = RequestMethod.GET)
	public ResponseEntity<Void> changePosition(@PathVariable String p1, @PathVariable String p2, HttpSession session) {
		session.setAttribute("p1", p1);
		session.setAttribute("p2", p2);
		if(session.getAttribute("savedRest")!=null){
			Restaurant r = (Restaurant) session.getAttribute("savedRest");
			r.setLocX(Double.parseDouble(p1));
			r.setLocY(Double.parseDouble(p2));
			rs.saveRestaurant(r);
			session.setAttribute("mapaHidden", "true");
		 	List<Restaurant> rest = rs.findAll();
		 	session.setAttribute("restaurants", rest);
		}
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/restSet", method = RequestMethod.GET)
	public ResponseEntity<Void> restSet(HttpSession session) {
		session.setAttribute("mapaHidden", "false");
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/menSet", method = RequestMethod.GET)
	public ResponseEntity<Void> menSet(HttpSession session) {
		session.setAttribute("mapaHidden", "true");
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/restMenSet", method = RequestMethod.GET)
	public ResponseEntity<Void> restMenSet(HttpSession session) {
		session.setAttribute("mapaHidden", "true");
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/chooseRest/{name}", method = RequestMethod.GET)
    public ResponseEntity<Void> chooseRest(@PathVariable String name,   UriComponentsBuilder ucBuilder, HttpSession session) {
		System.out.println("USAOOOOO");
		System.out.println("NAME JE " + name);
		session.setAttribute("chosenRest", name);
		Long id = (Long)session.getAttribute("savedRM");
		System.out.println("ID je " + id);
		RestaurantManager rm2 = us.findRestaurantManagerById(id);
		System.out.println("FOUND RM2 " + rm2.getId());
		if(rm2!=null){
			Restaurant rr = rs.findByName(name);
			System.out.println("ID RESTORANA JE : " + rr.getId());
			rm2.setRestaurant(rr);
			rr.setRestManager(rm2);
			us.saveUser(rm2);
			rs.saveRestaurant(rr);
			us.saveUser(rm2);
			session.setAttribute("mapaHidden", "true");
		}
        return new ResponseEntity<Void>( HttpStatus.CREATED);
    }
}
