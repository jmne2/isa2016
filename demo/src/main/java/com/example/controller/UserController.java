package com.example.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Admin;
import com.example.models.Bartender;
import com.example.models.Cook;
import com.example.models.Guest;
import com.example.models.Offer;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;
import com.example.models.Supplier;
import com.example.models.User;
import com.example.models.Waiter;
import com.example.service.DishService;
import com.example.service.OfferService;
import com.example.service.ReservationService;
import com.example.service.RestOrderService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService us;
	@Autowired
	private OfferService os;
	@Autowired
	private RestOrderService ros;
	@Autowired
	private RestaurantService rs;
	@Autowired
	private DishService ds;
	
	@Autowired
	private ReservationService rvs;
	
	private JavaMailSenderImpl sender = new JavaMailSenderImpl();
	
	public UserController() {
		
	}
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = (List<User>) us.findAll();
        System.out.println("Listing all users");
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        for (int i = 0; i < users.size(); i++){
        	System.out.println("User je :"+users.get(i).getFirstName());
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	
	/*@RequestMapping(
			value = "/users/",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<User>> getUsers() {
		 System.out.println("Listing all users");
		logger.info("> getUsers");

		Collection<User> users = (Collection<User>) ur.findAll();

		logger.info("< getUsers");
		return new ResponseEntity<Collection<User>>(users,
				HttpStatus.OK);
	}*/
	
/*	 @RequestMapping(value = "/addWaiter/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createUser(@RequestBody Waiter w,   UriComponentsBuilder ucBuilder) {
		 	System.out.println("Creating Waiter " + w.getFirstName());
		 	w.setPassword("pass");
	        Waiter waiter = (Waiter) us.saveUser(w);
		    System.out.println("Konobar: "+ waiter.toString());
	        return new ResponseEntity<Void>( HttpStatus.CREATED);
	    }
*/
	 @RequestMapping(value = "/addAdmin/", method = RequestMethod.POST)
	    public String createUser(@RequestBody Admin a, UriComponentsBuilder ucBuilder) {
		 	System.out.println("Creating Admin " + a.getFirstName());
		 	a.setPassword("pass");
	        Admin admin = (Admin) us.saveUser(a);
		    System.out.println("Admin: "+ admin.toString());
	        return "CREATED";
	    }
	 
	 @RequestMapping(value = "/addRestMan/", method = RequestMethod.POST)
	    public String createRestMan(@RequestBody RestaurantManager a, UriComponentsBuilder ucBuilder, HttpSession session) {
		 //	String restName = (String) session.getAttribute("chosenRest");
		 	a.setPassword("pass");
		/* 	Restaurant r = null;
		 	if(restName!=null){
		 		r = rs.findByName(restName);
		 		if(r!=null){
		 			a.setRestaurant(r);
		 			r.setRestManager(a);
		 		}
		 	}*/
		 	RestaurantManager rm = (RestaurantManager) us.saveUser(a);
		 	Long id = rm.getId();
		 	System.out.println("SAVED RM JE " + id);
		 	session.setAttribute("savedRM", id);
		 /*   if(r!=null){
		    	rs.saveRestaurant(r);
		    	session.setAttribute("mapaHidden", "true");
		    }*/
	        return "CREATED";
	    }
	 @RequestMapping(value = "/changeSupplier/", method = RequestMethod.POST)
	    public String createSupplier(@RequestBody Supplier s, UriComponentsBuilder ucBuilder, HttpSession session) {
		 long id = (long)session.getAttribute("userId");
		 Supplier sup = us.findSupplierById(id);
		
		 	if(sup!= null){
		 		if(s.getFirstName()!="")
		 			sup.setFirstName(s.getFirstName());
		 		if(s.getLastName()!="")
		 			sup.setLastName(s.getLastName());
		 		if(s.getEmail()!="")
		 			sup.setEmail(s.getEmail());
		 		if(s.getPassword()!="")
		 			sup.setPassword(s.getPassword());
		 		Supplier supp = (Supplier) us.saveUser(sup);
		 		session.setAttribute("user", supp);
		 		return "CREATED";
		 	}
		 	else{
			 	Supplier supp = (Supplier) us.saveUser(s);
			 	System.out.println("Supplier: "+ supp.toString());
			 	session.setAttribute("user", supp);
		        return "CREATED";
		 	}
	 }
	 
	 @RequestMapping(value = "/addGuest/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createGuest(@RequestBody Guest g,   UriComponentsBuilder ucBuilder) {
		 	System.out.println("Creating Guest " + g.getFirstName());
		 	g.setAccountState(1);
	        Guest guest = (Guest) us.saveUser(g);
	        sendConfirmationMail(guest);
		    System.out.println("Gost: "+ guest.getFirstName());
	        return new ResponseEntity<Void>( HttpStatus.CREATED);
	    }
	 @RequestMapping(value = "/changeGuest/", method = RequestMethod.POST)
	    public String changeGuest(@RequestBody Guest g,   UriComponentsBuilder ucBuilder, HttpSession session) {
		 	long id = (long)session.getAttribute("userId");
	        Guest guest = us.findGuestById(id);
	        if(guest!= null){
		 		if(g.getFirstName()!="")
		 			guest.setFirstName(g.getFirstName());
		 		if(g.getLastName()!="")
		 			guest.setLastName(g.getLastName());
		 		if(g.getEmail()!="")
		 			guest.setEmail(g.getEmail());
		 		if(g.getPassword()!="")
		 			guest.setPassword(g.getPassword());
		 		Guest guss = (Guest) us.saveUser(guest);
		 		session.setAttribute("user", guss);
		 		return "CREATED";
		 	}
		 	else{
			 	Guest guss = (Guest) us.saveUser(g);
			 	session.setAttribute("user", guss);
		        return "CREATED";
		 	}
	    }
	@RequestMapping(value = "/changePass/", method = RequestMethod.POST)
	   public ResponseEntity<Void> changePass(@RequestBody String lozinka,   UriComponentsBuilder ucBuilder, HttpSession session) {
	 	System.out.println("Changing Pass " + lozinka);
	 	int index = lozinka.indexOf("=");
	 	String pass = lozinka.substring(index+1);
		User user = (User)session.getAttribute("user");
		User user2 = us.findByEmail(user.getEmail());
		user2.setPassword(pass);
		us.saveUser(user2);
		session.setAttribute("user", user2);
	    return new ResponseEntity<Void>( HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/loginGuest/", method = RequestMethod.POST)
	 public String loginUser(@RequestBody Guest u, UriComponentsBuilder ucBuilder, Model model, HttpSession session){
		 System.out.println("Checking for user " + u.getEmail());
		 System.out.println("x: " + u.getLocationX() + " y: " + u.getLocationY());
		 User user = us.findByEmailAndPassword(u.getEmail(), u.getPassword());
		 if(user == null){
			 return "null";
		 }
		 System.out.println("Found user is " + user.getId() + user.getFirstName());
		 session.setAttribute("userId", user.getId());
		 
		 if(user.getPassword().equals("pass")){
			 session.setAttribute("user", user);
			 return "changePass.jsp";
			 }
		 //ZA GLAVNOG ADMINA
		 
		 if(user.getEmail().equals("admin@gmail.com")){
			 //model.addAttribute("isHeadAdmin", "true");
			 List<Restaurant> restaurants = rs.findAll();
			 session.setAttribute("restaurants", restaurants);
			 session.setAttribute("isHeadAdmin", "true");
			 session.setAttribute("mapaHidden", "true");
			 return "Admin.jsp";
		 } 
		 //ZA OSTALE ADMINE
		 else if(user instanceof Admin){
			 session.setAttribute("isHeadAdmin", "false");
			 List<Restaurant> restaurants = rs.findAll();
			 session.setAttribute("restaurants", restaurants);
			 session.setAttribute("mapaHidden", "true");
			 return "Admin.jsp";
		 }
		 else if(user instanceof Supplier){
			 List<Offer> offers = os.findBySupplier((Supplier)user);
			 
			 session.setAttribute("offers", offers);
			 
			 Collection<RestaurantOrder> orders = ros.findAll();
			 System.out.println("UKUPNO ORDERA : " + orders.size());
			 Collection<RestaurantOrder> currOrders = new ArrayList<RestaurantOrder>();
			 Date d = new Date();
			 for(RestaurantOrder ro : orders){
				 System.out.println("status : " +ro.getState());
				 if(ro.getExpirationDate().after(d) && ro.getState().equals("AKTIVAN")){
					 currOrders.add(ro);
				 }
			 }
			 System.out.println("ORDERS " + currOrders.size());
			 session.setAttribute("user", (Supplier)user);
			 session.setAttribute("currOrders", currOrders);
			 session.setAttribute("offerHidden2", true);
			 session.setAttribute("offerHidden", true);
			 return "Ponudjac.jsp";
		 }
		 else if(user instanceof RestaurantManager){

			 session.setAttribute("user", (RestaurantManager)user);
			 RestaurantManager rm = (RestaurantManager)user;
			 Restaurant rest = rs.findByRestManager(rm);
			 session.setAttribute("restaurant", rest);
			
			rs.saveRestaurant(rest);
			session.setAttribute("restaurant", rest);
			session.setAttribute("hiddenJelovnik", true);
			session.setAttribute("hiddenJelovnik2", true);
			session.setAttribute("hiddenKarta", true);
			session.setAttribute("hiddenKarta2", true);
			session.setAttribute("hiddenStolovi", true);
			session.setAttribute("hiddenStolovi2", true);
			session.setAttribute("hiddenKuvari", true);
			session.setAttribute("hiddenKuvari2", true);
			session.setAttribute("hiddenKonobari", true);
			session.setAttribute("hiddenKonobari2", true);
			session.setAttribute("hiddenSankeri", true);
			session.setAttribute("hiddenSankeri2", true);

			session.setAttribute("restHidden", true);
			session.setAttribute("jelovnikHidden", true);
			session.setAttribute("kartaHidden", true);
			session.setAttribute("stoloviHidden", true);
			session.setAttribute("kuvariHidden", true);
			session.setAttribute("konobariHidden", true);
			session.setAttribute("sankeriHidden", true);
			session.setAttribute("currOrderHidden", true);
			session.setAttribute("newOrderHidden", true);
			session.setAttribute("ponudeHidden", true);
			session.setAttribute("regPonHidden", true);
			 return "RestManager.jsp";
		 }else if(user instanceof Cook){
			 Cook c = us.findCookByEmail(user.getEmail());
			 System.out.println("KUVAR KOJI SE ULOGOVAO JE "+ c.getFirstName());
			 session.setAttribute("LogCook", c);
			 return "Cook.jsp";
		 }
		 else if(user instanceof Bartender){
			 Bartender b = us.findBartenderByEmail(user.getEmail());
			 System.out.println("SANKER KOJI SE ULOGOVAO JE: "+b.getFirstName());
			 session.setAttribute("LogBartender", b);
			 return "Bartender.jsp";
		 }
		 else if(user instanceof Waiter){
			 Waiter w = us.findWaiterByEmail(user.getEmail());
			 System.out.println("KONOBAR KOJI SE ULOGOVAO JE: "+w.getFirstName());
			 session.setAttribute("LogWaiter", w);
			 return "Waiter.jsp";
			
		 }
		 else{
			 Guest g1 = us.findGuestByEmail(u.getEmail());
			 g1.setLocationX(u.getLocationX());
			 g1.setLocationY(u.getLocationY());
			 us.saveUser(g1);
			 g1 = us.findGuestByEmail(u.getEmail());
			 List<Reservation> rvg = rvs.findByReservator(g1);
			 session.setAttribute("user", g1);
			 session.setAttribute("pastResv", rvg);
			 return "/guestMain/fillRestaurants";
		 }
	 }
	 	 
	 @RequestMapping(value = "/activate/", method = RequestMethod.GET)
	 public ModelAndView resolveActivation(@RequestParam("token") String token){
		Long id = Long.parseLong(token);
		System.out.println("dosao gost " + id);
		Guest g = (Guest) us.findOne(id);
		g.setAccountState(2);
		Guest izmenjen = (Guest) us.saveUser(g);
		//System.out.println("Aktiviran gost " + izmenjen.getId() + " " + izmenjen.getFirstName());
		return new ModelAndView("index.jsp");
	 }
	 
	 private void sendConfirmationMail(Guest guest){		
		 sender.setHost("smtp.gmail.com");
		 sender.setPassword("natamicazana");
		 sender.setUsername("isa2016temp@gmail.com");
		 Properties emailProperties = System.getProperties();
	        emailProperties.put("mail.smtp.port", "465");
	        emailProperties.put("mail.smtp.auth", "true");
	        emailProperties.put("mail.smtp.starttls.enable", "true");
	        emailProperties.put("mail.smtp.ssl.enable", "true");
	        
		sender.setJavaMailProperties(emailProperties);
		String link = "http://localhost:8080/user/activate/?token=" + guest.getId();
		MimeMessage message = sender.createMimeMessage();

		try {
			// use the true flag to indicate you need a multipart message
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo(guest.getEmail());
			helper.setSubject("Activation Mail");
			helper.setFrom(new InternetAddress("isa2016temp@gmail.com"));
			// use the true flag to indicate the text included is HTML
			helper.setText("<html><body><p>Dear " + guest.getFirstName() + " " + guest.getLastName() + ", </p>"
					 		+ "<p> to activate your account on our website, please proceed to the following link "
					 		+ "<a href='"+link+"'>link</a></p></body></html>", true);

			sender.send(message);
		} catch (MailException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	 }

}
