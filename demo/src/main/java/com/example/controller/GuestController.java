package com.example.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.models.Dish;
import com.example.models.Drink;
import com.example.models.FriendshipRequest;
import com.example.models.Guest;
import com.example.models.Invitation;
import com.example.models.InvitationReservation;
import com.example.models.LunchTable;
import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;
import com.example.models.OrderedFood;
import com.example.models.Reservation;
import com.example.models.ReservationModel;
import com.example.models.ReservedTableInfo;
import com.example.models.Restaurant;
import com.example.models.User;
import com.example.service.DishService;
import com.example.service.DrinkService;
import com.example.service.FriendshipRequestService;
import com.example.service.InvitationReservationService;
import com.example.service.LunchTableService;
import com.example.service.OrderForTableService;
import com.example.service.OrderedDishService;
import com.example.service.OrderedDrinksService;
import com.example.service.ReservationService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/guestMain")
public class GuestController {
	
	@Autowired
	private UserService us;
	
	@Autowired
	private RestaurantService rr;
	
	@Autowired
	private ReservationService rsv;
	
	@Autowired
	private FriendshipRequestService fr;
	
	@Autowired
	private DishService ds;
	
	@Autowired
	private DrinkService drs;
	
	@Autowired
	private OrderForTableService ofts;
	
	@Autowired
	private OrderedDishService ods;
	
	@Autowired
	private OrderedDrinksService odrs;
	
	@Autowired
	private LunchTableService lts;

	@Autowired
	private InvitationReservationService irs;
	
	@Autowired
	private SimpMessagingTemplate template;
	
	private ArrayList<Invitation> reservationInvitations = new ArrayList<Invitation>();
	
	public GuestController() {
	}
	
	@RequestMapping(value = "/acceptFriend/", method = RequestMethod.POST)
	public ResponseEntity<Void> acceptFriend(@RequestBody String accepted, HttpSession session) {
		Guest g = (Guest) session.getAttribute("user");
		g = us.findGuestByEmail(g.getEmail());
		Long accepted_id = Long.parseLong(accepted);
		FriendshipRequest f = fr.findByRequesterAndResponser(accepted_id, g.getId());
		f.setStatus("accepted");
		fr.saveFriendship(f);
		findFriends(session);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/denyFriend/", method = RequestMethod.POST)
	public ResponseEntity<Void> denyFriend(@RequestBody String denied, HttpSession session) {
		Guest g = (Guest) session.getAttribute("user");
		System.out.println("NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO FRIEEEEEEEEEEEEEEEEEEEEEEND COME BACK");
		g = us.findGuestByEmail(g.getEmail());
		Long denied_id = Long.parseLong(denied);
		FriendshipRequest f = fr.findByRequesterAndResponser(denied_id, g.getId());
		fr.delete(f);
		findFriends(session);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/removeFriend/", method = RequestMethod.POST)
	public ResponseEntity<Void> removeFriend(@RequestBody String toRemove, HttpSession session) {
		Guest g = (Guest) session.getAttribute("user");
		g = us.findGuestByEmail(g.getEmail());
		Guest g2 = us.findGuestByEmail(toRemove);
		FriendshipRequest f = fr.findByRequesterAndResponser(g2.getId(), g.getId());
		if(f == null){
			f = fr.findByRequesterAndResponser(g.getId(), g2.getId());
		}
		fr.delete(f);
		findFriends(session);
		findPeople(session);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPeople/", method = RequestMethod.GET)
	public ResponseEntity<Void> getPeople(HttpSession session) {
		findPeople(session);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getFriends/", method = RequestMethod.GET)
	public ResponseEntity<Void> getFriends(HttpSession session) {
		findFriends(session);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/fillRestaurants")
	public String fillRestaurants (HttpSession session){
		findFriends(session);
		findPeople(session);
		Guest g = (Guest)session.getAttribute("user");
		Guest g1 = us.findGuestByEmail(g.getEmail());
		System.out.println(g1.getEmail() + " " + g1.getId());
		List<Restaurant> rests = rr.findByLocXBetweenAndLocYBetween(g1.getLocationX(), g1.getLocationX(), g1.getLocationY(), g1.getLocationY());
		session.setAttribute("currRests", rests);
		rests = rr.findAll();
		session.setAttribute("allRests", rests);
		List<Reservation> reservations = rsv.findByReservator(g1);
		Date today = new Date();
		ArrayList<Reservation> history = new ArrayList<Reservation>();
		for(Reservation r : reservations){
			if(r.getDate().before(today))
				history.add(r);
		}
		session.setAttribute("pastResv", history);
		return "Guest.jsp";
	}
	
	private void findFriends(HttpSession session){
		Guest g1 = (Guest)session.getAttribute("user");
		g1 = us.findGuestByEmail(g1.getEmail());
		List<FriendshipRequest> flist = fr.findByRequesterOrResponser(g1.getId(), g1.getId());
		ArrayList<Long> toFind = new ArrayList<Long>();
		ArrayList<Guest> list = new ArrayList<Guest>();
		for(FriendshipRequest f : flist){
			if(f.getStatus().equals("accepted")){
				if(f.getRequester().equals(g1.getId()))
					toFind.add(f.getResponser());
				else
					toFind.add(f.getRequester());
			}
		}
		for(Long gId : toFind){
			list.add(us.findGuestById(gId));
		}
		System.out.println(list.size());
		session.setAttribute("friends", list);
	}
	private void findPeople(HttpSession session){
		Guest g1 = (Guest) session.getAttribute("user");
		System.out.println(g1.getId() + " " + g1.getEmail());
		g1 = us.findGuestByEmail(g1.getEmail());
		List<FriendshipRequest> flist = fr.findByRequesterOrResponser(g1.getId(), g1.getId());
		ArrayList<Long> toIgnore = new ArrayList<Long>();
		ArrayList<Guest> toSend = new ArrayList<Guest>();
		for(FriendshipRequest f : flist){
			if(f.getStatus().equals("accepted")){
				if(f.getRequester().equals(g1.getId()))
					toIgnore.add(f.getResponser());
				else
					toIgnore.add(f.getRequester());
			}
		}
		toIgnore.add(g1.getId());
		List<User> list = us.findByIdNotIn(toIgnore);
		for(User u : list){
			Guest guest = us.findGuestById(u.getId());
			if(guest != null)
				toSend.add(guest);
		}
		session.setAttribute("people", toSend);
	}
	
	@RequestMapping(value = "/getRestaurants/", method = RequestMethod.GET)
	public ResponseEntity<Void> getCloseRestaurants( String locX, String locY, HttpSession session) {
		double locx = Double.parseDouble(locX);
		double locy = Double.parseDouble(locY);
		List<Restaurant> rests = rr.findByLocXBetweenAndLocYBetween(locx, locx, locy, locy);
		session.setAttribute("currRests", rests);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllRestaurants/", method = RequestMethod.GET)
	public ResponseEntity<Void> getAllRestaurants(HttpSession session) {
		List<Restaurant> rests = rr.findAll();
		session.setAttribute("allRests", rests);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/startReservation/", method = RequestMethod.POST)
	public String redirectToReservation(@RequestBody String request, HttpSession session) {
		Long id = Long.parseLong(request);
		Restaurant r = rr.findOne(id);
		List<Dish> dishes = ds.findByRestaurant(r);
		List<Drink> drinks = drs.findByRestaurant(r);
		session.setAttribute("restForReserv", r);
		session.setAttribute("dishes", dishes);
		session.setAttribute("drinks", drinks);
		List<LunchTable> lunchTables = lts.findByRestaurant(r);
		ArrayList<LunchTable> pusacki = new ArrayList<LunchTable>();
		ArrayList<LunchTable> nepusacki = new ArrayList<LunchTable>();
		ArrayList<LunchTable> basta = new ArrayList<LunchTable>();
		
		for (LunchTable lunchTable : lunchTables) {
			if(lunchTable.getSegment().contains("Pusacki")){
				pusacki.add(lunchTable);
			}
			else if(lunchTable.getSegment().contains("Nepusacki")){
				nepusacki.add(lunchTable);
			}else if(lunchTable.getSegment().contains("Basta")){
				basta.add(lunchTable);
			}
			
		}
		session.setAttribute("pusackiSegment", pusacki);
		session.setAttribute("nepusackiSegment", nepusacki);
		session.setAttribute("bastaSegment", basta);
		return "Reservation.jsp";
	}
	
	@RequestMapping(value = "/setReservation/", method = RequestMethod.POST)
	public String sendReservation(@RequestBody ReservationModel reserv, HttpSession session){
		
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Guest g = (Guest) session.getAttribute("user");
		Restaurant r = (Restaurant) session.getAttribute("restForReserv");
		g = us.findGuestByEmail(g.getEmail());
		String date = resolveDate(reserv.getDate());
		Date d = new Date();
		try {
			d = fmt.parse(date);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		
		String str = reserv.getStart().substring(0, 2);
		int sati = Integer.parseInt(str);
		Reservation weee = new Reservation();
		weee.setDate(d);
		weee.setStart(new java.sql.Time(sati, 0, 0));
		str = reserv.getEnd().substring(0, 2);
		int satiEnd = Integer.parseInt(str);
		weee.setEnd(new java.sql.Time(satiEnd, 0, 0));
		weee.setPrepareBeforehand(reserv.isPrePrepared());
		weee.setReservator(g);
		weee.setRestaurant(r.getName());
		OrderForTable oft = new OrderForTable();
		oft.setDate(d);
		oft.setTime(new java.sql.Time(sati, 0, 0));
		oft.setReadyOnArrival(reserv.isPrePrepared());
		oft = ofts.saveOrderForTable(oft);
		if(reserv.getDishes().size() != 0){
			ArrayList<String> dishes = reserv.getDishes();
			for(String dishId : dishes){
				Long id = Long.parseLong(dishId);
				Dish jelo = ds.findOne(id);
				OrderedFood of = new OrderedFood();
				of.setFood(jelo);
				of.setPrepState("poruceno");
				of = ods.saveOrderedFodd(of);
				oft.addOrderFood(of);
				double suma = oft.getTotalCount();
				suma += jelo.getPrice();
				oft.setTotalCount(suma);
				oft = ofts.saveOrderForTable(oft);
			}
		}
		if(reserv.getDrinks().size() != 0){
			ArrayList<String> drinks = reserv.getDrinks();
			for(String drinkId : drinks){
				Long id = Long.parseLong(drinkId);
				Drink pice = drs.findOne(id);
				OrderedDrinks od = new OrderedDrinks();
				od.setDrink(pice);
				od.setPrepState("poruceno");
				od = odrs.saveOrderedDrinks(od);
				oft.addOrderDrinks(od);
				double suma = oft.getTotalCount();
				suma += pice.getPrice();
				oft.setTotalCount(suma);
				oft = ofts.saveOrderForTable(oft);
			}
		}
		oft = ofts.saveOrderForTable(oft);
		ArrayList<String> tables = reserv.getTableId();
		int gdeSamStala = -1;
		for(int i = 0; i < tables.size(); i++){
			Long id = Long.parseLong(tables.get(i));
			LunchTable table = lts.findOne(id);
			OrderForTable oft2 = oft;
			if(i == 0){
				table.addOrder(oft2);
				table = lts.saveLunchTable(table);
				oft2 = ofts.saveOrderForTable(oft2);
			}
			else{
				oft2 = new OrderForTable();
				oft2.setDate(d);
				oft2.setTime(new java.sql.Time(sati, 0, 0));
				oft2.setReadyOnArrival(reserv.isPrePrepared());
				oft2 = ofts.saveOrderForTable(oft2);
				table.addOrder(oft2);
			}
			weee = rsv.saveReservation(weee);
			if(reserv.getFriends().size() != 0){
				ArrayList<String> friends = reserv.getFriends();
				for(int j = 0; j < friends.size(); j++){
					if(gdeSamStala != -1)
						j = gdeSamStala;
					if(j < table.getChairCount()-1){
						Guest friend = us.findGuestByEmail(friends.get(j));
						InvitationReservation inv = irs.findByReservationAndInvited(weee.getId(), friend.getId());
						if(inv == null){
							InvitationReservation invitation = new InvitationReservation();
							invitation.setReservation(weee.getId());
							invitation.setTableId(oft2.getId());
							invitation.setInvited(friend.getId());
							invitation.setStatus("pending");
							invitation = irs.saveInvitationReservation(invitation);
							sendNotification(friend.getEmail(), oft2, r);
						}
					} else {
						System.out.println("Siftujemo se na drugi sto!!!!");
						gdeSamStala = j;
						break;
					}
				} // for za friends
			} //if za friends
			
			InvitationReservation inv = irs.findByReservationAndInvited(weee.getId(), g.getId());
			if(inv == null){
				InvitationReservation invitation = new InvitationReservation();
				invitation.setReservation(weee.getId());
				invitation.setTableId(oft.getId());
				invitation.setInvited(g.getId());
				invitation.setStatus("accepted");
				invitation = irs.saveInvitationReservation(invitation);
			}
			ReservedTableInfo message = new ReservedTableInfo(resolveDate(weee.getDate()), sati, satiEnd, table.getId());
			template.convertAndSend("/topic/" + r.getId() + "/chairs", message);
		}
			return "/guestMain/fillRestaurants";
	}
	
	private void sendNotification(String friendMejl, OrderForTable oft, Restaurant r){
		reservationInvitations.clear();
		Guest friend = us.findGuestByEmail(friendMejl);
		List<InvitationReservation>lista = irs.findByInvitedAndStatusLike(friend.getId(), "pending");
		for(InvitationReservation it : lista){
			Reservation reservation = rsv.findById(it.getReservation());
			Guest reservator = reservation.getReservator();
			Invitation inv = new Invitation();
			inv.setInvitation_id(it.getId());
			inv.setTable_id(oft.getId());
			inv.setInvited_by(reservator.getId());
			inv.setInvited_by_name(reservator.getFirstName() + " " + reservator.getLastName());
			inv.setRestaurant_name(reservation.getRestaurant());
			inv.setRestaurant_id(r.getId());
			inv.setInvited_id(friend.getId());
			reservationInvitations.add(inv);
		}
		template.convertAndSend("/topic/"+friendMejl+"/pendingInvitations", reservationInvitations);
	}
	
	private String resolveDate(Date d){
		String date = d.toString();
		String[] parts = date.split(" ");
		String month = parts[1];
		String date2 = "";
		date2 += date.substring(date.length()-5);
		date2 += "-";
		switch (month) {
			case "Jan": date2 += "01"; break;
			case "Feb": date2 += "02"; break;
			case "Mar": date2 += "03"; break;
			case "Apr": date2 += "04"; break;
			case "May": date2 += "05"; break;
			case "Jun": date2 += "06"; break;
			case "Jul": date2 += "07"; break;
			case "Aug": date2 += "08"; break;
			case "Sep": date2 += "09"; break;
			case "Oct": date2 += "10"; break;
			case "Nov": date2 += "11"; break;
			case "Dec": date2 += "12"; break;
		}
		date2 += "-";
		date2 += parts[2];
		return date2;
	}

	
	@RequestMapping(value = "/denyInvitation/", method = RequestMethod.POST)
	public ResponseEntity<Void> cancelInvitation(@RequestBody String invId, HttpSession session) {
		Long id = Long.parseLong(invId);
		InvitationReservation res = irs.findOne(id);
		res.setStatus("cancelled");
		irs.saveInvitationReservation(res);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/acceptInvitation/", method = RequestMethod.POST)
	public String respondToInvitation(@RequestBody ReservationModel invId, HttpSession session) {
		System.out.println("ACEPTING DING DING DING DIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIING");
		System.out.println("invId size je: " + invId.getFriends().size());
		Long id = Long.parseLong(invId.getFriends().get(0));
		InvitationReservation res = irs.findOne(id);
		session.setAttribute("acceptedInvitation", res);
		Reservation r = rsv.findById(res.getReservation());
		session.setAttribute("restaurant_name", r.getRestaurant());
		id = Long.parseLong(invId.getFriends().get(1));
		Restaurant restoran = rr.findOne(id);
		List<Dish> dishes = ds.findByRestaurant(restoran);
		List<Drink> drinks = drs.findByRestaurant(restoran);
		session.setAttribute("restForReserv", r);
		session.setAttribute("dishes", dishes);
		session.setAttribute("drinks", drinks);
		return "friendReservation.jsp";
	}
	
	@RequestMapping(value = "/setFriendReservation/", method = RequestMethod.POST)
	public String sendFriendReservation(@RequestBody ReservationModel reserv, HttpSession session){
		
		Guest g = (Guest)session.getAttribute("user");
		InvitationReservation inv = (InvitationReservation)session.getAttribute("acceptedInvitation");
		OrderForTable oft = ofts.findOne(inv.getTableId());
		g = us.findGuestByEmail(g.getEmail());
		Reservation weee = rsv.findById(inv.getReservation());;
		if(reserv.getDishes().size() != 0){
			ArrayList<String> dishes = reserv.getDishes();
			for(String dishId : dishes){
				Long id = Long.parseLong(dishId);
				Dish jelo = ds.findOne(id);
				OrderedFood of = new OrderedFood();
				of.setFood(jelo);
				of.setPrepState("poruceno");
				of = ods.saveOrderedFodd(of);
				oft.addOrderFood(of);
				double suma = oft.getTotalCount();
				suma += jelo.getPrice();
				oft.setTotalCount(suma);
				oft = ofts.saveOrderForTable(oft);
			}
		}
		if(reserv.getDrinks().size() != 0){
			ArrayList<String> drinks = reserv.getDrinks();
			for(String drinkId : drinks){
				Long id = Long.parseLong(drinkId);
				Drink pice = drs.findOne(id);
				OrderedDrinks od = new OrderedDrinks();
				od.setDrink(pice);
				od.setPrepState("poruceno");
				od = odrs.saveOrderedDrinks(od);
				oft.addOrderDrinks(od);
				double suma = oft.getTotalCount();
				suma += pice.getPrice();
				oft.setTotalCount(suma);
				oft = ofts.saveOrderForTable(oft);
			}
		}
		oft = ofts.saveOrderForTable(oft);
		weee = rsv.saveReservation(weee);
		inv.setStatus("accepted");
		irs.saveInvitationReservation(inv);
		return "/guestMain/fillRestaurants";
	}
	
	@RequestMapping(value = "/cancelReservation/", method = RequestMethod.POST)
	public ResponseEntity<Void> cancelReservation(@RequestBody String reservationId, HttpSession session) {
		Long id = Long.parseLong(reservationId);
		//Reservation r = rsv.findById(id);
		//TODO: DODAJ BRISANJE IZ DRUGIH TABELA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//TODO: I'M SIRIUS!!!!!!
		rsv.delete(id);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/testFriendsReservation/", method = RequestMethod.POST)
	public ResponseEntity<Void> sendFriendList(@RequestBody List<String> toSend, HttpSession session) {
		System.out.println(toSend.size());
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
}
