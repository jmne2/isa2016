package com.example.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Bartender;
import com.example.models.Cook;
import com.example.models.Dish;
import com.example.models.Drink;
import com.example.models.LunchTable;
import com.example.models.Offer;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.models.RestaurantOrder;
import com.example.models.RestaurantOrderItem;
import com.example.models.Supplier;
import com.example.models.User;
import com.example.models.Waiter;
import com.example.service.DishService;
import com.example.service.DrinkService;
import com.example.service.LunchTableService;
import com.example.service.OfferService;
import com.example.service.RestManService;
import com.example.service.RestOrderItemService;
import com.example.service.RestOrderService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/restMan")
public class RestManController {

	@Autowired
	private UserService us;
	@Autowired
	private RestaurantService rs;
	@Autowired
	private RestManService rms;
	@Autowired
	private DishService ds;
	@Autowired
	private DrinkService drs;
	@Autowired
	private LunchTableService ls;
	@Autowired
	private OfferService os;
	@Autowired
	private RestOrderService ros;
	@Autowired
	private RestOrderItemService rois;
	
	public RestManController(){
		
	}
	
	@RequestMapping(value = "/onload/", method = RequestMethod.POST)
    public ResponseEntity<Void> onload(UriComponentsBuilder ucBuilder, HttpSession session) {
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		session.setAttribute("restaurant", r);
		
		List<Dish> dishes = ds.findByRestaurant(r2);
		System.out.println("size od dishes" + dishes.size());
		session.setAttribute("dishes", dishes);
		
		List<Drink> drinks = drs.findByRestaurant(r2);
		System.out.println("size of drinks " + drinks.size());
		session.setAttribute("drinks", drinks);
		
		List<LunchTable> tables = ls.findByRestaurant(r2);
		System.out.println("size of tables " + tables.size());
		session.setAttribute("tables", tables);
		
		List<Cook> cooks = us.findCookByRestaurant(r2);
		System.out.println("size of cooks " + cooks.size());
		session.setAttribute("cooks", cooks);

		
		List<Waiter> waiters = us.findWaiterByRestaurant(r2);
		session.setAttribute("waiters", waiters);
		
		List<Bartender> bartenders = us.findBartenderByRestaurant(r2);
		session.setAttribute("bartenders", bartenders);
		
		return new ResponseEntity<Void>( HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/restSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> restSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", false);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/stoloviSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> stoloviSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", false);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/jelovnikSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> jelovnikSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", false);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kartaSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> kartaSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", false);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kuvariSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> kuvariSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", false);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/konobariSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> konobariSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", false);
		session.setAttribute("sankeriHidden", true);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sankeriSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> sankeriSet(String id, Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", false);
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenKarta", true);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("ponudeHidden", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeRest/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeRest(@RequestBody Restaurant rest,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		if(!rest.getName().equals(r2.getName())){
			r2.setName(rest.getName());
		}
		if(!rest.getDescr().equals(r2.getDescr())){
			r2.setDescr(rest.getDescr());
		}
		RestaurantManager restMan = r2.getRestManager();
		restMan.setRestaurant(r2);
		r2.setRestManager(restMan);
		rms.saveRestMan(restMan);
		rs.saveRestaurant(r2);
		session.setAttribute("user", restMan);
		session.setAttribute("restaurant", r2);
		System.out.println("restoran ime" + r.getName());
		
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeDishId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeDishId(String id, Model model, HttpSession session) {
		Dish d = ds.findOne(Long.parseLong(id));
		session.setAttribute("changingDish", d);
		session.setAttribute("hiddenJelovnik2", false);
		session.setAttribute("hiddenJelovnik", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeDrinkId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeDrinkId(String id, Model model, HttpSession session) {
		Drink d = drs.findOne(Long.parseLong(id));
		session.setAttribute("changingDrink", d);
		session.setAttribute("hiddenKarta2", false);
		session.setAttribute("hiddenKarta", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeTableId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeTableId(String id, Model model, HttpSession session) {
		LunchTable lt = ls.findOne(Long.parseLong(id));
		session.setAttribute("changingTable", lt);
		session.setAttribute("hiddenStolovi2", false);
		session.setAttribute("hiddenStolovi", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeCookId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeCookId(String id, Model model, HttpSession session) {
		System.out.println("lookin for cook " + id);
		Cook lt = us.findCookById(Long.parseLong(id));
		System.out.println("Cook found");
		session.setAttribute("changingCook", lt);
		session.setAttribute("hiddenKuvari2", false);
		session.setAttribute("hiddenKuvari", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeWaiterId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeWaiterId(String id, Model model, HttpSession session) {
		Waiter lt = us.findWaiterById(Long.parseLong(id));
		session.setAttribute("changingWaiter", lt);
		session.setAttribute("hiddenKonobari2", false);
		session.setAttribute("hiddenKonobari", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeBartenderId/", method = RequestMethod.GET)
	public ResponseEntity<Void> changeBartenderId(String id, Model model, HttpSession session) {
		Bartender lt = us.findBartenderById(Long.parseLong(id));
		session.setAttribute("changingBartender", lt);
		session.setAttribute("hiddenSankeri2", false);
		session.setAttribute("hiddenSankeri", true);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/addDishSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addDishSet(Model model, HttpSession session) {
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenJelovnik", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addDrinkSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addDrinkSet(Model model, HttpSession session) {
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("hiddenKarta", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/addTableSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addTableSet(Model model, HttpSession session) {
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenStolovi", false);
		List<LunchTable> tables = ls.findAll();
		session.setAttribute("nextTable", tables.size());
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addCookSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addCookSet(Model model, HttpSession session) {
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKuvari", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addWaiterSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addWaiterSet(Model model, HttpSession session) {
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenKonobari", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addBartenderSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> addBartenderSet(Model model, HttpSession session) {
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("hiddenSankeri", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/changeDish/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeDish(@RequestBody Dish dish,   UriComponentsBuilder ucBuilder, HttpSession session) {
		System.out.println("CHANGING DISH");
		Long id = ((Dish)session.getAttribute("changingDish")).getId();
		Dish d = ds.findOne(id);
		if(!dish.getName().equals(d.getName())){
			d.setName(dish.getName());
		}
		if(!dish.getDescription().equals(d.getDescription())){
			d.setDescription(dish.getDescription());
		}
		if(!dish.getPrice().equals(d.getPrice())){
			d.setPrice(dish.getPrice());
		}
		ds.saveDish(d);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("changingDish", null);
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<Dish> dishes = ds.findByRestaurant(r2);
		session.setAttribute("dishes", dishes);

		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeDrink/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeDrink(@RequestBody Drink drink,   UriComponentsBuilder ucBuilder, HttpSession session) {
		System.out.println("CHANGING DRINK");
		Long id = ((Drink)session.getAttribute("changingDrink")).getId();
		Drink d = drs.findOne(id);
		if(!drink.getName().equals(d.getName())){
			d.setName(drink.getName());
		}
		if(!drink.getDescription().equals(d.getDescription())){
			d.setDescription(drink.getDescription());
		}
		if(!drink.getPrice().equals(d.getPrice())){
			d.setPrice(drink.getPrice());
		}
		drs.saveDrink(d);
		session.setAttribute("hiddenKarta2", true);
		session.setAttribute("changingDrink", null);

		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<Drink> drinks = drs.findByRestaurant(r2);
		session.setAttribute("drinks", drinks);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeTable/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeTable(@RequestBody LunchTable lt,   UriComponentsBuilder ucBuilder, HttpSession session) {
		System.out.println("CHANGING TABLE");
		Long id = ((LunchTable)session.getAttribute("changingTable")).getId();
		LunchTable lt2 = ls.findOne(id);
		if(lt.getChairCount()!= lt2.getChairCount()){
			lt2.setChairCount(lt.getChairCount());
		}
		if(!lt.getSegment().equals(lt2.getSegment())){
			lt2.setSegment(lt.getSegment());
		}
		ls.saveLunchTable(lt2);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("changingTable", null);

		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<LunchTable> tables = ls.findByRestaurant(r2);
		session.setAttribute("tables", tables);

		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeCook/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeCook(@RequestBody Cook c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Cook)session.getAttribute("changingCook")).getId();
		Cook c2 = us.findCookById(id);
		if(!c.getFirstName().equals(c2.getFirstName())){
			c2.setFirstName(c.getFirstName());
		}
		if(!c.getLastName().equals(c2.getLastName())){
			c2.setLastName(c.getLastName());
		}
		if(c.getBirthDate()!= null && !c.getBirthDate().equals(c2.getBirthDate())){
			c2.setBirthDate(c.getBirthDate());
		}
		if(c.getClothesSize() != c2.getClothesSize()){
			c2.setClothesSize(c.getClothesSize());
		}
		if(c.getShoesSize() != c2.getShoesSize()){
			c2.setShoesSize(c.getShoesSize());
		}
		if(!c.getRole().equals(c2.getRole())){
			c2.setRole(c.getRole());
		}
		if(c.getCurrShift()!=c2.getCurrShift()){
			c2.setCurrShift(c.getCurrShift());
		}
		if(c.getNextShift()!=c2.getNextShift()){
			c2.setNextShift(c.getNextShift());
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c2.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c2.setNextWeek(cal.getTime());
		
		us.saveUser(c2);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("changingCook", null);

		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<Cook> cooks = us.findCookByRestaurant(r2);
		session.setAttribute("cooks", cooks);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeWaiter/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeWaiter(@RequestBody Waiter c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Waiter)session.getAttribute("changingWaiter")).getId();
		Waiter c2 = us.findWaiterById(id);
		if(!c.getFirstName().equals(c2.getFirstName())){
			c2.setFirstName(c.getFirstName());
		}
		if(!c.getLastName().equals(c2.getLastName())){
			c2.setLastName(c.getLastName());
		}
		if(c.getBirthDate()!= null && !c.getBirthDate().equals(c2.getBirthDate())){
			c2.setBirthDate(c.getBirthDate());
		}
		if(c.getClothesSize() != c2.getClothesSize()){
			c2.setClothesSize(c.getClothesSize());
		}
		if(c.getShoesSize() != c2.getShoesSize()){
			c2.setShoesSize(c.getShoesSize());
		}
		if(!c.getReon().equals(c2.getReon())){
			c2.setReon(c.getReon());
		}
		if(c.getCurrShift()!=c2.getCurrShift()){
			c2.setCurrShift(c.getCurrShift());
		}
		if(c.getNextShift()!=c2.getNextShift()){
			c2.setNextShift(c.getNextShift());
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c2.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c2.setNextWeek(cal.getTime());
		
		us.saveUser(c2);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("changingWaiter", null);

		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<Waiter> waiters = us.findWaiterByRestaurant(r2);
		session.setAttribute("waiters", waiters);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeBartender/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeBartender(@RequestBody Bartender c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Bartender)session.getAttribute("changingBartender")).getId();
		Bartender c2 = us.findBartenderById(id);
		if(!c.getFirstName().equals(c2.getFirstName())){
			c2.setFirstName(c.getFirstName());
		}
		if(!c.getLastName().equals(c2.getLastName())){
			c2.setLastName(c.getLastName());
		}
		if(c.getBirthDate()!= null && !c.getBirthDate().equals(c2.getBirthDate())){
			c2.setBirthDate(c.getBirthDate());
		}
		if(c.getClothesSize() != c2.getClothesSize()){
			c2.setClothesSize(c.getClothesSize());
		}
		if(c.getShoesSize() != c2.getShoesSize()){
			c2.setShoesSize(c.getShoesSize());
		}
		if(c.getCurrShift()!=c2.getCurrShift()){
			c2.setCurrShift(c.getCurrShift());
		}
		if(c.getNextShift()!=c2.getNextShift()){
			c2.setNextShift(c.getNextShift());
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c2.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c2.setNextWeek(cal.getTime());
		
		us.saveUser(c2);
		session.setAttribute("hiddenSankeri2", true);
		session.setAttribute("changingBartender", null);
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		List<Bartender> bartenders = us.findBartenderByRestaurant(r2);
		session.setAttribute("bartenders", bartenders);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/addDish/", method = RequestMethod.POST)
    public ResponseEntity<Void> addDish(@RequestBody Dish dish,   UriComponentsBuilder ucBuilder, HttpSession session) {
	//	session.setAttribute("hiddenJelovnik", false);
		session.setAttribute("hiddenJelovnik2", true);
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.addDish(dish);
		ds.saveDish(dish);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenJelovnik", true);
		List<Dish> dishes = ds.findByRestaurant(r2);
		session.setAttribute("dishes", dishes);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addDrink/", method = RequestMethod.POST)
    public ResponseEntity<Void> addDrink(@RequestBody Drink drink,   UriComponentsBuilder ucBuilder, HttpSession session) {
	//	session.setAttribute("hiddenKarta", false);
		session.setAttribute("hiddenKarta2", true);
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.addDrink(drink);
		drs.saveDrink(drink);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenKarta", true);
		List<Drink> drinks = drs.findByRestaurant(r2);
		session.setAttribute("drinks", drinks);
		
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addTable/", method = RequestMethod.POST)
    public ResponseEntity<Void> addTable(@RequestBody LunchTable lt,   UriComponentsBuilder ucBuilder, HttpSession session) {
	//	session.setAttribute("hiddenStolovi", false);
		session.setAttribute("hiddenStolovi2", true);
		if(lt.getTableNum() == 0){
			lt.setTableNum((int) session.getAttribute("nextTable"));
		}
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.addTable(lt);
		ls.saveLunchTable(lt);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenStolovi", true);
		List<LunchTable> tables = ls.findByRestaurant(r2);
		session.setAttribute("tables", tables);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addCook/", method = RequestMethod.POST)
    public ResponseEntity<Void> addCook(@RequestBody Cook c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		//session.setAttribute("hiddenKuvari", false);
		System.out.println("trenutna smena izabrana je " + c.getCurrShift());
		Date d = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c.setNextWeek(cal.getTime());
		
		session.setAttribute("hiddenKuvari2", true);
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		c.setPassword("pass");
		r2.addCook(c);
		us.saveUser(c);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenKuvari", true);
		List<Cook> cooks = us.findCookByRestaurant(r2);
		session.setAttribute("cooks", cooks);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addWaiter/", method = RequestMethod.POST)
    public ResponseEntity<Void> addWaiter(@RequestBody Waiter c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		//session.setAttribute("hiddenKuvari", false);
		session.setAttribute("hiddenKonobari2", true);
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		c.setPassword("pass");
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c.setNextWeek(cal.getTime());
		
		r2.addWaiter(c);
		us.saveUser(c);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenKonobari", true);
		List<Waiter> waiters = us.findWaiterByRestaurant(r2);
		session.setAttribute("waiters", waiters);
		
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addBartender/", method = RequestMethod.POST)
    public ResponseEntity<Void> addBartender(@RequestBody Bartender c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		//session.setAttribute("hiddenKuvari", false);
		session.setAttribute("hiddenSankeri2", true);
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		c.setPassword("pass");
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		
		c.setCurrWeek(cal.getTime());
		
		cal.add(Calendar.DATE, 1);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
		    cal.add(Calendar.DATE, 1);
		}
		c.setNextWeek(cal.getTime());
		
		r2.addBartender(c);
		us.saveUser(c);
		rs.saveRestaurant(r2);
		session.setAttribute("hiddenSankeri", true);

		List<Bartender> bartenders = us.findBartenderByRestaurant(r2);
		session.setAttribute("bartenders", bartenders);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteDishId/", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteDishId(String id, Model model, HttpSession session) {
		Dish dish = ds.findOne(Long.parseLong(id));
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.removeDish(dish);
		ds.delete(dish);
		rs.saveRestaurant(r2);
		
		List<Dish> dishes = ds.findByRestaurant(r2);
		session.setAttribute("dishes", dishes);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	@RequestMapping(value = "/deleteDrinkId/", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteDrinkId(String id, Model model, HttpSession session) {
		Drink drink = drs.findOne(Long.parseLong(id));
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.removeDrink(drink);
		drs.delete(drink);
		rs.saveRestaurant(r2);
		
		List<Drink> drinks = drs.findByRestaurant(r2);
		session.setAttribute("drinks", drinks);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteTableId/", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteTableId(String id, Model model, HttpSession session) {
		LunchTable  lt = ls.findOne(Long.parseLong(id));
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		r2.removeTable(lt);
		ls.delete(lt);
		rs.saveRestaurant(r2);
		
		List<LunchTable> tables = ls.findByRestaurant(r2);
		session.setAttribute("tables", tables);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteEmployeeId/", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteEmployeeId(String id, Model model, HttpSession session) {
		User user = us.findOne(Long.parseLong(id));
		
		Restaurant r = (Restaurant)session.getAttribute("restaurant");
		Restaurant r2 = rs.findOne(r.getId());
		
		if(user instanceof Cook){
			Cook c = us.findCookById(Long.parseLong(id));
			r2.removeCook(c);
			us.delete(user);
			rs.saveRestaurant(r2);
			
			List<Cook> cooks = us.findCookByRestaurant(r2);
			session.setAttribute("cooks", cooks);

			return new ResponseEntity<Void>( HttpStatus.OK);
		}
		else if (user instanceof Waiter){
			Waiter w = us.findWaiterById(Long.parseLong(id));
			r2.removeWaiter(w);
			us.delete(user);
			rs.saveRestaurant(r2);
			
			List<Waiter> waiters = us.findWaiterByRestaurant(r2);
			session.setAttribute("waiters", waiters);

			return new ResponseEntity<Void>( HttpStatus.OK);
		}
		else if(user instanceof Bartender){
			Bartender b = us.findBartenderById(Long.parseLong(id));
			r2.removeBartender(b);
			us.delete(user);
			rs.saveRestaurant(r2);
			
			List<Bartender> bartenders = us.findBartenderByRestaurant(r2);
			session.setAttribute("bartenders", bartenders);

			return new ResponseEntity<Void>( HttpStatus.OK);
		}
		return new ResponseEntity<Void>( HttpStatus.CONFLICT);
	}
	
	
	
	@RequestMapping(value = "/porudzbinaSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> porudzbinaSet( Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		
		session.setAttribute("currOrderHidden", false);
		session.setAttribute("newOrderHidden", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", true);
		
		Long id =  (Long) session.getAttribute("userId");
		RestaurantManager rm = us.findRestaurantManagerById(id);
		System.out.println("kastovao uspesno rest managera sa id " + rm.getId());
		
		RestaurantOrder ro =  ros.findByStateAndRestManager("AKTIVAN", rm);
		session.setAttribute("order", ro);
		if(ro != null){
			System.out.println("nasao order sa id : " + ro.getId());
			Set<RestaurantOrderItem> orderItems = (Set<RestaurantOrderItem>) ro.getItems();
			System.out.println("nasao orderItems velicine : " + orderItems.size());
			session.setAttribute("orderItems", orderItems);
		}
		else session.setAttribute("orderItems", null);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/novaPorudzbinaSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> novaPorudzbinaSet( Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", false);
		session.setAttribute("regPonHidden", true);

		session.setAttribute("ponudeHidden", true);
		
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addOrderItem/", method = RequestMethod.POST)
    public ResponseEntity<Void> addOrderItem(@RequestBody RestaurantOrderItem orderItem,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Set<RestaurantOrderItem> newOrderItems = (Set<RestaurantOrderItem>) session.getAttribute("newOrderItems");
		if(newOrderItems == null){
			newOrderItems = new HashSet<RestaurantOrderItem>();
		}
		newOrderItems.add(orderItem);
		session.setAttribute("newOrderItems", newOrderItems);
		return new ResponseEntity<Void>( HttpStatus.CREATED);
	}
	

	@RequestMapping(value = "/saveDate/", method = RequestMethod.POST)
    public ResponseEntity<Void> saveDate(@RequestBody RestaurantOrder ro,   UriComponentsBuilder ucBuilder, HttpSession session) {
		session.setAttribute("orderDate", ro.getExpirationDate());
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addOrder/", method = RequestMethod.GET)
    public ResponseEntity<Void> addOrder(UriComponentsBuilder ucBuilder, HttpSession session) {
		System.out.println("USAO");
		Long id =  (Long) session.getAttribute("userId");
		RestaurantManager rm = us.findRestaurantManagerById(id);
		
		Set<RestaurantOrderItem> newOrderItems = (Set<RestaurantOrderItem>) session.getAttribute("newOrderItems");
		System.out.println("SIZE OF NEWORDERITEMS : " + newOrderItems.size());
		List<RestaurantOrder> ro2 =  ros.findByRestManager(rm);
		if(ro2!=null){
			for(RestaurantOrder ro : ro2){
				if(ro.getState().equals("AKTIVAN")){
					ro.setState("ZAVRSENO");
					Set<Offer> offers = ro.getOffers();
					for(Offer o : offers){
						if(o.getOfferState().equals("NA CEKANJU"))
							o.setOfferState("ODBIJENO");
					}
				}
			}
		}
		if( ros.findByRestManager(rm) == null){
			System.out.println("MENADZER NOT FOUND!");
		}
		RestaurantOrder ro = new RestaurantOrder(rm, new Date());
		System.out.println("VELICINA ITEMA JE : " + ro.getItems().size());
		ro.setItems((Set<RestaurantOrderItem>) newOrderItems);
		Date d = (Date) session.getAttribute("orderDate");
		System.out.println(session.getAttribute("orderDate"));
		if(d != null){
			ro.setExpirationDate(d);
		}
		session.setAttribute("order", ro);
		
		session.setAttribute("orderItems", newOrderItems);
		ro.setRestManager(rm);
	//	rm.setRestOrder(ro);
		
	//	us.saveUser(rm);
		ros.saveRestOrder(ro);
		us.saveUser(rm);
		for (RestaurantOrderItem roi : newOrderItems){
			rois.saveRestOrderItem(roi);
		}
		session.setAttribute("newOrderItems", null);
		return new ResponseEntity<Void>( HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/ponudeSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> ponudeSet( Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);
		session.setAttribute("ponudeHidden", false);
		session.setAttribute("regPonHidden", true);
		
		Long id =  (Long) session.getAttribute("userId");
		RestaurantManager rm = us.findRestaurantManagerById(id);
		
		RestaurantOrder ro = ros.findByStateAndRestManager("AKTIVAN", rm);
		System.out.println("FOUND REST ORDER BY ID " + ro.getId());
		
		List<Offer> offers = os.findByRestOrder(ro);
		session.setAttribute("offersRM", offers);
		
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/acceptOffer/", method = RequestMethod.GET)
	public ResponseEntity<Void> acceptOffer(String id, Model model, HttpSession session) {
		Offer acceptedOffer = os.findOne(Long.parseLong(id));	
		acceptedOffer.setOfferState("PRIHVACENO");
		os.saveOffer(acceptedOffer);
		
		Long idd =  (Long) session.getAttribute("userId");
		RestaurantManager rm = us.findRestaurantManagerById(idd);
		
		RestaurantOrder ro = ros.findByStateAndRestManager("AKTIVAN", rm);
		System.out.println("FOUND REST ORDER BY ID " + ro.getId());
		
		List<Offer> offers = os.findByRestOrder(ro);
		session.setAttribute("offersRM", offers);
		for(Offer o : offers){
			if(o.getId()!=Long.parseLong(id)){
				o.setOfferState("ODBIJENO");
				os.saveOffer(o);
			}
		}
		ro.setState("ZAVRSENO");
		ros.saveRestOrder(ro);
		session.setAttribute("offersRM", offers);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/declineOffer/", method = RequestMethod.GET)
	public ResponseEntity<Void> declineOffer(String id, Model model, HttpSession session) {
		Offer declinedOffer = os.findOne(Long.parseLong(id));
		declinedOffer.setOfferState("ODBIJENO");
		os.saveOffer(declinedOffer);
		Long idd =  (Long) session.getAttribute("userId");
		RestaurantManager rm = us.findRestaurantManagerById(idd);
		RestaurantOrder ro = ros.findByStateAndRestManager("AKTIVAN", rm);
		System.out.println("FOUND REST ORDER BY ID " + ro.getId());
		
		List<Offer> offers = os.findByRestOrder(ro);
		session.setAttribute("offersRM", offers);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/regPonudjaca/", method = RequestMethod.POST)
    public ResponseEntity<Void> regPonudjaca(@RequestBody Supplier sup,   UriComponentsBuilder ucBuilder, HttpSession session) {
		sup.setPassword("pass");
		us.saveUser(sup);
		session.setAttribute("regPonHidden", true);
		return new ResponseEntity<Void>( HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/regPonudjacaSet/", method = RequestMethod.GET)
	public ResponseEntity<Void> regPonudjacaSet( Model model, HttpSession session) {
		session.setAttribute("restHidden", true);
		session.setAttribute("stoloviHidden", true);
		session.setAttribute("jelovnikHidden", true);
		session.setAttribute("kartaHidden", true);
		session.setAttribute("kuvariHidden", true);
		session.setAttribute("konobariHidden", true);
		session.setAttribute("sankeriHidden", true);

		session.setAttribute("hiddenJelovnik", true);
		session.setAttribute("hiddenJelovnik2", true);
		session.setAttribute("hiddenStolovi", true);
		session.setAttribute("hiddenStolovi2", true);
		session.setAttribute("hiddenKuvari", true);
		session.setAttribute("hiddenKuvari2", true);
		session.setAttribute("hiddenKonobari", true);
		session.setAttribute("hiddenKonobari2", true);
		session.setAttribute("hiddenSankeri", true);
		session.setAttribute("hiddenSankeri2", true);
		
		session.setAttribute("currOrderHidden", true);
		session.setAttribute("newOrderHidden", true);
		session.setAttribute("ponudeHidden", true);
		session.setAttribute("regPonHidden", false);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
}
