package com.example.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Offer;
import com.example.models.RestaurantOrder;
import com.example.models.RestaurantOrderItem;
import com.example.models.Supplier;
import com.example.service.OfferService;
import com.example.service.RestOrderItemService;
import com.example.service.RestOrderService;
import com.example.service.UserService;

@Controller
@RequestMapping("/offer")
public class OfferController {

	@Autowired
	private UserService us;
	@Autowired
	private OfferService os;
	@Autowired
	private RestOrderService ros;
	@Autowired
	private RestOrderItemService rois;
	
	public OfferController(){
		
	}
	
	@RequestMapping(value = "/addOffer/", method = RequestMethod.POST)
    public ResponseEntity<Void> createOffer(@RequestBody Offer o,   UriComponentsBuilder ucBuilder, HttpSession session) {
			long id = (long) session.getAttribute("userId");
			Supplier sup = (Supplier)us.findSupplierById(id);
			long orderId = (long) session.getAttribute("orderIdToBind");
			RestaurantOrder ro = ros.findOne(orderId);
			o.setRest_order(ro);
			ro.addOffer(o);
			
			sup.addOffer(o);
			us.saveUser(sup);
			ros.saveRestOrder(ro);
			session.setAttribute("offerHidden2", true);
			List<Offer> offers = os.findBySupplier(sup);
			 session.setAttribute("offers", offers);
		    return new ResponseEntity<Void>( HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/changeOffer/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeOffer(@RequestBody Offer o,   UriComponentsBuilder ucBuilder, HttpSession session) {
			Offer offer = (Offer) session.getAttribute("curOffer");
		if(offer.getOfferState().equals("PRIHVACENO")){
			return new ResponseEntity<Void>( HttpStatus.CREATED);
		}
			if(o.getStart()!=null)
				offer.setStart(o.getStart());
			if(o.getEnd()!=null)
				offer.setEnd(o.getEnd());
			if(o.getDeadline()!=null)
				offer.setDeadline(o.getDeadline());
			if(o.getGaranty()!=null)
				offer.setGaranty(o.getGaranty());
			if(o.getPrice()!=null)
				offer.setPrice(o.getPrice());
			
			os.saveOffer(offer);
			 session.setAttribute("offers", os.findAll());
			 session.setAttribute("curOffer", offer);
		    return new ResponseEntity<Void>( HttpStatus.CREATED);
		//}
    }
	
	@RequestMapping(value = "/currentOffer/", method = RequestMethod.POST)
    public ResponseEntity<Void> currentOffer(UriComponentsBuilder ucBuilder, HttpSession session) throws ParseException {

		session.setAttribute("offerHidden", false);
		List<Offer> offers =  (List<Offer>) session.getAttribute("offers");
		System.out.println("OFFERS SIZE: " + offers.size());
		 List<Offer> currOffers = new ArrayList<Offer>();
		 
		 Date date = new Date();
		 
		for(int i = 0; i<offers.size(); i++){
			System.out.println("VREME ISTEKA PRVOG JE : " + offers.get(i).getEnd().toString());
			if(offers.get(i).getEnd().compareTo(date) >= 0){
				System.out.println("dodajeem");
				currOffers.add(offers.get(i));
			}
		}
 
		session.setAttribute("currOffers", currOffers);
		System.out.println("CURRENT OFFERS SIZE: " + currOffers.size());
		 return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/selectOffer/", method = RequestMethod.GET)
	public ResponseEntity<Void> selectOffer(String id, Model model, HttpSession session) {
		session.setAttribute("offerHidden", false);
		Offer of = os.findOne(Long.parseLong(id));
		session.setAttribute("curOffer", of);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/makeOffer/", method = RequestMethod.GET)
	public ResponseEntity<Void> makeOffer( String id, HttpSession session) {
		long idSup = (long) session.getAttribute("userId");
		Supplier sup = (Supplier)us.findSupplierById(idSup);
		RestaurantOrder ro = ros.findOne(Long.parseLong(id));
		long idResMan = ro.getRestManager().getId();
		//List<Offer> offers = os.findByRestOrder(ro);
		Set<Offer> offers = sup.getOffers();
		for(Offer of : offers){
			if(of.getRest_order().getId().equals(ro.getId())){
				session.setAttribute("offerHidden2", true);
				session.setAttribute("message", "exists");
				return new ResponseEntity<Void>( HttpStatus.OK);
			}
		}
		session.setAttribute("offerHidden2", false);
		session.setAttribute("orderIdToBind", Long.parseLong(id));
		session.setAttribute("message", "ok");
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getDetails/", method = RequestMethod.GET)
	public ResponseEntity<Void> getDetails( String id, HttpSession session) {
		RestaurantOrder ro = ros.findOne(Long.parseLong(id));
		List<RestaurantOrderItem> items = (List<RestaurantOrderItem>) rois.findByRestOrder(ro);
		session.setAttribute("selOrderItems", items);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
}
