package com.example.controller;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.models.Bartender;
import com.example.models.Bill;
import com.example.models.Cook;
import com.example.models.Dish;
import com.example.models.Drink;
import com.example.models.InvitationReservation;
import com.example.models.LunchTable;
import com.example.models.OrderForTable;
import com.example.models.OrderedDrinks;
import com.example.models.OrderedFood;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.Waiter;
import com.example.service.BillService;
import com.example.service.DishService;
import com.example.service.DrinkService;
import com.example.service.FriendshipRequestService;
import com.example.service.InvitationReservationService;
import com.example.service.LunchTableService;
import com.example.service.OrderForTableService;
import com.example.service.OrderedDishService;
import com.example.service.OrderedDrinksService;
import com.example.service.ReservationService;
import com.example.service.RestManService;
import com.example.service.RestOrderService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
@RequestMapping("/cookCont")
public class CookController {

	
	@Autowired
	private RestManService rs;
	
	@Autowired
	private RestOrderService ros;
	
	@Autowired
	private RestaurantService rests;
	
	@Autowired
	private FriendshipRequestService fr;
	
	@Autowired
	private UserService us;
	
	@Autowired
	private DishService ds;
	
	@Autowired
	private DrinkService drs;
	
	@Autowired
	private ReservationService rvs;
	
	@Autowired
	private LunchTableService lts;
	
	@Autowired
	private OrderForTableService oft;
	
	@Autowired
	private OrderedDishService odishs;
	
	@Autowired
	private OrderedDrinksService odrinks;
	
	@Autowired
	private BillService bs;
	
	@Autowired 
	private InvitationReservationService irs;
	
	public CookController(){}
	// poruceno preuzeto spremno
	@RequestMapping(value ="/izlastajPorudzbine", method = RequestMethod.GET) 
	public ResponseEntity<Void> izlastajPorudzbine(HttpSession session) {
		String tipKuvara = ((Cook)session.getAttribute("LogCook")).getRole();
		Restaurant restaurant = ((Cook)session.getAttribute("LogCook")).getRestaurant();
		List<LunchTable> lunchTables = lts.findByRestaurant(restaurant);
		ArrayList<OrderForTable> orderForTable = new ArrayList<OrderForTable>();
		ArrayList<OrderedFood> odabranaJela = new ArrayList<OrderedFood>();
		
		for (LunchTable lunchTable : lunchTables) {
			OrderForTable ofte = oft.findByLunchTable(lunchTable);
			if(ofte != null){
			orderForTable.add(ofte);
			}
		}
		for (OrderForTable oneOrder : orderForTable) {
			List <OrderedFood> porucenaPicaPoStolu = odishs.findByOrder(oneOrder);
			for (OrderedFood orderedFood : porucenaPicaPoStolu) {
				if(tipKuvara.toLowerCase().contains(orderedFood.getFood().getType().substring(0, 2).toLowerCase())){
					if(orderedFood.getPrepState().equals("poruceno")){
					odabranaJela.add(orderedFood);
					}
				}
			}
		}
		session.setAttribute("dishes", odabranaJela);
		return new ResponseEntity<Void>( HttpStatus.CREATED);  
	}
	
	@RequestMapping(value ="/izlastajPorudzbineBartender", method = RequestMethod.GET) 
	public ResponseEntity<Void> izlastajPorudzbineBartender(HttpSession session) {
		Restaurant restaurant = ((Bartender)session.getAttribute("LogBartender")).getRestaurant();
		List<LunchTable> lunchTables = lts.findByRestaurant(restaurant);
		List<OrderForTable> orderForTable = new ArrayList<OrderForTable>();
	//	ArrayList<Integer> brojStola = new ArrayList<Integer>();
		List<OrderedDrinks> porucenaPicaSva = new ArrayList<OrderedDrinks>();
		List<OrderedDrinks> porucenaPica = new ArrayList<OrderedDrinks>();
		for (LunchTable lunchTable : lunchTables) {
			OrderForTable ofte = oft.findByLunchTable(lunchTable);
			if(ofte != null){
			orderForTable.add(ofte);
		//	brojStola.add(lunchTable.getTableNum());
			
			}
		
		}
		for (OrderForTable oneOrder : orderForTable) {
			List <OrderedDrinks> porucenaPicaPoStolu = odrinks.findByOrder(oneOrder);
			porucenaPicaSva.addAll(porucenaPicaPoStolu);
		}
		for (OrderedDrinks orderedDrink : porucenaPicaSva) {
			if(orderedDrink.getPrepState().equals("poruceno")){
				porucenaPica.add(orderedDrink);
			}
		}
		
		session.setAttribute("glupiAtribut", "nekiglupiatribut");
		session.setAttribute("porucenaPica", porucenaPica);
		System.out.println("VELICINA PORUCENIH PICA"+porucenaPica.size());
		for (OrderedDrinks porucenaPica1 : porucenaPica) {
			System.out.println("PORUCENO PICE: "+ porucenaPica1.getPrepState());
			System.out.println("PODRECENO PICE ID " +porucenaPica1.getDrink().getName());
		}
	
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/izmeniStatusPica/{id}", method = RequestMethod.POST) 
	public ResponseEntity<Void> izmeniStatusPica(HttpSession session, @PathVariable("id") String id) {
		OrderedDrinks od = odrinks.findOne(Long.parseLong(id));
		od.setPrepState("preuzeto");
		odrinks.saveOrderedDrinks(od);
		
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/obrisiPiceIzPorudzbine/{id}", method = RequestMethod.POST) 
	public String obrisiPiceIzPorudzbine(HttpSession session, @PathVariable("id") String id) {
		System.out.println("Usao u obrisi pice iz porudzbine");
		OrderedDrinks od = odrinks.findOne(Long.parseLong(id));
		if(od.getPrepState().equals("poruceno")){
			System.out.println("");
			OrderForTable ordfor = oft.findOne(od.getOrder().getId());
			ordfor.removeDrink(od);
			oft.saveOrderForTable(ordfor);
			odrinks.delete(od);
			oft.saveOrderForTable(ordfor);
			//return "/cookCont/izlastajStolovePonudu";
			return "ok";
		}
		
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return "notOK";
		//return "/cookCont/izlastajStolovePonudu";
	}
	
	@RequestMapping(value ="/obrisiHranuIzPorudzbine/{id}", method = RequestMethod.POST) 
	public String obrisiHranuIzPorudzbine(@PathVariable("id") String id, HttpSession session) {
		System.out.println("Usao u obrisi hranu iz porudzbine");
		OrderedFood od = odishs.findOne(Long.parseLong(id));
		System.out.println("PREP STATE" +od.getPrepState());
		if(od.getPrepState().equals("poruceno")){
			
			OrderForTable ordfor = oft.findOne(od.getOrder().getId());
			ordfor.removeDish(od);
			oft.saveOrderForTable(ordfor);
			odishs.delete(od);
			oft.saveOrderForTable(ordfor);
			System.out.println("OBRISAO");
			return "ok";
		//	return "/cookCont/izlastajStolovePonudu";
		}
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return "notOk";
		//return "/cookCont/izlastajStolovePonudu";
	}
	
	@RequestMapping(value ="/izmeniStatusHrane/{id}", method = RequestMethod.POST) 
	public ResponseEntity<Void> izmeniStatusHrane(@PathVariable("id") String id,HttpSession session) {
	
		OrderedFood od = odishs.findOne(Long.parseLong(id));
		od.setPrepState("preuzeto");
		odishs.saveOrderedFodd(od);
		
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/izmeniStatusPicaSpremno/{id}", method = RequestMethod.POST) 
	public ResponseEntity<Void> izmeniStatusPicaSpremno(HttpSession session, @PathVariable("id") String id) {
		OrderedDrinks od = odrinks.findOne(Long.parseLong(id));
		od.setPrepState("spremno");
		odrinks.saveOrderedDrinks(od);
		
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/izmeniSpremnoJelo/{id}", method = RequestMethod.POST) 
	public ResponseEntity<Void> izmeniSpremnoJelo(HttpSession session, @PathVariable("id") String id) {
		OrderedFood od = odishs.findOne(Long.parseLong(id));
		od.setPrepState("spremno");
		odishs.saveOrderedFodd(od);
		
		List<OrderForTable> porudzbinaZaSto = oft.findAll();
		session.setAttribute("picaZaStolove", porudzbinaZaSto);
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/izdajRacun/{id}", method = RequestMethod.POST) 
	public ResponseEntity<Void> izdajRacun(HttpSession session, @PathVariable("id") String id) {
		System.out.println("USAO");
		Date d = new Date();
		int dHours = d.getHours();
		LunchTable lunchTable = lts.findOne(Long.parseLong(id));
		Waiter w = (Waiter)session.getAttribute("LogWaiter");
		Waiter w2 = us.findWaiterById(w.getId());
		Restaurant restaurant = w2.getRestaurant();
		System.out.println("REST " + restaurant.getId());
		List<Reservation> rezervacije = rvs.findByRestaurant(restaurant.getName());
		List<InvitationReservation> invit = new ArrayList<InvitationReservation>();
		for(Reservation r : rezervacije){
			if(r.getDate().getDay()==d.getDay() && r.getDate().getYear()==d.getYear() && r.getDate().getMonth()==d.getMonth() && r.getStart().getHours()==d.getHours()){
				invit.addAll(irs.findByReservation(r.getId()));
				System.out.println("DODAT");
			}
		}
		OrderForTable order = null;
		System.out.println(invit.size());
		for(InvitationReservation ir : invit){
			Long idd = ir.getTableId();
			OrderForTable orderForTable=oft.findOne(idd);
			if(orderForTable.getTable().getId().equals(lunchTable.getId())){
				order = orderForTable;
				System.out.println("JEDNAKOO");
				break;
			}
		}
		
		Bill bill = new Bill();
		Date date = new Date();
		String datum = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		datum = sdf.format(date);
		bill.setDateString(datum);
		bill.setDate(date);
		bill.setRestaurant(restaurant);
		
		OrderForTable ooo = oft.findOne(order.getId());
		
		Double tt = ooo.getTotalCount();
		bill.setTotal(tt);
		
		bill.setWaiter(w2);
		bill = bs.saveBill(bill);

		
		InvitationReservation irr = irs.findByTableOrderId(ooo.getId());
		
		Set<OrderedDrinks> oodd = ooo.getDrinkOrders();
		for(OrderedDrinks od : oodd){
			bill.addDrinkOrder(od);
		}
		//bill.setDrinkOrders(oodd);
		Set<OrderedFood> ooff = ooo.getFoodOrders();
		for(OrderedFood of : ooff){
			bill.addFoodOrder(of);
		}
		//bill.setFoodOrders(ooff);

		
		bs.saveBill(bill);
		System.out.println("REZERVACIJA " + irr.getId());
		Reservation res = rvs.findById(irr.getReservation());
		System.out.println("REZ2 " + res.getId());
		
		bill.setReservation(res);
		rvs.saveReservation(res);
	
		
		bs.saveBill(bill);
		oft.saveOrderForTable(ooo);
		restaurant.addBill(bill);
		rests.saveRestaurant(restaurant);
		w2.setBill(bill);
		us.saveUser(w2);
		for(OrderedFood of : ooo.getFoodOrders()){
			odishs.saveOrderedFodd(of);
		}
		for(OrderedDrinks od : ooo.getDrinkOrders()){
			odrinks.saveOrderedDrinks(od);
		}
		bs.saveBill(bill);
		System.out.println("KRAJ");
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/predjinaOcenjivanje/{id}", method = RequestMethod.POST) 
	public String predjinaOcenjivanje(HttpSession session, @PathVariable("id") String id) {
		
		session.setAttribute("ocenaAtribut",id);
		 return "ocenjivanje.jsp";
	}
	
	@RequestMapping(value ="/Ocena/{id}/{ocenaJela}/{ocenaUsluge}/{ocenaRestorana}", method = RequestMethod.POST) 
	public String Ocena(HttpSession session, @PathVariable("id") String id, @PathVariable("ocenaJela") String ocenaJela, @PathVariable("ocenaUsluge") String ocenaUsluge, @PathVariable("ocenaRestorana") String ocenaRestorana) {
		Reservation reservation = rvs.findById(Long.parseLong(id));
		Bill bill = bs.findByReservation(reservation);
		
		Restaurant restaurant = rests.findByName(reservation.getRestaurant());
		restaurant.setSumOfGrades((restaurant.getSumOfGrades()+Integer.parseInt(ocenaRestorana)));
		restaurant.setNumberOfGrades(restaurant.getNumberOfGrades()+1);
		restaurant.setGrade(((double)restaurant.getSumOfGrades())/restaurant.getNumberOfGrades());
		rests.saveRestaurant(restaurant);
		
		Waiter waiter = bill.getWaiter();
		waiter.setSumOfGrades(waiter.getSumOfGrades()+Integer.parseInt(ocenaUsluge));
		waiter.setNumOfGrades(waiter.getNumOfGrades()+1);
		waiter.setGrade((double)waiter.getSumOfGrades()/waiter.getNumOfGrades());
		us.saveUser(waiter);
		
		Set<OrderedFood> orderedFood = bill.getFoodOrders();
		
		for (OrderedFood orderedFood2 : orderedFood) {
			Dish dish = orderedFood2.getFood();
			dish.setSumOfGrades(dish.getSumOfGrades()+Integer.parseInt(ocenaJela));
			dish.setNumberOfGrades(dish.getNumberOfGrades()+1);
			dish.setRate((double)dish.getSumOfGrades()/dish.getNumberOfGrades());
			ds.saveDish(dish);
		}
		
		return "/guestMain/fillRestaurants";	}
	
	@RequestMapping(value ="/izlastajStolovePonudu", method = RequestMethod.POST) 
	public ResponseEntity<Void> izlastajStolovePonudu(HttpSession session) {
		Restaurant restaurant = ((Waiter)session.getAttribute("LogWaiter")).getRestaurant();
		List<Drink> drinks = drs.findByRestaurant(restaurant);
		List<Dish> dishes =  ds.findByRestaurant(restaurant);
		List<LunchTable> tables = lts.findByRestaurant(restaurant);
		List<OrderForTable> ofts = new ArrayList<OrderForTable>();
		List<LunchTable> reonTables = new ArrayList<LunchTable>();
		List<LunchTable> stoloviPoruceni = new ArrayList<LunchTable>();
		System.out.println("reontables iz baze " + tables.size());
		Date d = new Date();
		for (LunchTable lunchTable : tables) {
			System.out.println("REON"+((Waiter)session.getAttribute("LogWaiter")).getReon());
			System.out.println("SEGMENT"+lunchTable.getSegment());
			if(((Waiter)session.getAttribute("LogWaiter")).getReon().trim().equals(lunchTable.getSegment().trim())){
				reonTables.add(lunchTable);
				
				List<OrderForTable> f = oft.findByLunchTableAndTime(lunchTable, new java.sql.Time(d.getHours(), 0, 0));
				for (OrderForTable orderForTable : f) {
					if(orderForTable.getDate().getYear()==d.getYear() && orderForTable.getDate().getMonth()==d.getMonth() && orderForTable.getDate().getDay() == d.getDay()){
						ofts.add(orderForTable);
						stoloviPoruceni.add(orderForTable.getTable());
					}
				}
				//OrderForTable f = oft.findByLunchTable(lunchTable);
				
				/*if(f!=null){
					System.out.println("onaj fu if petlji" + f.getId());
				ofts.add(f);
				stoloviPoruceni.add(f.getTable());
				
				}*/
			}
		}
		session.setAttribute("orderedDrinks1", ofts);
		session.setAttribute("tablesBill", stoloviPoruceni);
	//	System.out.println("broj orderDrinks1" + ofts.size());
		session.setAttribute("drinks", drinks);
		session.setAttribute("dishes", dishes);
		session.setAttribute("tables", reonTables);
		System.out.println("BROJ STOLOVA" + reonTables.size());
		
		return new ResponseEntity<Void>( HttpStatus.OK);	}
	
	@RequestMapping(value ="/prikazRada", method = RequestMethod.GET) 
	public ResponseEntity<Void> prikazRada(HttpSession session) {
		Restaurant restaurant = ((Cook)session.getAttribute("LogCook")).getRestaurant();
		List<Cook> kuvari = us.findCookByRestaurant(restaurant);
		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int brojDana = numOfDays(month+1, year+1);
		ArrayList<Cook> prvaSmena = new ArrayList<Cook>();
		ArrayList<Cook> drugaSmena = new ArrayList<Cook>();
		//cal.set(Calendar.DAY_OF_MONTH, 1);
	    //System.out.println(cal.getTime());
		for (Cook cook : kuvari) {
			if(cook.getCurrShift()==1){
				prvaSmena.add(cook);
			}else{
				drugaSmena.add(cook);
				System.out.println("DRUGA SMENA KUVAR  "+cook);
			}
		}
		Date currWeek = kuvari.get(0).getCurrWeek();
		Date nextWeek = kuvari.get(0).getNextWeek();
		
		String[] currw = currWeek.toString().split(" ");
		String[] nextw = nextWeek.toString().split(" ");
		String ponedeljakPrvaSmena = currw[0].substring(currw[0].length()-2, currw[0].length());
		String ponedeljakPrvaSmenaMesec = currw[0].substring(currw[0].length()-5, currw[0].length()-3);
		String ponedeljakDrugaSmena = nextw[0].substring(nextw[0].length()-2, nextw[0].length());
		String ponedeljakDrugaSmenaMesec = nextw[0].substring(nextw[0].length()-5, nextw[0].length()-3);
		
		System.out.println(Integer.parseInt(ponedeljakPrvaSmenaMesec) + "JE PRVA SMENA TJ DAN");
		System.out.println(Integer.parseInt(ponedeljakDrugaSmenaMesec) + "JE DRUGa SMENA TJ DAN");
	    String mesec = getMonthForInt(month);
		session.setAttribute("danUMesecu", dayOfMonth);
		session.setAttribute("brojDana", brojDana);
		session.setAttribute("mesec", mesec);
		session.setAttribute("mesecTrenutni", month);
		session.setAttribute("kuvariPrvaSmena", prvaSmena);
		session.setAttribute("kuvariDrugaSmena", drugaSmena);
		session.setAttribute("pocetakNedelje", Integer.parseInt(ponedeljakPrvaSmena));
		session.setAttribute("pocetakNedeljeMesec", Integer.parseInt(ponedeljakPrvaSmenaMesec));
		session.setAttribute("krajNedelje", Integer.parseInt(ponedeljakDrugaSmena));
		session.setAttribute("krajNedeljeMesec", Integer.parseInt(ponedeljakDrugaSmenaMesec));
		
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/reloadPorudzbine", method = RequestMethod.GET) 
	public ResponseEntity<Void> reloadPorudzbine(HttpSession session) {
		Restaurant restaurant = ((Waiter)session.getAttribute("LogWaiter")).getRestaurant();
		
		return new ResponseEntity<Void>( HttpStatus.OK);  
	}
	
	@RequestMapping(value ="/prikaziGrafStolovi", method = RequestMethod.POST) 
	public ResponseEntity<Void> prikaziGrafStolovi(HttpSession session) {
		Restaurant restaurant = ((Waiter)session.getAttribute("LogWaiter")).getRestaurant();
		List<LunchTable> lunchTables = lts.findByRestaurant(restaurant);
		ArrayList<LunchTable> pusacki = new ArrayList<LunchTable>();
		ArrayList<LunchTable> nepusacki = new ArrayList<LunchTable>();
		ArrayList<LunchTable> basta = new ArrayList<LunchTable>();
		
		for (LunchTable lunchTable : lunchTables) {
			if(lunchTable.getSegment().contains("Pusacki")){
				pusacki.add(lunchTable);
				System.out.println("DODAO STO" +lunchTable.getTableNum());
			}
			else if(lunchTable.getSegment().contains("Nepusacki")){
				nepusacki.add(lunchTable);
			}else if(lunchTable.getSegment().contains("Basta")){
				basta.add(lunchTable);
			}
			
		}
		
		session.setAttribute("pusackiSegment", pusacki);
		session.setAttribute("nepusackiSegment", nepusacki);
		session.setAttribute("bastaSegment", basta);
		session.setAttribute("duzinaKalendara", 5);
		
		return new ResponseEntity<Void>( HttpStatus.OK);	}
	
	//******************************DODAVANJE PICA I HRANE**************************************//
	
	//*******************************************************************************************//
	
	@RequestMapping(value = "/addOrderDrink/{sto}/{pice}", method = RequestMethod.POST) 
	public ResponseEntity<Void> addOrderDrink(@PathVariable("sto") String sto, @PathVariable("pice") String pice, HttpSession session) {
		System.out.println("STO  " + sto +"  PICE   "+ pice);
		Restaurant restaurant = ((Waiter)session.getAttribute("LogWaiter")).getRestaurant();
		Drink drink = drs.findByName(pice);
		LunchTable lunchTable = lts.findByName(Integer.parseInt(sto));
		
		
		OrderForTable orderForTable = oft.findByLunchTable(lunchTable);
		if(orderForTable == null){
			OrderForTable ordForTab = new OrderForTable();
			ordForTab.setTable(lunchTable);
			ordForTab.setTotalCount(drink.getPrice());
			ordForTab.setReadyOnArrival(false);
			oft.saveOrderForTable(ordForTab);
			
			OrderedDrinks od = new OrderedDrinks();
			od.setDrink(drink);
			od.setPrepState("poruceno");
			od.setOrder(ordForTab);
			
			odrinks.saveOrderedDrinks(od);
			System.out.println("SET PICA " + ordForTab.getDrinkOrders().size());
			session.setAttribute("orderedDrinks1", ordForTab.getDrinkOrders());
			session.setAttribute("PoruceniSto", sto);
		}else{
		
		OrderedDrinks od = new OrderedDrinks();
		od.setDrink(drink);
		od.setPrepState("poruceno");
		od.setOrder(orderForTable);
		
		orderForTable.setTotalCount(orderForTable.getTotalCount()+od.getDrink().getPrice());
		odrinks.saveOrderedDrinks(od);
		oft.saveOrderForTable(orderForTable);
		
		

		//dodati za rezervaciju da li treba da bude spremno ili ne odmah kad dodju gosti
		System.out.println("SET PICA " + orderForTable.getDrinkOrders().size());
		//session.setAttribute("orderedDrinks1", orderForTable.getDrinkOrders());
		//session.setAttribute("PoruceniSto", sto);
		System.out.println("ORDER FOR TABLE " +orderForTable);
		System.out.println("PORUCENA PICA" + od);
		}
		return new ResponseEntity<Void>( HttpStatus.OK);	}
	
	@RequestMapping(value = "/addOrderDish/{sto}/{jelo}", method = RequestMethod.POST) 
	public ResponseEntity<Void> addOrderDish(@PathVariable("sto") String sto, @PathVariable("jelo") String jelo, HttpSession session) {
		System.out.println("STO  " + sto +"  JELO   "+ jelo);
		Restaurant restaurant = ((Waiter)session.getAttribute("LogWaiter")).getRestaurant();
		Dish dish = ds.findByName(jelo);
		LunchTable lunchTable = lts.findByName(Integer.parseInt(sto));
		
		
		OrderForTable orderForTable = oft.findByLunchTable(lunchTable);
		if(orderForTable == null){
			OrderForTable ordForTab = new OrderForTable();
			ordForTab.setTable(lunchTable);
			ordForTab.setTotalCount(dish.getPrice());
			ordForTab.setReadyOnArrival(false);
			oft.saveOrderForTable(ordForTab);
			
			OrderedFood od = new OrderedFood();
			od.setFood(dish);
			od.setPrepState("poruceno");
			od.setOrder(ordForTab);
			
			odishs.saveOrderedFodd(od);
			System.out.println("SET Hrane " + ordForTab.getFoodOrders().size());
			//session.setAttribute("orderedFood1", ordForTab.getFoodOrders());
			//session.setAttribute("PoruceniSto1", sto);
		}else{
		
		OrderedFood od = new OrderedFood();
		od.setFood(dish);
		od.setPrepState("poruceno");;
		od.setOrder(orderForTable);
		odishs.saveOrderedFodd(od);
		
		orderForTable.setTotalCount(orderForTable.getTotalCount()+od.getFood().getPrice());
		oft.saveOrderForTable(orderForTable);
		
		

		//dodati za rezervaciju da li treba da bude spremno ili ne odmah kad dodju gosti
		System.out.println("SET HRANE " + orderForTable.getFoodOrders().size());
		//session.setAttribute("orderedFood1", orderForTable.getFoodOrders());
		//session.setAttribute("PoruceniSto1", sto);
		System.out.println("ORDER FOR TABLE " +orderForTable);
		System.out.println("PORUCENA HRANA" + od);
		}
		return new ResponseEntity<Void>( HttpStatus.OK);	}
	
	//*****************************IZMENE PODATAKA****************************************//
	
	//*************************************************************************************//
	
	
	@RequestMapping(value = "/changeCook1/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeCook1(@RequestBody Cook c,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Cook)session.getAttribute("LogCook")).getId();
		Cook c2 = us.findCookById(id);
		System.out.println("USAOOO! "+ id);
		if(!c.getFirstName().equals(c2.getFirstName())){
			c2.setFirstName(c.getFirstName());
		}
		if(!c.getLastName().equals(c2.getLastName())){
			c2.setLastName(c.getLastName());
		}
		System.out.println("pass pre"+c.getPassword());
		System.out.println("pass posle"+c2.getPassword());
		if(!c.getPassword().equals(c2.getPassword())){
			c2.setPassword(c.getPassword());
		}
		if(c.getClothesSize() != c2.getClothesSize()){
			c2.setClothesSize(c.getClothesSize());
		}
		if(c.getShoesSize() != c2.getShoesSize()){
			c2.setShoesSize(c.getShoesSize());
		}
		System.out.println("ROLEEEE"+c.getRole());
		if(!c.getRole().equals(c2.getRole())){
			c2.setRole(c.getRole());
			System.out.println("ROLEEEE"+c.getRole());
		}
		session.setAttribute("LogCook", c2);
		us.saveUser(c2);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeWaiter1/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeWaiter1(@RequestBody Waiter w,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Waiter)session.getAttribute("LogWaiter")).getId();
		Waiter w2 = us.findWaiterById(id);
		System.out.println("Change waiter" + id);
		if(!w.getFirstName().equals(w2.getFirstName())){
			w2.setFirstName(w.getFirstName());
		}
		if(!w.getLastName().equals(w2.getLastName())){
			w2.setLastName(w.getLastName());
		}
		if(!w.getPassword().equals(w2.getPassword())){
			w2.setPassword(w.getPassword());
		}
		if(w.getBirthDate()!= null && !w.getBirthDate().equals(w2.getBirthDate())){
			w2.setBirthDate(w.getBirthDate());
		}
		if(w.getClothesSize() != w2.getClothesSize()){
			w2.setClothesSize(w.getClothesSize());
		}
		if(w.getShoesSize() != w2.getShoesSize()){
			w2.setShoesSize(w.getShoesSize());
		}
		session.setAttribute("LogWaiter", w2);
		us.saveUser(w2);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changeBartender1/", method = RequestMethod.POST)
    public ResponseEntity<Void> changeBartender1(@RequestBody Bartender b,   UriComponentsBuilder ucBuilder, HttpSession session) {
		Long id = ((Bartender)session.getAttribute("LogBartender")).getId();
		Bartender b2 = us.findBartenderById(id);
		System.out.println("Change bartender" + id);
		if(!b.getFirstName().equals(b2.getFirstName())){
			b2.setFirstName(b.getFirstName());
		}
		if(!b.getLastName().equals(b2.getLastName())){
			b2.setLastName(b.getLastName());
		}
		if(!b.getPassword().equals(b2.getPassword())){
			b2.setPassword(b.getPassword());
		}
		if(b.getClothesSize() != b2.getClothesSize()){
			b2.setClothesSize(b.getClothesSize());
		}
		if(b.getShoesSize() != b2.getShoesSize()){
			b2.setShoesSize(b.getShoesSize());
		}
		session.setAttribute("LogBartender", b2);
		us.saveUser(b2);
		return new ResponseEntity<Void>( HttpStatus.OK);
	}
	
	public int numOfDays(int mm,int yyyy){
		int daysofmonth;
		if((mm == 4) || (mm ==6) || (mm ==9) || (mm == 11)){	
			daysofmonth = 30;
		}
		else{
			daysofmonth = 31;
			
			if(mm == 2){
				if (yyyy/4 - (yyyy/4) != 0){
					daysofmonth = 28;
				}
				else if (yyyy/100 - (yyyy/100) != 0){
					daysofmonth = 29;
				}
				else if(yyyy/400 - (yyyy/400) != 0){
					daysofmonth = 28;
				}
				else{
					daysofmonth = 29;
				}

			}

		}

		return daysofmonth;
	}
	
	String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }
}
