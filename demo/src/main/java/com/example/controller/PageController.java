package com.example.controller;

import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.models.Admin;
import com.example.models.Dish;
import com.example.models.Drink;
import com.example.models.FriendshipRequest;
import com.example.models.Guest;
import com.example.models.LunchTable;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.models.RestaurantManager;
import com.example.service.DishService;
import com.example.service.DrinkService;
import com.example.service.FriendshipRequestService;
import com.example.service.LunchTableService;
import com.example.service.ReservationService;
import com.example.service.RestManService;
import com.example.service.RestOrderService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
public class PageController {

	@Autowired
	private RestManService rs;
	
	@Autowired
	private RestOrderService ros;
	
	@Autowired
	private RestaurantService rests;
	
	@Autowired
	private FriendshipRequestService fr;
	
	@Autowired
	private UserService us;
	
	@Autowired
	private DishService ds;
	
	@Autowired
	private DrinkService drs;
	
	@Autowired
	private LunchTableService lts;
	
	@Autowired
	private ReservationService rvs;
	
	public PageController(){
		
	}
	
	
	@RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request,HttpServletResponse response) throws IOException{
		//response.sendRedirect("index.html");
	    return new ModelAndView("index.jsp");
    }

	/*
	@RequestMapping(value="/regRestoran", method = RequestMethod.GET)
    String regRestoran(HttpServletRequest request,HttpServletResponse response) throws IOException{
		response.sendRedirect("regRest.html");
	    return "regRest";
    }
	*/
	
	//STRANICA ZA PONUDJACA
	@RequestMapping("/ponudjac") 
	public ModelAndView ponudjac() {
	   ModelAndView mv = new ModelAndView("Ponudjac.jsp");
	   //mv.addObject("isRestaurant", true);		// moze da se koristi iz view-a kao $("#isRestaurant")
	   return mv;
	}
	
	@RequestMapping("/dodajPonudjaca") 
	public ResponseEntity<Void> dodajPonudjaca() {

	Admin a = new Admin("marko", "markovic", "admin@gmail.com", "pass");
	us.saveUser(a);
/*	
	Restaurant r = new Restaurant("RESTORAN1", "italijanski restoran");
	
	Restaurant r1 = new Restaurant("RESTORAN2", "kineski restoran");
	
	RestaurantManager rm = new RestaurantManager("Dusan", "Jeftic", "d@gmail.com", "ddddd", 45, 41);
	rm.setRestaurant(r);
	r.setRestManager(rm);

	rs.saveRestMan(rm);

	rests.saveRestaurant(r);

	rests.saveRestaurant(r);
	
	RestaurantManager rm1 = new RestaurantManager("Zana", "Bilbija", "z@gmail.com", "zzzzz", 45, 41);
	rm1.setRestaurant(r1);
	r1.setRestManager(rm1);

	rs.saveRestMan(rm1);
	rests.saveRestaurant(r1);
*/

//		Supplier sup = new Supplier("Milica", "Milic", "mm@gmail.com", "mm");
//		us.saveUser(sup);
/*	Admin a = new Admin("marko", "markovic", "admin@gmail.com", "pass");
	us.saveUser(a);
		Restaurant r = new Restaurant("RESTORAN1", "italijanski restoran");
		
		RestaurantManager rm = new RestaurantManager("Dusan", "Jeftic", "d@gmail.com", "ddddd", 45, 41);
		rm.setRestaurant(r);
		r.setRestManager(rm);

		rs.saveRestMan(rm);
		rests.saveRestaurant(r);
	//	RestaurantManager rm = new RestaurantManager("Dusan", "Jeftic", "d@gmail.com", "ddddd", 45, 41);
		rs.saveRestMan(rm);
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		try {
			d = fmt.parse("2017-02-26");
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RestaurantOrderItem it1 = new RestaurantOrderItem("brasno", (double) 50, "kg");
		RestaurantOrder o1 = new RestaurantOrder(rm, d);
		o1.addItem(it1);
		ros.saveRestOrder(o1);*/
		return new ResponseEntity<Void>( HttpStatus.CREATED);  
	}
	
	@RequestMapping("/dodajDefaultGosta") 
	public ResponseEntity<Void> dodajDefaultGosta() {
		Guest g = new Guest("Vexhalia", "Vassar", "money@gmail.com", "trinket", 2, 0.0, 0.0);
		Guest g2 = new Guest("Pike", "Trickfoot", "bahamut@gmail.com", "pike", 2, 0.0, 0.0);
		Guest g3 = new Guest("Keyleth", "Ashari", "airdruid@gmail.com", "aramente", 2, 0.0, 0.0);
		Reservation res = new Reservation();
		Reservation res2 = new Reservation();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		Date d2 = new Date();
		try {
			d = fmt.parse("2017-12-26");
			d2 = fmt.parse("2017-10-22");
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		res.setDate(d);
		res2.setDate(d2);
		res.setReservator(g);
		res2.setReservator(g);
		res.setPrepareBeforehand(false);
		res2.setPrepareBeforehand(false);
		res.setRestaurant("Dagger Dagger Dagger");
		res2.setRestaurant("Mythral's Corner");
		res.setStart(new java.sql.Time(17, 42, 20));
		res2.setStart(new java.sql.Time(18, 30, 50));
		res.setEnd(new Time(18, 20, 59));
		res2.setEnd(new Time(19, 05, 20));
		us.saveUser(g);
		us.saveUser(g2);
		us.saveUser(g3);
		rvs.saveReservation(res);
		rvs.saveReservation(res2);
		return new ResponseEntity<Void>( HttpStatus.CREATED);  
	}
	
	@RequestMapping("/dodajDefaultStolove") 
	public ResponseEntity<Void> dodajDefaultStolove() {
		Restaurant r = rests.findByName("Dagger Dagger Dagger");
		Dish d = new Dish("baklava", "honeyed cake", 80.0 , 0.0 , 0,"Desert", 1, null, r);
		Dish d2 = new Dish("fried salmon", "fried fish with carrots", 120.0, 0.0,0, "Peceno jelo", 1,null, r);
		Drink dr = new Drink();
		dr.setDescription("red wine");
		dr.setName("red wine");
		dr.setPrice(60.0);
		dr.setRestaurant(r);
		d = ds.saveDish(d);
		d2 = ds.saveDish(d2);
		dr = drs.saveDrink(dr);
		r.addDish(d);
		r.addDish(d2);
		r.addDrink(dr);
		r = rests.saveRestaurant(r);
		
		LunchTable lt = new LunchTable();
		lt.setChairCount(4);
		lt.setRestaurant(r);
		lt.setSegment("pusacki");
		lt.setTableNum(1);
		LunchTable lt2 = new LunchTable();
		lt2.setChairCount(3);
		lt2.setRestaurant(r);
		lt2.setSegment("pusacki");
		lt2.setTableNum(2);
		lt = lts.saveLunchTable(lt);
		lt2 = lts.saveLunchTable(lt2);
		r.addTable(lt);
		r.addTable(lt2);
		rests.saveRestaurant(r);
		return new ResponseEntity<Void>( HttpStatus.CREATED);
	}
	
	@RequestMapping("/dodajDefaultPrijatelje") 
	public ResponseEntity<Void> dodajDefaultPrijatelje() {
		Guest def = us.findGuestByEmail("money@gmail.com");
		Guest g = new Guest("Vaxildan", "Vassar", "dagger@gmail.com", "vex", 2, 0.0, 0.0);
		us.saveUser(g);
		g = us.findGuestByEmail(g.getEmail());
		FriendshipRequest f = new FriendshipRequest();
		f.setRequester(def.getId());
		f.setResponser(g.getId());
		f.setStatus("accepted");
		fr.saveFriendship(f);
		g = new Guest("Percy", "De Rollo", "whitestone@gmail.com", "cassandra", 2, 0.0, 0.0);
		us.saveUser(g);
		g = us.findGuestByEmail(g.getEmail());
		f = new FriendshipRequest();
		f.setRequester(def.getId());
		f.setResponser(g.getId());
		f.setStatus("accepted");
		fr.saveFriendship(f);
		return new ResponseEntity<Void>( HttpStatus.CREATED);  
	}
	@RequestMapping("/dodajDefaultRestoran") 
	public ResponseEntity<Void> dodajDefaultRestoran() {
		Restaurant r1 = new Restaurant("Dagger Dagger Dagger", "despite its name, it's a quaint tea place");
		r1.setLocX(49.258);
		r1.setLocY(22.55);
		Restaurant r2 = new Restaurant("Mythral's Corner", "add text here");
		r2.setLocX(270.758);
		r2.setLocY(-450.668);
		rests.saveRestaurant(r1);
		rests.saveRestaurant(r2);
		Restaurant r3 = new Restaurant("Sarenrae's Quaint Temple", "For all your meditative places");
		r3.setGrade(5);
		r3.setLocX(52.699);
		r3.setLocY(30.478);
		rests.saveRestaurant(r3);
		return new ResponseEntity<Void>( HttpStatus.CREATED);  
}
	




	// ADMIN , REGISTROVANJE RESTORANA ILI MENADZERA RESTORANA
	@RequestMapping("/headAdmin") 
	//@Produces(MediaType.APPLICATION_JSON)
	public String register(Model model ) {
		System.out.println("USER JE " + model.asMap().get("user"));
		 model.addAttribute("isHeadAdmin", "true");
	     return "Admin.jsp";
	}
	
	@RequestMapping("/guest") 
	public ModelAndView display() {
	   ModelAndView mv = new ModelAndView("Guest.html");
	   return mv;
	}
	@RequestMapping("foo")
	public String foo() {
		throw new RuntimeException("Exception u kontroleru");
	}
	
	@RequestMapping(value="/logOut/", method = RequestMethod.POST)
	public String logOut(HttpSession session) {
		System.out.println("LOG OUT");
		session.invalidate();
		return "index.jsp";	
	}
}
