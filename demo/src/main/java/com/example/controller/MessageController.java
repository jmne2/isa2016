package com.example.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.models.FriendRequest;
import com.example.models.FriendshipRequest;
import com.example.models.Guest;
import com.example.models.Invitation;
import com.example.models.InvitationReservation;
import com.example.models.Reservation;
import com.example.models.Restaurant;
import com.example.service.FriendshipRequestService;
import com.example.service.InvitationReservationService;
import com.example.service.ReservationService;
import com.example.service.RestaurantService;
import com.example.service.UserService;

@Controller
public class MessageController {

	private ArrayList<String> messages = new ArrayList<String>();
	
	private ArrayList<FriendRequest> requests = new ArrayList<FriendRequest>();
	
	private ArrayList<Invitation> reservationInvitations = new ArrayList<Invitation>();
	
	@Autowired
	private SimpMessagingTemplate template;
	
	@Autowired
	private FriendshipRequestService fr;
	
	@Autowired
	private UserService us;
	
	@Autowired
	private ReservationService rs;
	
	@Autowired
	private InvitationReservationService irs;
	
	@Autowired
	private RestaurantService rest;
	
	@RequestMapping("/send/{username}/invitation")
	public void invitationSender(@PathVariable String username, String param){
		reservationInvitations.clear();
		Guest g = us.findGuestByEmail(param);
		List<InvitationReservation> invitations = irs.findByInvitedAndStatusLike(g.getId(), "pending");
		if(invitations != null){
			for(InvitationReservation it : invitations){
				Reservation reservation = rs.findById(it.getReservation());

				Date sada = new Date();
				if(reservation.getDate().after(sada)){
					Guest reservator = reservation.getReservator();
					Invitation inv = new Invitation();
					inv.setInvitation_id(it.getId());
					inv.setInvited_by(reservator.getId());
					inv.setInvited_by_name(reservator.getFirstName() + " " + reservator.getLastName());
					inv.setRestaurant_name(reservation.getRestaurant());
					Restaurant r = rest.findByName(reservation.getRestaurant());
					inv.setRestaurant_id(r.getId());
					inv.setInvited_id(g.getId());
					reservationInvitations.add(inv);
				} else {
					it.setStatus("passed");
					irs.saveInvitationReservation(it);
				}
			}
		}
	}
	
	@RequestMapping("/send/{username}/friendshippending")
	public void sender(@PathVariable String username, String sender, HttpSession session){
		Guest responser = us.findGuestByEmail(username);
		Guest requester = us.findGuestByEmail(sender);
		FriendshipRequest req = fr.findByRequesterAndResponser(requester.getId(), responser.getId());
		if(req == null){
			req = new FriendshipRequest();
			req.setRequester(requester.getId());
			req.setResponser(responser.getId());
			req.setStatus("pending");
			FriendshipRequest f = fr.saveFriendship(req);
		}
		requests.clear();
		List<FriendshipRequest> list = fr.findByResponserAndStatusLike(responser.getId(), "pending");
		if(list.size() != 0){
			for(FriendshipRequest request : list){
				Guest g1 = us.findGuestById(request.getRequester());
				FriendRequest rrr = new FriendRequest(request.getRequester(), g1.getFirstName() +" "+g1.getLastName(),
						false);
				requests.add(rrr);
			}
		}
		this.template.convertAndSend("/topic/" + username, requests);
	}
	
	@RequestMapping("/send/{username}/message")
	public void test(@PathVariable String username, String mess, HttpSession session){
		messages.add(mess);
		this.template.convertAndSend("/topic/" + username, messages);
	}
	
	@RequestMapping("/topic/{username}/init")
	public void initMessages(@PathVariable String username, String param){
		requests.clear();
		Guest g = us.findGuestByEmail(username);
		List<FriendshipRequest> list = fr.findByResponserAndStatusLike(g.getId(), "pending");
		if(list.size() != 0){
			for(FriendshipRequest f : list){
				Guest g1 = us.findGuestById(f.getRequester());
				FriendRequest r = new FriendRequest(f.getRequester(), g1.getFirstName() +" "+g1.getLastName(),
						false);
				requests.add(r);
			}
		}
		this.template.convertAndSend("/topic/" + username, requests);
	}
}
