<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Konobar</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script  type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			document.getElementById("konobariForm").hidden=true;
			document.getElementById("unosPorudzbine").hidden=false;
			document.getElementById("porudzbine").hidden=true;
			document.getElementById("prikazGrafickiStolova").hidden=true;
			document.getElementById("SviRacuni").hidden = false;
			izlistajStolovePonudu();
			preuzmiGrafStolovi();
			
			
		});
		 
		function info(){
			document.getElementById("konobariForm").hidden=false;
			document.getElementById("unosPorudzbine").hidden = true;
			document.getElementById("porudzbine").hidden=true;
			document.getElementById("prikazGrafickiStolova").hidden=true;
			document.getElementById("SviRacuni").hidden = true;
			
		}
		
		function konobarPocetna(){
			document.getElementById("konobariForm").hidden=true;
			document.getElementById("prikazGrafickiStolova").hidden=true;
			document.getElementById("unosPorudzbine").hidden=false;
			document.getElementById("porudzbine").hidden=true;
			document.getElementById("SviRacuni").hidden = false;
			
		}
		function prikaziGrafStolovi(){
				
				document.getElementById("prikazGrafickiStolova").hidden=false;
				document.getElementById("unosPorudzbine").hidden=true;
				document.getElementById("porudzbine").hidden=true;
				document.getElementById("konobariForm").hidden=true;
				document.getElementById("SviRacuni").hidden = true;
				
		}
		function porudzbine(){
			
			document.getElementById("prikazGrafickiStolova").hidden=true;
			document.getElementById("unosPorudzbine").hidden=true;
			document.getElementById("porudzbine").hidden=false;
			document.getElementById("konobariForm").hidden=true;
			document.getElementById("SviRacuni").hidden = true;
		}
		
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#konobariForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		  
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
			
		function changeWaiter(){
			
			var $form = $("#konobariForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/changeWaiter1/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
						
		         }
	         });
			document.getElementById("konobarForm").hidden=true;
			 alert("Uspesno ste izmenili svoje podatke!");
		}
		
		
		
		function addOrderDrink(){
			var sto = $("#selektovaniSto option:selected").text();
			var pice = $("#selektovanoPice option:selected").text();
			alert("usao u addOrderDrink()")
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/addOrderDrink/"+sto+"/"+pice+"/",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
			
			reloadPorudzbine();
		}
		
		function addDishToOrder(){
			var sto = $("#selektovaniSto option:selected").text();
			var jelo = $("#selektovanoJelo option:selected").text();
			
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/addOrderDish/"+sto+"/"+jelo+"/",
	             complete: function(datas){
	            	 alert(sto)
		         }
	         });
	      
	        location.reload();
		}
		
		function reloadPorudzbine(){
			
			$.ajax({
	             type: "GET",
	             url: "/cookCont/reloadPorudzbine/",
	             complete: function(){
		         }
			});
			location.reload();
		}
		
		function izlistajStolovePonudu(){
			$.ajax({
	             type: "POST",
	             url: "/cookCont/izlastajStolovePonudu/",
	             complete: function(){
		         }
			});
		}
		
		function preuzmiGrafStolovi(){
			$.ajax({
	             type: "POST",
	             url: "/cookCont/prikaziGrafStolovi/",
	             complete: function(){
	            	 
		         }
			});}
	
		

		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}
		
		function obrisiPice(id){
			$.ajax({
		         type: "POST",
		         url: "/cookCont/obrisiPiceIzPorudzbine/"+id+"/",
		         dataType: 'text',
				 contentType:"application/json",
				 success: function(data){
					 if(data=="notOK"){
						 alert("Ne mozete obrisati pice iz porudzbine, pice je u pripremi!");
					 }
		         }
			});
		}
		
		function obrisiHranu(id){
			$.ajax({
		         type: "POST",
		         url: "/cookCont/obrisiHranuIzPorudzbine/"+id+"/",
		         dataType: 'text',
				 contentType:"application/json",
				 success: function(data){
					 if(data=="notOK"){
						 alert("Ne mozete obrisati jelo iz porudzbine, jelo je u pripremi!");
					 }
		         }
			});
		}
		
		function izdajRacun(id){
			
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/izdajRacun/"+id+"/",
	             complete: function(datas){
	     	        location.reload();
		         }
	         });
		}
		

</script>
  </head>
  <body background="images/Elegant_Background-7.jpg">
  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" id = "kuvarButton" onclick = "konobarPocetna()" href="#">Konobar</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li><a href="#"  id="prikazPorudzbina" onclick="porudzbine()" role="presentation">Porudzbine</a></li>
        <li><a href=""  id="rasporedButton" onclick="raspored()" role="presentation">Raspored rada</a></li>
        <li><a href="#"  id="pocetnaButton" onclick="info()" role="presentation">Podaci</a></li>
        <li><a href="#"  id="prikazGrafButton" onclick="prikaziGrafStolovi()" role="presentation">Graficki prikaz stolova</a></li>
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
    </ul>
    </div>
  </div>
</nav>
<div id="prikazGrafickiStolova">
<c:set var="i" scope="session" value="${0}"/>
<c:set var="j" scope="session" value="${0}"/>
<c:set var="reonUKomRadi" value="${LogWaiter.reon}"/>
<c:set var="pusackidELIC" value="${'Pusacki deo'}"/>
<c:set var="nepusackidELIC" value="${'Nepusacki deo'}"/>
<c:set var="bastadELIC" value="${'Basta'}"/>
	<svg width="525" height="1200">



	<text  x="0" y="15" fill="black" style="font-size:20px">Pusacki deo</text>
	<c:set var="j" value="${j+1}"/>
	<c:forEach items="${pusackiSegment}"  var="pusSeg">
		<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
		<c:set var="j"  value="${j+1}"/>
	<!--  	<circle cx="j*100+60" cy="i*100+60" r="40" stroke="green" stroke-width="4" fill="green" /> -->
		</c:if>
		<c:if test="${!(i >4) }"> 	
		
		<c:if test="${reonUKomRadi==pusackidELIC}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="green" stroke-width="4" fill="green" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${pusSeg.tableNum}</text>
		</c:if>
		<c:if test="${!(reonUKomRadi==pusackidELIC)}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="red" stroke-width="4" fill="red" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${pusSeg.tableNum}</text>
		</c:if>
		<c:set var="i"  value="${i+1}"/>
		
		</c:if>
	</c:forEach>
	
	<c:set var="i" value="${0}"/>
	<c:set var="j" value="${j+1}"/>
	
	<text x="0" y="${j*100+60}" fill="black" style="font-size:20px">Nepusacki deo</text>
	<c:set var="j" value="${j+1}"/>
	<c:forEach items="${nepusackiSegment}"  var="nepSeg">
		<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
		<c:set var="j"  value="${j+1}"/>
		
	<!--  	<circle cx="j*100+60" cy="i*100+60" r="40" stroke="green" stroke-width="4" fill="green" /> -->
		</c:if>
		<c:if test="${!(i >4) }"> 	
		<c:if test="${reonUKomRadi==nepusackidELIC}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="green" stroke-width="4" fill="green" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${nepSeg.tableNum}</text>
		</c:if>
		<c:if test="${!(reonUKomRadi==nepusackidELIC)}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="red" stroke-width="4" fill="red" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${nepSeg.tableNum}</text>
		</c:if>
		<c:set var="i"  value="${i+1}"/>
		
		</c:if>
	</c:forEach>
	
	<c:set var="i" value="${0}"/>
	<c:set var="j" value="${j+1}"/>
	<text x="0" y="${j*100+60}" fill="black" style="font-size:20px">Basta</text>
	<c:set var="j" value="${j+1}"/>
	<c:forEach items="${bastaSegment}"  var="basSeg">
		<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
		<c:set var="j"  value="${j+1}"/>
	<!--  	<circle cx="j*100+60" cy="i*100+60" r="40" stroke="green" stroke-width="4" fill="green" /> -->
		</c:if>
		<c:if test="${!(i >4) }"> 	
		
		<c:if test="${reonUKomRadi==bastadELIC}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="green" stroke-width="4" fill="green" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${basSeg.tableNum}</text>
		</c:if>
		<c:if test="${!(reonUKomRadi==bastadELIC)}">
		<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="red" stroke-width="4" fill="red" />
		<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${basSeg.tableNum}</text>
		</c:if>
		<c:set var="i"  value="${i+1}"/>
		
		</c:if>
	</c:forEach>
	<!--  <rect width="980" height="980" style="stroke-width:3;stroke:rgb(0,255,0)" /> -->
   
      Sorry, your browser does not support inline SVG.
	</svg> 
</div>

   	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="konobariForm" role="form" focus="box-shadow">
		  	<div class="form-group" >
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${LogWaiter.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName" value="${LogWaiter.lastName }">
		    </div>
			<div class="form-group">
		    	<label>Lozinka:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="password" value="${LogWaiter.password }">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="clothesSize" value="${LogWaiter.clothesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="shoesSize" value="${LogWaiter.shoesSize }">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeWaiterF" onclick="changeWaiter()" class="btn btn-default"><br><br>
		</form>
		</div>
<div id = "unosPorudzbine" class=" col-md-5 col-md-offset-1 centered">
		Unesi porudzbinu:
		<form id="PorudzbinaForm" role="form" focus="box-shadow">
		 <div class="form-group">
		    	<label>Odaberite sto:</label>
		    	<select type="text" name="role" id="selektovaniSto" class="form-control">
		    		<c:forEach items="${tables}"  var="table">
		    		<option value="${table.tableNum}">${table.tableNum}</option>
		    		</c:forEach>
		    	</select>
		    </div>
			<div class="form-group">
		    	<label>Odaberi jelo:</label>
		    	<select type="text" name="role" id="selektovanoJelo" class="form-control">
		    		<c:forEach items="${dishes}"  var="dish">
		    		<option value="${dish.name}">${dish.name}</option>
		    		</c:forEach>
		    	</select>
		    </div>
		     <input type="submit" value="Dodaj jelo u porudzbinu" id="addDish" onclick="addDishToOrder()" class="btn btn-default"><br><br>
		    <div class="form-group">
		    	<label>Odaberi pice:</label>
		    	<select type="text" name="role" id="selektovanoPice" class="form-control">
		    		<c:forEach items="${drinks}"  var="drink">
		    		<option value="${drink.name}">${drink.name}</option>
		    		</c:forEach>
		    	</select>
		    </div>
		    <input type="submit" value="Dodaj pice u porudzbinu" id="addDrink" onclick="addOrderDrink()" class="btn btn-default"><br><br>
		</form>
		</div>
		
		
		<div id="SviRacuni" class=" col-md-5 col-md-offset-1 centered">
				<div class="form-group">
				<table class="table">
				<tr>
					<th>Izdaj racun za sto:</th>
				</tr>
		    		<c:forEach items="${tablesBill}"  var="tableBill">
		    		<tr>	
						<td><c:out value="${tableBill.tableNum}"/></td>	
						<td><input type="submit" value="Izdaj racun" id="izdajRacun" onclick="izdajRacun(${tableBill.id})" class="btn btn-default"></td>	
					</tr>
		    		</c:forEach>
		    		</table>
		    </div>
		</div>
		
		
		<div id = "porudzbine" >
			<div>
			<table class="table">
			<c:set var="i" scope = "session" value="${0}"/>
			<c:forEach items="${orderedDrinks1}"  var="orderedDrink">
			
			<tr>
				<c:set var="i" scope = "session" value="${i+1}"/>
			</tr>
				<tr>
					<th>Porudzbina:</th>
					<th>Status:</th>
				</tr>
				<c:forEach items="${orderedDrink.getDrinkOrders()}"  var="orderedDrinkForTable">
					<tr>	
						<td><c:out value="${orderedDrinkForTable.drink.name}"/></td>
						<td><c:out value="${orderedDrinkForTable.prepState}"/></td>		
						<td><input type="submit" value="Obrisi" id="obrisiPice" onclick="obrisiPice(${orderedDrinkForTable.id})" class="btn btn-default"></td>	
					</tr>
				</c:forEach>
				
				<c:forEach items="${orderedDrink.getFoodOrders()}"  var="orderedDishForTable">
					<tr>	
						<td><c:out value="${orderedDishForTable.food.name}"/></td>
						<td><c:out value="${orderedDishForTable.prepState}"/></td>		
						<td><input type="submit" value="Obrisi" id="obrisiHranu" onclick="obrisiHranu(${orderedDishForTable.id})" class="btn btn-default"></td>	
					</tr>
				</c:forEach>
					</c:forEach>
			</table>
			</div>
		</div>
  </body>
</html>