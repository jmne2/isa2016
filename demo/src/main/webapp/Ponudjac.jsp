<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<title>Ponudjac</title>
<script  type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			if ("${userId }" === ""){
				window.location="index.jsp";
			}
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allOffersDiv").hidden=true;
			if(${offerHidden2}===false)
				document.getElementById("allRestOrders").hidden=false;
			else
				document.getElementById("allRestOrders").hidden=true;
			if(${offerHidden}===false)
				document.getElementById("offerFormDiv").hidden = false;
			else
				document.getElementById("offerFormDiv").hidden = true;
			if("${curOffer.offerState}"==="NA CEKANJU"){
				document.getElementById("offerForm").hidden = ${offerHidden};
			}
			else{
				document.getElementById("offerForm").hidden = true;
			}
			document.getElementById("offerForm2").hidden = ${offerHidden2};
		});
		
		function saveSupplier() {
				var $form = $("#usrInfoForm");
				var data = getFormData($form);
				var s = JSON.stringify(data);
		        $.ajax({
		             type: "POST",
		             url: "/user/changeSupplier/",
		             data: s,
		             contentType : "application/json",
		             complete: function(datas){
			            	location.reload();
			         }
		         });
	      }  
		function changeOffer(){
			var $form = $("#offerForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/offer/changeOffer/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function saveOffer2(){
			var $form = $("#offerForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/offer/addOffer/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		

		function makeOffer(id, e){
			e.preventDefault();
			$.ajax({
	             type: "GET",
	             url: "/offer/makeOffer/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 if( "${message}" === "exists"){
	            		 alert("Vec ste napravili ponudu za ovu porudzbinu.");
	            	 }
	            	 else{
	            		 location.reload();
	            	 }
		         }
			});
		}
		
		function details(id, e){
			$.ajax({
	             type: "GET",
	             url: "/offer/getDetails/",
	             data: {"id": id},
	             dataType: "application/json",
	             complete: function(datas){
	            	location.reload();
		         }
			});
		}
		function selectOffer(id){
			$.ajax({
	             type: "GET",
	             url: "/offer/selectOffer/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 	});
		    $("#usrInfoForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { saveSupplier(); }
		    });
		    $("#offerForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		    $("#offerForm2").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		    $('.taskPassValidation').each(function () {
		        $(this).rules('add', {
		            required: false,
		            minlength: 5,
		            messages: {
		                minlength: "Sifra mora imati najmanje 5 slova"
		            }
		        });
		    });
		    $('.taskPassConfValidation').each(function () {
		        $(this).rules('add', {
		            required: false,
		            minlength: 5,
		            equalTo: "#password",
		            messages: {
		                equalTo: "Niste uneli istu sifru"
		            }
		        });
		    });
		});
		
</script>
</head>
<body>
<body background="images/Elegant_Background-7.jpg">
	<nav class="navbar navbar-inverse">
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
			<li><a href="#" id="offer" onclick="offer()" role="presentation">Aktivne ponude</a>
			</li>
			<li><a href="#" id="allOffers" onclick="allOffers()" role="presentation">Istorija ponuda</a>
			</li>
			<li><a href="#" id="usrInfo" onclick="usrInfo()" role="presentation">Licni podaci</a>
			</li>
			<li><a href="#" id="porudzbine" onclick="porudzbine()" role="presentation">Porudzbine</a>
			</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
		    </ul>
		</div>
	</nav>
	</br></br>
	<div id="offerFormDiv" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
		<div  class=" col-md-6 col-md-offset-1 centered">
		<ul>
		<c:forEach items="${currOffers}"  var="offer">
			<li><a href="#" onclick="selectOffer(${offer.id})" role="presentation">${offer.id} ${offer.offerState }</a></li>
		</c:forEach></ul>
		</div>
		<div style=" overflow: hidden">
		<form id="offerForm" role="form" focus="box-shadow" hidden="${offerHidden }" >
		  	<div class="form-group">
			    <label>Pocetak ponude:</label>
			    <p>${curOffer.start }</p>
			    <input type="date"  name="start" value="${curOffer.start  }">
			</div>
			<div class="form-group">
			    <label>Kraj ponude:</label>
			    <p>${curOffer.end }</p>
			    <input type="date"  name="end" value="${curOffer.end  }">
			</div>
			<div class="form-group">
		    	<label>Cena:</label>
		    	<input type="number"  name="price" value="${curOffer.price }">
		    </div>
		    <div class="form-group">
			    <label>Rok isporuke:</label>
			    <p>${curOffer.deadline }</p>
			    <input type="date" name="deadline" value="${curOffer.deadline  }">
			</div>
			<div class="form-group">
			    <label>Garancija:</label>
			    <p>${curOffer.garanty }</p>
			    <input type="date"  name="garanty" value="${curOffer.garanty  }">
			</div>
		    <input type="submit" value="Sacuvaj" onclick="changeOffer()" class="btn btn-default"><br><br>
		</form>
		</div>
		</br></br>
	</div>
	<div id="allOffersDiv" class=" col-md-6 col-md-offset-1 centered">
		<div >
			<table class="table">
				<tr>
					<th>ID</th>
					<th>Pocetak</th>
					<th>Kraj</th>
					<th>Cena</th>
					<th>Rok isporuke</th>
					<th>Garancija</th>
					<th>Stanje</th>
				</tr>
				<c:forEach items="${offers}"  var="offer">
					<tr>	
						<td><c:out value="${offer.id}"/></td>				
						<td><c:out value="${offer.start}"/></td>
						<td><c:out value="${offer.end}"/></td>
						<td><c:out value="${offer.price}"/></td>
						<td><c:out value="${offer.deadline}"/></td>
						<td><c:out value="${offer.garanty}"/></td>
						<td><c:out value="${offer.offerState}"/></td>
					</tr>
				</c:forEach>
			</table>
			</div>	
	</div>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="usrInfoForm" role="form" focus="box-shadow">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${user.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName" value="${user.lastName }">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email" class="form-control taskNameValidation" name="email" value="${user.email }">
		    </div>
		    <div class="form-group">
		    	<label>Nova sifra:</label>
		    	 <input type="password" class="form-control taskPassValidation" id="password" name="password" >
		    </div>
		    <div class="form-group">
		    	<label>Ponovi unos sifre:</label>
		    	 <input type="password" class="form-control taskPassConfValidation" name="sifra2">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="saveSupp" class="btn btn-default"><br><br>
		</form>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
	<div id="allRestOrders" class=" col-md-6 col-md-offset-1 centered">
		<div >
			<table class="table">
				<thead>
				<tr>
					<th>ID porudzbine</th>
					<th>Menadzer restorana</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${currOrders}"  var="currOrder">
						<tr>	
							<td><c:out value="${currOrder.id}"/></td>				
							<td><c:out value="${currOrder.restManager.firstName} ${currOrder.restManager.lastName}"/></td>
							<td><a href="#" onclick="details(${currOrder.id}, event)" role="presentation">Detalji</a></td>
							<td><a href="#"  onclick="makeOffer(${currOrder.id}, event)" role="presentation">Napravi ponudu</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
				<ul><c:forEach items="${selOrderItems }" var="item">
					<li><c:out value="${item.name }  ${item.amount }  ${item.metrics }"></c:out></li>
				</c:forEach></ul>
			</div>
			
	</div>
	<div style=" overflow: hidden">
	<form id="offerForm2" role="form" focus="box-shadow" hidden="${offerHidden2 }">
		  	<div class="form-group">
			    <label>Pocetak ponude:</label>
			    <input type="date" class="form-control taskNameValidation" name="start" >
			</div>
			<div class="form-group">
			    <label>Kraj ponude:</label>
			    <input type="date" class="form-control taskNameValidation" name="end" >
			</div>
			<div class="form-group">
		    	<label>Cena:</label>
		    	<input type="Restaurant r2 = rs.findOne(r.getId());" class="form-control taskNameValidation" name="price" >
		    </div>
		    <div class="form-group">
			    <label>Rok isporuke:</label>
			    <input type="date" class="form-control taskNameValidation" name="deadline">
			</div>
			<div class="form-group">
			    <label>Garancija:</label>
			    <input type="date" class="form-control taskNameValidation" name="garanty" >
			</div>
		    <input type="submit" value="Sacuvaj" onclick="saveOffer2()" class="btn btn-default"><br><br>
		</form>	
		</div>
	</div>
	<script>
		function usrInfo(){
			document.getElementById("usrInfoForm").hidden=false;
			document.getElementById("offerFormDiv").hidden=true;
			document.getElementById("allOffersDiv").hidden=true;
			document.getElementById("allRestOrders").hidden=true;
			document.getElementById("offerForm2").hidden = true;
			document.getElementById("offerForm").hidden = true;
		}
		function offer(){
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("offerFormDiv").hidden=false;
			document.getElementById("allOffersDiv").hidden=true;
			document.getElementById("allRestOrders").hidden=true;
			document.getElementById("offerForm2").hidden = true;
			$.ajax({
	             type: "POST",
	             url: "/offer/currentOffer/",
	             complete: function(){
		         }
	         });
		}
		function allOffers(){
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("offerFormDiv").hidden=true;
			document.getElementById("allOffersDiv").hidden=false;
			document.getElementById("allRestOrders").hidden=true;
			document.getElementById("offerForm2").hidden = true;
			document.getElementById("offerForm").hidden = true;
		}
		
		function porudzbine(){
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("offerFormDiv").hidden=true;
			document.getElementById("allOffersDiv").hidden=true;
			document.getElementById("allRestOrders").hidden=false;
			document.getElementById("offerForm").hidden = true;
			
		}

	
	</script>
</body>
</html>