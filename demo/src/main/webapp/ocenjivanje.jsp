<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sanker</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script  type="text/javascript">
	
	
	
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#infoBartenderFrom").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		  
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
		
function Oceni(id){
	var ocenaJela = $("#dish option:selected").text();
	var ocenaUsluge = $("#usluga option:selected").text();
	var ocenaRestorana = $("#restoran option:selected").text();
	
	$.ajax({
        type: "POST",
        url: "/cookCont/Ocena/"+id+"/"+ocenaJela+"/"+ocenaUsluge+"/"+ocenaRestorana+"/",
			contentType:"application/json",
        complete: function(datas){
        	var loc = datas.responseJSON.message;
       	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
       	 window.location = loc2;
        }
	});
}

</script>
  </head>
  <body background="images/Elegant_Background-7.jpg">
  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="" onclick="pocetna(event)">OCENJIVANJE</a>
    </div>
  </div>
</nav>
   
<div class=" col-md-6 col-md-offset-1 centered">
		<form id="infoBartenderFrom" role="form">
		  	<div class="form-group">
			    <label>Ocena za jela:</label>
			    <select type="text" name="role" id="dish" class="form-control">
		    		<c:forEach var="i" begin="1" end="10">
		    		<option value="${i}">${i}</option>
		    		</c:forEach>
		    	</select>
			</div>
			<div class="form-group">
			    <label>Ocena za uslugu:</label>
			    <select type="text" name="role" id="usluga" class="form-control">
		    		<c:forEach var="i" begin="1" end="10">
		    		<option value="${i}">${i}</option>
		    		</c:forEach>
		    	</select>
			</div>
			<div class="form-group">
			    <label>Ocena za restoran:</label>
			    <select type="text" name="role" id="restoran" class="form-control">
		    		<c:forEach var="i" begin="1" end="10">
		    		<option value="${i}">${i}</option>
		    		</c:forEach>
		    	</select>
			</div>
			
		    <input type="submit" value="Oceni" onclick="Oceni(${ocenaAtribut})" class="btn btn-default"><br><br>
		</form>
	</div>
    
  

  <script>

	</script>
  </body>
</html>