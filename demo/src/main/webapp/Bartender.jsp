<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sanker</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script  type="text/javascript">
	
	
	
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#infoBartenderFrom").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		  
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
		
	$(document).ready(function () {
		document.getElementById("infoBartenderFrom").hidden=true;
		document.getElementById("porudzbine").hidden=true;
	});
	
	function changeWaiter(){
			
			var $form = $("#infoBartenderFrom");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/changeBartender1/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	
						
		         }
	         });
		//	document.getElementById("infoBartenderFrom").hidden=true;
			
			 alert("Uspesno ste izmenili svoje podatke!");
		}
		
	function izlistajPorudzbine(){
		$.ajax({
	         type: "GET",
	         url: "/cookCont/izlastajPorudzbineBartender/",
	         contentType:"application/json",
	         complete: function(){
	
	         }
		});
		
	}
	
	
		
	function izmeniPice(id){
		$.ajax({
	         type: "POST",
	         url: "/cookCont/izmeniStatusPica/"+id+"/",
				contentType:"application/json",
	         complete: function(){
	         }
		});
	}
	
	function obrisiPice(id){
		$.ajax({
	         type: "POST",
	         url: "/cookCont/izmeniStatusPicaSpremno/"+id+"/",
				contentType:"application/json",
	         complete: function(){
	         }
		});
	}
	
	function logOut(){
		$.ajax({
             type: "POST",
             url: "/logOut/",
 			contentType:"application/json",
             complete: function(datas){
            	 var loc = datas.responseJSON.message;
            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
            	 window.location = loc2;
	         }
		});
	}
</script>
  </head>
  <body background="images/Elegant_Background-7.jpg">
  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="" onclick="pocetna(event)">Sanker</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
       <li><a href=""  id="rasporedButton" onclick="prikazRada(event)" role="presentation">Raspored rada</a></li>
        <li><a href=""  id="pocetnaButton" onclick="info(event)" role="presentation">Podaci</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="" onclick="logOut()" ><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
      </ul>
    </div>
  </div>
</nav>
   
 <div class=" col-md-6 col-md-offset-1 centered">
		<form id="infoBartenderFrom" role="form">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${LogBartender.firstName }">
			</div>
			<div class="form-group">
			    <label>Prezime:</label>
			    <input type="text" class="form-control taskNameValidation" name="lastName" value="${LogBartender.lastName }">
			</div>
			<div class="form-group has-feedback">
			    <label>Email:</label>
			    <input type="email" class="form-control taskNameValidation" name="email" value="${LogBartender.email }" >
			</div>
			<div class="form-group">
		    	<label>Sifra:</label>
		    	<input type="text" class="form-control taskNameValidation" name="password" value="${LogBartender.password }">
		    </div>
			<div class="form-group">
			    <label>Velic�ina odece:</label>
			    <input type="text" class="form-control taskNameValidation" name="clothesSize" value="${LogBartender.clothesSize }">
			</div>
			<div class="form-group">
			    <label>Velic�ina obuce:</label>
			    <input type="text" class="form-control taskNameValidation" name="shoesSize" value="${LogBartender.shoesSize }">
			</div>
		    <input type="submit" value="Izmeni podatke" onclick="changeWaiter()" class="btn btn-default"><br><br>
		</form>
	</div>
    
    <div class=" col-md-6 col-md-offset-1 centered" id = "porudzbine" >
			<div>
			<table>
					<tr>
						<th>Poruceno pice</th>
						<th>Status</th>
					</tr>
					<c:forEach items="${porucenaPica}"  var="orderedDrink11">
						<tr>	
							<td>${orderedDrink11.drink.name}</td>
							<td>${orderedDrink11.prepState}</td>		
						<!-- 	<td><input type="submit" value="Preuzmi porudzbinu" id="izmeniPice" onclick="izmeniPice(${orderedDrink.id})" class="btn btn-default"></td>  -->
							<td><input type="submit" value="Spremno" id="obrisiPice" onclick="obrisiPice(${orderedDrink11.id})" class="btn btn-default"></td>	
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>

  <script>
		function pocetna(e){
		e.preventDefault()
		izlistajPorudzbine()
		document.getElementById("infoBartenderFrom").hidden=true;
		document.getElementById("porudzbine").hidden=false;
		
		}
	
		function info(e){
			e.preventDefault()
			document.getElementById("infoBartenderFrom").hidden=false;
			document.getElementById("porudzbine").hidden=true;
		}
	</script>
  </body>
</html>