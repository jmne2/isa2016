<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Project: Change pass</title>
<script type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
	function promeni(e){
		e.preventDefault();
		var lozinka = $("#password").val();
		if(lozinka == "")
		{
				alert("Niste uneli lozinku!");
		}
		else if(lozinka == "pass")
		{
			alert("Unesite novu lozinku");
		}
		else if(lozinka != "" && lozinka!="pass"){
			var s = JSON.stringify({
				"password":lozinka,
			});
			
			 $.ajax({
				 type: "POST",
				 url: "user/changePass/",
				 data: {"lozinka":lozinka},
				 contentType : "application/json",
				 complete: function(){
					 window.location="index.jsp";
				 }
				});
/*			 
				var s = JSON.stringify({
					"firstName":"temp",
					"lastName":"temp",
					"email":"${user.email}",
					"password":lozinka,
					
					});
					$.ajax({
					 type: "POST",
					 url: "user/loginGuest/",
					 data: s,
					 contentType : "application/json",
					 complete(datas){
						var loc = datas.responseJSON.message;
		            	var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
		            	window.location = loc2;
		            	window.location = loc2;
					 }
					});
					*/

		}
	}
	
</script>

</head>
<body>
		<br></br><br></br><div align="center">
			<form id="changePass" action="" method="post">
			    <h3>Unesite novu lozinku:</h3><br>
			    <input type="password" name="password" id="password" class="form-control" style="width:40%"><br>
			    <input type="submit" value="Sacuvaj izmenu" id="changePassF" onclick="promeni(event)" class="btn btn-default"><br><br>
			</form> <br>
		</div>
</body>
</html>