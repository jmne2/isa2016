<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Project: Get Your Food With Less Fuss</title>
<script type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			document.getElementById("loginBlock").hidden=false;
			document.getElementById("regGuestForm").hidden=true;
		});
		
		function regGuest(){
			document.getElementById("regGuestForm").hidden=false;
			document.getElementById("loginBlock").hidden=true;
		}
</script>

</head>
<body>
	<br></br><br></br>
	<div class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; ">
		<div class=" col-md-6 col-md" id="loginBlock" >
			<form id="logGuestForm" role="form" action="" method="post">
				<fieldset>
					<label>Email:</label><br>
					<input type="text" class="form-control" name="email" id="logEmail"  ><br>
					<label>Lozinka:</label><br>
					<input type="password" class="form-control" name="password" id="logPass"><br>
					<input class="btn btn-lg btn-primary" type="submit" value="Prijavi se">
				</fieldset>
			</form> <br>
			<div id="regPrompt">
			<label><h4>Nov korisnik? Registruj se!</h4></label> <button id="regButton" class="btn btn-lg btn-primary" onclick="regGuest()">Registrovanje</button>
			</div>
		</div>
		<div>
			<form id="regGuestForm" action="" method="post">
			    <label>Ime:</label><br>
			    <input type="text" name="firstName" class="form-control" id="firstName"><br>
			    <label>Prezime:</label><br>
			    <input type="text" name="lastName" class="form-control" id="lastName"><br>
			    <label>Email:</label><br>
			    <input type="text" name="email" class="form-control" id="email"> <br>
			    <div id="errorEmail" hidden="true" style="color: red"> Niste uneli email u adekvatnom obliku</div>
			    <label>Lozinka:</label><br>
			    <input type="password" id="pass" class="form-control" name="password"> <br>
			    <label>Potvrdi lozinku:</label> <br>
			    <input type="password" class="form-control" id="confirmPass"><br>
			    <div id="errorPass" hidden="true" style="color: red"> Niste uneli istu lozinku</div>
			    <button class="btn btn-lg btn-primary" type="submit">Potvrdi</button><br><br>
			</form> <br>
		</div>
	</div>
	<script type="text/javascript">
	
	$(document).on('submit','#logGuestForm',function(e){
		e.preventDefault();
		var lozinka = $("#logPass").val();
		var email = $("#logEmail").val();
		var x;
		var y;
		if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
		}
		function showPosition(position) {
		    x = + position.coords.latitude;
		    y = position.coords.longitude;
		}
		var s = JSON.stringify({
			"firstName":"temp",
			"lastName":"temp",
			"email":email,
			"password":lozinka,
			"accountState":2,
			"locationX":x,
			"locationY":y
		});
		//alert("saljem " + s);
		 $.ajax({
             type: "POST",
             url: "user/loginGuest/",
             data: s,
             contentType : "application/json",
             complete: function(datas){
            	 var loc = datas.responseJSON.message;
            	 if(loc === "null"){
            		 alert("Uneti korisnik ne postoji u bazi.");
            		 location.reload();
            	 }
            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
            	 window.location = loc2;
	         }
         });
	});
	
		$(document).on('submit','#regGuestForm',function(e){
			e.preventDefault();
			console.log("registracija");
			var lozinka = $("#pass").val();
			var ime = $("#firstName").val();
			var prezime = $("#lastName").val();
			var email = $("#email").val();
			var lozinka2 = $("#confirmPass").val();
			var regexEmail = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");
			
			document.getElementById("errorPass").hidden=true;
			document.getElementById("errorEmail").hidden=true;
			//provere unosa
			if(ime == ""){
				alert("Niste uneli ime!");
			}
			else if(prezime == ""){
				alert("Niste uneli prezime!");
			}
			else if(email == ""){
				alert("Niste uneli email!");
			}
			else if(!regexEmail.test(email)){
				document.getElementById("errorEmail").hidden=false;
			}
			else if(lozinka == ""){
				alert("Niste uneli lozinku!");
			}
			else if (lozinka2 !== lozinka){
				document.getElementById("errorPass").hidden=false;
			}
			else {
				var s = formToJSON(ime, prezime, email, lozinka);
				$.ajax({
					type : 'POST',
					url : '/user/addGuest/',
					contentType : 'application/json',
					dataType : "text",
					data : s,
					success : function(data) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert("AJAX ERROR: " + errorThrown);
					}
				});		
			}
		});
		
		
		
		function formToJSON(ime, prezime, email, lozinka) {
			return JSON.stringify({
				"firstName":ime,
				"lastName": prezime,
				"email" : email,
				"password" : lozinka,
				"type":"GUEST"
			});
		}
	</script>
</body>

</html>