<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/bar.css">
<link rel="stylesheet" href="css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<title>Izvestaji</title>
<script type="text/javascript">
	function getFormData($form){
	    var unindexed_array = $form.serializeArray();
	    var indexed_array = {};
	
	    $.map(unindexed_array, function(n, i){
	        indexed_array[n['name']] = n['value'];
	    });
	
	    return indexed_array;
	}
	$(document).ready(function () {
		document.getElementById("restoranIzDiv").hidden=${restIzDivHidden};
		document.getElementById("jeloIzDiv").hidden=${jeloIzDivHidden};
		document.getElementById("konobarIzDiv").hidden=${konobarIzDivHidden};
		document.getElementById("grafikDiv").hidden=${grafikDivHidden};
		document.getElementById("grafikDiv2").hidden=${grafikDiv2Hidden};
		document.getElementById("prihodiDiv").hidden=${prihodiDivHidden};
		document.getElementById("prihodiKonDiv").hidden=${prihodiKonDivHidden};
	});
	
	function izaberiJelo(e){
		e.preventDefault();
		var $form = $("#jeloForm");
		var data = getFormData($form);
		var s = JSON.stringify(data);
        $.ajax({
             type: "POST",
             url: "/reports/jeloForm/",
             data: s,
             contentType : "application/json",
             complete: function(datas){
            	 location.reload();
	         }
         });
	}
	function izaberiJelo2(e){
		e.preventDefault();
		var $form = $("#jeloForm2");
		var data = getFormData($form);
		var s = JSON.stringify(data);
        $.ajax({
             type: "POST",
             url: "/reports/jeloForm2/",
             data: s,
             contentType : "application/json",
             complete: function(datas){
            	 location.reload();
	         }
         });
	}
	
	function selKonobar(id, e){
		e.preventDefault();
		$.ajax({
             type: "GET",
             url: "/reports/selKonobar/",
             data: {"id": id},
 			contentType:"application/json",
             complete: function(){
            	 location.reload();
	         }
		});
	}
	function danIzabran(){
		var d = $("#danZaGrafik").val();
		$.ajax({
            type: "GET",
            url: "/reports/dayChosen/",
            data: {"date": d},
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	function prihodiIz(){
		var d1 = $("#pocetakDatum").val();
		var d2 = $("#krajDatum").val();
		$.ajax({
            type: "GET",
            url: "/reports/prihodiIzvestaj/"+d1+"/"+d2,
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	function prihodiKonIz(){
		var d = $("#konSel").val();
		$.ajax({
            type: "GET",
            url: "/reports/prihodiKonIzvestaj/"+d,
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
</script>
</head>
<body>
<body background="images/Elegant_Background-7.jpg">
  <nav class="navbar navbar-inverse">
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
		<li><a href="#" id="restoranIz" onclick="restoran()" role="presentation">Izvestaj za restoran</a>
		</li>
		<li><a href="#" id="jeloIz" onclick="jelo()" role="presentation">Izvestaj za jelo</a>
		</li>
		<li><a href="#" id="konobarIz" onclick="konobar()" role="presentation">Izvestaj za konobara</a>
		</li>
		<li><a href="#" id="graf" onclick="grafik()" role="presentation">Dnevna posecenost</a>
		</li>
		<li><a href="#" id="graf" onclick="grafik2()" role="presentation">Nedeljna posecenost</a>
		</li>
		<li><a href="#" id="prihodi" onclick="prihodi()" role="presentation">Prihodi restorana</a>
		</li>
		<li><a href="#" id="prihodiKon" onclick="prihodiKon()" role="presentation">Prihodi po konobaru</a>
		</li>
		<li><a href="#" id="nazad" onclick="nazad()" role="presentation">Nazad</a>
		</li>
		</ul>
	</div>
	</nav>
	<br></br><br></br>
	<div id="restoranIzDiv">
		</br></br>
		<h4>Ocena restorana je : ${ocenaRest }</h4>
	</div>
	<div id="jeloIzDiv" class=" col-md-4 col-md-offset-1 centered">
		</br></br>
		<form id="jeloForm" role="form" focus="box-shadow" >
		  	<div class="form-group" >
			    <label>Unesite naziv jela:</label>
			    <input type="text" name="name" class="form-control" >
			</div>
		    <input type="submit" value="Izaberi" id="izaberiJeloF" onclick="izaberiJelo(event)" class="btn btn-default"><br><br>
		</form>
		<div id="jeloIzDiv2">
			</br></br>
			<form id="jeloForm2" role="form" focus="box-shadow">
				<select type="text" name="name" class="form-control" >
					<c:forEach items="${dishes1}"  var="dish">
						<option><c:out value="${dish.name}"/></option>
					</c:forEach>	
				</select>
				<input type="submit" value="Izaberi" id="izaberiJeloF2" onclick="izaberiJelo2(event)" class="btn btn-default"><br><br>
			</form>			
		</div>
		</br></br>
		<h4>Ocena jela je : ${selDish.rate}</h4>
	</div>
	<div id="konobarIzDiv" class=" col-md-4 col-md-offset-1 centered">
		</br></br>
		<h5>Selektujte konobara: </h5>
		<c:forEach items="${konobari}"  var="konobar">
			<a href="#" role="presentation" onclick="selKonobar(${konobar.id}, event)" id="konobarSel" >${konobar.firstName }  ${konobar.lastName }</a></br>
		</c:forEach>
		</br><h5>Ocena konobara je : ${selWaiter.grade}</h5>
	</div>
	<div id="grafikDiv" >
		<label style="margin: 10px">Izaberite datum</label>
		<input type="date" id="danZaGrafik" class="form-control taskNameValidation" style="width: 20%; margin:10px">
		</br><button type="button" onclick="danIzabran()" class="btn btn-default" style="margin:10px">Izaberi</button>
		<div class="chart">
		<h4>Dnevna posecenost restorana za dan ${datumDanas }</h4>
		<table id="data-table" border="1" cellpadding="10" cellspacing="0" summary="SUMMARY NE ZNAM STA JE 2">
		    <thead>
		      <tr>
		         <td>&nbsp;</td>
		         <th scope="col"></th>
		      </tr>
		    </thead>
		    <tbody>
		    	<c:set var="ind" value="${9 }"/>
		    	<c:forEach items="${inc}"  var="inIt">
		      		<tr>
				         <th scope="row">${ind }:00</th>
				         <td>${inIt }</td>
				    </tr>
				    <c:set var="ind" value="${ind+1 }" />
				</c:forEach>
		    </tbody>
		  </table>
  		</div>
	</div>
	<div class="chart2" id="grafikDiv2" >
		<h4>Nedeljna posecenost restorana za period od ${startDate } do ${endDate } </h4>
		<table id="data-table2" border="1" cellpadding="10" cellspacing="0" summary="SUMMARY NE ZNAM STA JE ">
    <thead>
      <tr>
         <td>&nbsp;</td>
         <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     	<tr>
	         <th scope="row">Nedelja</th>
	         <td>${inc0 }</td> 
	    </tr>
	    <tr>
	         <th scope="row">Ponedeljak</th>
	         <td>${inc1 }</td>
	    </tr>
	    <tr>
	         <th scope="row">Utorak</th>
	         <td>${inc2 }</td>
	    </tr>
	    <tr>
	         <th scope="row">Sreda</th>
	         <td>${inc3 }</td>
	    </tr>
	    <tr>
	         <th scope="row">Cetvrtak</th>
	         <td>${inc4 }</td>
	    </tr>
	    <tr>
	         <th scope="row">Petak</th>
	         <td>${inc5 }</td>
	    </tr>
	    <tr>
	         <th scope="row">Subota</th>
	         <td>${inc6 }</td>
	    </tr>
    </tbody>
  </table>
	</div>
	<div id="prihodiDiv" class=" col-md-4 col-md-offset-1 centered">
		<h5>Izaberite period za racunanje prihoda: </h5><br></br>
		<label>Pocetak</label>
		<input type="date" id="pocetakDatum" class="form-control"/>
		<label>Kraj</label>
		<input type="date" id="krajDatum" class="form-control"/></br>
		<input type="button" onclick="prihodiIz()" value="Prikazi prihod" class="btn btn-default"/>
		<br></br><h5>Prihod za selektovani period je ${totalPrihod }</h5>
	</div>
	<div id="prihodiKonDiv" class=" col-md-4 col-md-offset-1 centered">
		<h5>Izaberite konobara: </h5></br>
		<select type="text" id="konSel"  class="form-control">
			<c:forEach items="${waiters }"  var="waiter">
	      		<option value="${waiter.id }">${waiter.firstName } ${waiter.lastName }</option>
			</c:forEach>
		</select>
		</br><input type="button" onclick="prihodiKonIz()" value="Prikazi prihod" class="btn btn-default"/>
		<br></br><h5>Prihod za selektovanog konobara je ${prihodKonobara }</h5> 
	</div>
<script>
	function restoran(){
		$.ajax({
            type: "GET",
            url: "/reports/restIzvestaj/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	function jelo(){
		$.ajax({
            type: "GET",
            url: "/reports/jeloIzvestajSet/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	function konobar(){
		$.ajax({
            type: "GET",
            url: "/reports/konobarIzvestajSet/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	
	function grafik(){
		$.ajax({
            type: "GET",
            url: "/reports/grafikSet/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	
	function grafik2(){
		$.ajax({
            type: "GET",
            url: "/reports/grafikSet2/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	
	function prihodi(){
		$.ajax({
            type: "GET",
            url: "/reports/prihodiSet/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	
	function prihodiKon(){
		$.ajax({
            type: "GET",
            url: "/reports/prihodiKonSet/",
			contentType:"application/json",
            complete: function(){
            	location.reload();
	         }
		});
	}
	
	function nazad(){
		$.ajax({
            type: "POST",
            url: "/reports/redirectNazad/",
			contentType:"application/json",
			complete: function(datas){
	           	 var loc = datas.responseJSON.message;
	           	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	           	 window.location = loc2;
		    }
		});
	}
</script>
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="js/graph.js"></script>
</body>
</html>