<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<!-- Koristi sockjs biblioteku da se konektuje na /stomp endpoint -->
<script type="text/javascript" src="//cdn.jsdelivr.net/sockjs/1.0.3/sockjs.min.js">
</script>
<!-- Koristi stomp biblioteku da se pretplati na brokerov /topic/message endpoint -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
<title>Rezervacija</title>
<script  type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
		var stompClient = null;
		var dishes = [];
		var drinks = [];
		var friends = [];
		var tableId = [];
		var timestampData;
		var chairCnt = 0;
		$(document).ready(function () {
			document.getElementById("popUpDiv").hidden = true;
			document.getElementById("popUpDishDiv").hidden = true;
			document.getElementById("popUpDrinkDiv").hidden = true;
			document.getElementById("friendDiv").hidden = true;
			document.getElementById("dishDiv").hidden = true;
			document.getElementById("drinkDiv").hidden = true;
			var socket = new SockJS('/stomp');
			stompClient = Stomp.over(socket);
			stompClient.connect({}, function(frame) {
				stompClient.subscribe("/topic/${restForReserv.id}/chairs", function(data) {
					var pr = JSON.parse(data.body);
						if(timestampData != null){
							var selDate = timestampData["date"];
							selDate = selDate.split("-");
							var d = new Date(selDate[0], selDate[1]-1, selDate[2]);
							var reservationDate = pr.date;
							reservationDate = reservationDate.split('-');
							var toCompare = new Date(reservationDate[0], reservationDate[1]-1, reservationDate[2]);
							if(d.getTime() === toCompare.getTime()){
								var start = timestampData["start"].split(':');
								var end = timestampData["end"].split(':');
								if(pr.end >= start[0] || pr.end <= end[0] || pr.start >= start[0]){
									var tableLink = $("a[name='" + pr.table_id+"']").last();
									$(tableLink).children().eq(0).attr('fill', 'red');
									if($.inArray(pr.table_id, tableId) != -1){
										var ind = $.inArray(pr.table_id, tableId);
										tableId.splice(ind, 1);
										chairCnt -= cnt;
									}
								}
							}
						}					
					});
				});		
			fixTime();
			});
		function fixTime(){
			var currentTime = new Date();
			var openingTime = new Date();
			var closingTime = new Date();
			openingTime.setHours(9,0,0);
			closingTime.setHours(23,0,0);
			var difference1 = (currentTime-openingTime) / 1000 /60 /60;
			var difference2 = (closingTime - currentTime) / 1000 /60 /60;
			if(difference2 > 0 ){
				for(i = 0; i < 12; i++){
					var currTime = new Date();
					currTime.setHours(currentTime.getHours()+i, 0,0);
					difference2 = (closingTime - currTime) / 1000 /60 /60;
					if(difference2 > 0){
						var value = currTime.getHours() + ":" + currTime.getMinutes();
						var option = "<option value="+ value + ">"+ currTime.getHours() + ":" + currTime.getMinutes() + "</option>";
						$("#startTime").append(option);
						if(i > 1){
							$("#endTime").append(option);
						}
					}
				}	
			}
		}
		$(document).on('click','#closePopup',function(e){
			e.preventDefault();
			document.getElementById("popUpDiv").hidden = true;
		});
		$(document).on('click','#closePopup1',function(e){
			e.preventDefault();
			document.getElementById("popUpDishDiv").hidden = true;
		});
		$(document).on('click','#closePopup2',function(e){
			e.preventDefault();
			document.getElementById("popUpDrinkDiv").hidden = true;
		});
		$(document).on('click','#addFriends',function(e){
			e.preventDefault();
			document.getElementById("popUpDiv").hidden = false;
		});
		$(document).on('click','#addDishes',function(e){
			e.preventDefault();
			document.getElementById("popUpDishDiv").hidden = false;
		});
		$(document).on('click','#addDrinks',function(e){
			e.preventDefault();
			document.getElementById("popUpDrinkDiv").hidden = false;
		});
		$(document).on('click','#addTo',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = document.getElementById(idDiva);
			$(div).find("#addTo").remove();
			var aaa = "<a href=\"\" id=\"removeFromList\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Ukloni</a>";
			$(div).append(aaa);
			$("#friendList").append(div);
		});
		$(document).on('click','#addDishTo',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#popUpDishDiv").find("#" + idDiva);
			$(div).find("#addDishTo").remove();
			var aaa = "<a href=\"\" id=\"removeDishFromList\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Ukloni</a>";
			$(div).append(aaa);
			$("#dishList").append(div);
		});
		$(document).on('click','#addDrinkTo',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#popUpDrinkDiv").find("#" + idDiva);
			$(div).find("#addDrinkTo").remove();
			var aaa = "<a href=\"\" id=\"removeDrinkFromList\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Ukloni</a>";
			$(div).append(aaa);
			$("#drinkList").append(div);
		});
		$(document).on('click','#removeFromList',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = document.getElementById(idDiva);
			$("#friendList").find(div).remove();
			$(div).find("#removeFromList").remove();
			var aaa = "<a href=\"\" id=\"addTo\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Dodaj</a>";
			$(div).append(aaa);
			$("#popUpDiv").append(div);
		});
		$(document).on('click','#removeDishFromList',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#dishList").find("#" + idDiva);
			$("#dishList").find(div).remove();
			$(div).find("#removeDishFromList").remove();
			var aaa = "<a href=\"\" id=\"addDishTo\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Dodaj</a>";
			$(div).append(aaa);
			$("#popUpDishDiv").append(div);
		});
		$(document).on('click','#removeDrinkFromList',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#drinkList").find("#" + idDiva);
			$("#drinkList").find(div).remove();
			$(div).find("#removeDrinkFromList").remove();
			var aaa = "<a href=\"\" id=\"addDrinkTo\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Dodaj</a>";
			$(div).append(aaa);
			$("#popUpDrinkDiv").append(div);
		});
		$(document).on('click','#sendFriendsReservation',function(e){
			e.preventDefault();
			friends.length = 0;
			if(tableId.length != 0){
				var mylist = $("#friendList");
				var listitems = mylist.children().get();
				if(listitems != null){
					 $.each(listitems, function(idx, itm){
						   var nesto = listitems[idx];
						   friends.push($(nesto).attr("id"));
					   });
					}
				if((chairCnt-1) < friends.length){
					alert("Potrebno Vam je jos mesta za pozvane goste");
				} else {
					var email = "${user.email}";
					var reserv = JSON.stringify({
						"prePrepared" : timestampData["prePrepared"],
						"restaurantId" : ${restForReserv.id},
						"dishes" : dishes,
						"drinks" : drinks,
						"friends" : friends,
						"reserverId" : email,
						"tableId" : tableId,
						"start" : timestampData["start"],
						"end" : timestampData["end"],
						"date" : timestampData["date"]
					});
					$.ajax({
			             type: "POST",
			             url: "/guestMain/setReservation/",
			             data: reserv,
			             contentType : "application/json",
			             complete: function(datas){
			            	 var loc = datas.responseJSON.message;
			            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
			            	 window.location = loc2;
				         }
			         });
				}
			}
		});
		$(document).on('click','#sendDishReservation',function(e){
			e.preventDefault();
			var mylist = $("#dishList");
			var listitems = mylist.children().get();
			if(listitems != null){
			 $.each(listitems, function(idx, itm){
				   var nesto = listitems[idx];
				   dishes.push($(nesto).attr("id"));
			   });
			 if(dishes.length != 0){
				document.getElementById("friendDiv").hidden = true;
				document.getElementById("dishDiv").hidden = true;
				document.getElementById("drinkDiv").hidden = false;
				document.getElementById("usrInfoForm").hidden = true;
				$("body").find("#sendDishReservation").remove();
				var sendDrin = "<a href=\"\" id=\"sendDrinkReservation\" class=\"btn btn-lg btn-primary\" style=\"position: absolute; bottom:0px; left:10px\">Dalje</a>";
				$("body").append(sendDrin);
			 }
			}
		});
		$(document).on('click','#sendDrinkReservation',function(e){
			e.preventDefault();
			var mylist = $("#drinkList");
			var listitems = mylist.children().get();
			if(listitems != null){
			 $.each(listitems, function(idx, itm){
				   var nesto = listitems[idx];
				   drinks.push($(nesto).attr("id"));
			   });
			}
			 	document.getElementById("dishDiv").hidden = true;
				document.getElementById("drinkDiv").hidden = true;
				//document.getElementById("sendDishReservation").hidden = true;
				document.getElementById("friendDiv").hidden = false;
				document.getElementById("usrInfoForm").hidden = true;
				var sendBtn = "<a href=\"\" id=\"sendFriendsReservation\" class=\"btn btn-lg btn-primary\" style=\"position: absolute; bottom:0px; left:10px\">Posalji rezervaciju</a>";
				$("body").find("#sendDrinkReservation").remove();
				$("body").append(sendBtn);
		});
		$(document).on('click','#tableLink',function(e){
			e.preventDefault();
			var id = $(this).attr("name");
			var cnt = $(this).attr("href");
			var colour = $(this).children().eq(0).attr('fill');
			if(colour != 'red'){
				if($.inArray(id, tableId) == -1){
					$(this).children().eq(0).attr('fill', 'green');
					tableId.push(id);
					chairCnt += cnt;
				} else {
					$(this).children().eq(0).attr('fill', 'yellow');
					var ind = $.inArray(id, tableId);
					tableId.splice(ind, 1);
					chairCnt -= cnt;
				}
			}
		});
		
		function saveTimestamp() {
				var $form = $("#usrInfoForm");
				var data = getFormData($form);
				timestampData = data;
				var current = new Date();
				//var d = new Date(year, month, day, hours, minutes, seconds, milliseconds);
				var selDate = timestampData["date"];
				selDate = selDate.split("-");
				var d = new Date(selDate[0], selDate[1]-1, selDate[2]);
				if(d < current){
					alert("Izaberite datum koji nije prosao.");
				} else {
					document.getElementById("friendDiv").hidden = true;
					document.getElementById("usrInfoForm").hidden = true;
					document.getElementById("dishDiv").hidden = false;
					document.getElementById("drinkDiv").hidden = true;
					var sendBtn = "<a href=\"\" id=\"sendDishReservation\" class=\"btn btn-lg btn-primary\" style=\"position: absolute; bottom:0px; left:10px\">Dalje</a>"
					$("body").append(sendBtn);
				}
	      }  
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 	});
		    $("#usrInfoForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) {}
		    });
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
</script>
</head>
<body>
	</br>
	<div class="container-fluid">
		<ul class="nav nav-pills">
		<li><button type="button" class="btn btn-lg btn-primary" disabled="disabled">${user.firstName} ${user.lastName}</button>
		</li>
		<li><button type="button" class="btn btn-lg btn-warning" disabled="disabled">${restForReserv.name}</button>
		</li>
		</ul>
	</div>
	</br></br>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="usrInfoForm" role="form" focus="box-shadow">
		  	<div class="form-group">
		  	<div class="input-group date" id="datetimepicker">
			    <label>Datum:</label>
			    <input type="date" class="form-control taskNameValidation" name="date">
			   </div>
			</div>
			<div class="form-group">
		    	<label>Vreme dolaska:</label>
		    	 <select id="startTime" name="start">
		       	</select>
		    </div>
		    <div class="form-group">
		    	<label>Vreme odlaska:</label>
			    	<select id="endTime" name="end">
		       		</select>
		    </div>
		    <div class="form-group">
		    <label>Hrana da bude unapred pripremljena:</label><br/>
		    <label class="btn btn-primary">
		        <input type="radio" class="form-control taskNameValidation" value="true" name="prePrepared" /> Da
  			</label>
  			<label class="btn btn-primary">
			    <input type="radio" class="form-control taskNameValidation" value="false" name="prePrepared" /> Ne
  			</label>
			</div>
		    <input type="submit" value="Sacuvaj izmene" id="saveTimeStamp" onclick="saveTimestamp()" class="btn btn-default"><br><br>
		</form>
	</div>
	<div id="dishDiv" class=" col-md-6 col-md-offset-1 centered">
		<a href="" id="addDishes" class="btn btn-lg btn-primary" >Dodaj jela</a>
		<h3>Spisak jela</h3>
		<div id="dishList" class=" col-md-6 col-md-offset-2 centered">
		</div>
	</div>
	<div id="drinkDiv" class=" col-md-6 col-md-offset-1 centered">
		<a href="" id="addDrinks" class="btn btn-lg btn-primary" >Dodaj pica</a>
		<h3>Spisak pica</h3>
		<div id="drinkList" class=" col-md-6 col-md-offset-2 centered">
		</div>
	</div>
	<div id="friendDiv" class=" col-md-6 col-md-offset-1 centered">
		<a href="" id="addFriends" class="btn btn-lg btn-primary" >Dodaj prijatelje</a>
		<br />
		<h3>Spisak prijatelja</h3>
		<div id="friendList" class=" col-md-6 col-md-offset-2 centered">
		</div>
	</div>
	<div id="popUpDiv" class=" col-md-6 col-md-offset-1 centered" style="z-index:100; position: absolute; left:500px;width: 300px;
    height: 420px;background-color:#FFFFFF;border:1px solid #999999;">
	    <c:forEach items="${friends}" var="friend">
				<div id="${friend.email}" class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${friend.firstName}" /></div>
					<div><c:out value="${friend.lastName}" /></div>
					<a href="" id="addTo" name="${friend.email}" class="btn btn-lg btn-primary">Dodaj</a>
				</div>
		</c:forEach>
		<a href="" id="closePopup" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; right:10px">Zavrsi</a>
	</div>
	
	<div id="popUpDishDiv" class=" col-md-6 col-md-offset-1 centered" style="z-index:100; position: absolute; left:500px;width: 300px;
    height: 420px;background-color:#FFFFFF;border:1px solid #999999;">
	    <c:forEach items="${dishes}" var="dish">
				<div id="${dish.id}" class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${dish.name}" /></div>
					<div><c:out value="${dish.price}" /></div>
					<a href="" id="addDishTo" name="${dish.id}" class="btn btn-lg btn-primary">Dodaj</a>
				</div>
		</c:forEach>
		<a href="" id="closePopup1" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; right:10px">Zavrsi</a>
	</div>
	<div id="popUpDrinkDiv" class=" col-md-6 col-md-offset-1 centered" style="z-index:100; position: absolute; left:500px;width: 300px;
    height: 420px;background-color:#FFFFFF;border:1px solid #999999;">
	    <c:forEach items="${drinks}" var="drink">
				<div id="${drink.id}" class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${drink.name}" /></div>
					<div><c:out value="${drink.price}" /></div>
					<a href="" id="addDrinkTo" name="${drink.id}" class="btn btn-lg btn-primary">Dodaj</a>
				</div>
		</c:forEach>
		<a href="" id="closePopup2" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; right:10px">Zavrsi</a>
	</div>
	<div id="prikazGrafickiStolova">
	<c:set var="i" scope="session" value="${0}"/>
	<c:set var="j" scope="session" value="${0}"/>
	<c:set var="reonUKomRadi" value="${LogWaiter.reon}"/>
	<c:set var="pusackidELIC" value="${'Pusacki deo'}"/>
	<c:set var="nepusackidELIC" value="${'Nepusacki deo'}"/>
	<c:set var="bastadELIC" value="${'Basta'}"/>
		<svg width="525" height="1200">
	
		<text  x="0" y="15" fill="black" style="font-size:20px">Pusacki deo</text>
		<c:set var="j" value="${j+1}"/>
		<c:forEach items="${pusackiSegment}"  var="pusSeg">
			<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
			<c:set var="j"  value="${j+1}"/>
			</c:if>
			<c:if test="${!(i >4) }"> 
			<a href="${pusSeg.chairCount}" id="tableLink" name="${pusSeg.id}">	
			<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="green" stroke-width="4" fill="yellow" />
			<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${pusSeg.tableNum}</text>
			</a>
			<c:set var="i"  value="${i+1}"/>
			</c:if>
		</c:forEach>
		
		<c:set var="i" value="${0}"/>
		<c:set var="j" value="${j+1}"/>
		
		<text x="0" y="${j*100+60}" fill="black" style="font-size:20px">Nepusacki deo</text>
		<c:set var="j" value="${j+1}"/>
		<c:forEach items="${nepusackiSegment}"  var="nepSeg">
			<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
			<c:set var="j"  value="${j+1}"/>
			
		<!--  	<circle cx="j*100+60" cy="i*100+60" r="40" stroke="green" stroke-width="4" fill="green" /> -->
			</c:if>
			<c:if test="${!(i >4) }">
			<a href="${nepSeg.chairCount}" id="tableLink" name="${nepSeg.id}"> 	
			<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="red" stroke-width="4" fill="yellow" />
			<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${nepSeg.tableNum}</text>
			</a>
			<c:set var="i"  value="${i+1}"/>
			
			</c:if>
		</c:forEach>
		
		<c:set var="i" value="${0}"/>
		<c:set var="j" value="${j+1}"/>
		<text x="0" y="${j*100+60}" fill="black" style="font-size:20px">Basta</text>
		<c:set var="j" value="${j+1}"/>
		<c:forEach items="${bastaSegment}"  var="basSeg">
			<c:if test="${i >4 }"> <c:set var="i" value="${0}"/>	
			<c:set var="j"  value="${j+1}"/>
			</c:if>
			<c:if test="${!(i >4) }"> 	
			<a href="${basSeg.chairCount}" id="tableLink" name="${basSeg.id}">
			<circle cx="${ i*100+60}" cy="${j*100+60}" r="40" stroke="green" stroke-width="4" fill="yellow" />
			<text  x="${ i*100+60-30}" y="${j*100+60}" fill="black" style="font-size:20px">STO ${basSeg.tableNum}</text>
			</a>
			<c:set var="i"  value="${i+1}"/>
			
			</c:if>
		</c:forEach>
	   
	      Sorry, your browser does not support inline SVG.
		</svg> 
	</div>
		
		
</body>
</html>