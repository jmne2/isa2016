<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html"charset="ISO-8859-1" >

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" language="javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" language="javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js" language="javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js" language="javascript"></script>
<title>Menadzer sistema</title>
<script  type="text/javascript" language="javascript">

		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			var isHead = "${isHeadAdmin}";
			if(isHead === "false"){
				document.getElementById("manButton").remove();
			}
			document.getElementById("regRestFormDiv").hidden =${mapaHidden};
			document.getElementById("regRestMenFormDiv").hidden=true;
			document.getElementById("regMenForm").hidden=true;
		});

		
		function registerRest() {
			var $form = $("#regRestForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/rest/addRestaurant",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
	      }  
		
		function registerRestMen(e) {
			event.preventDefault();
			var $form = $("#regRestMenForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/user/addRestMan/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
	            	 document.getElementById("regRestMenFormDiv").hidden=false;
	            	 var dd = $("#chooseRestForm option:selected").text();
	     	        $.ajax({
	     	             type: "GET",
	     	             url: "/rest/chooseRest/" + dd + "/",
	     	             contentType : "application/json",
	     	             complete: function(datas){
	     	            	 	document.getElementById("regRestMenFormDiv").hidden=false;
	     	            	 	alert("DODAT MENADZER RESTORANA");
	     		         }
	     	         });
	             }
	         });
	        
	      }  
		
		function registerMen() {
			var $form = $("#regMenForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/user/addAdmin/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
	            	 location.reload();
		         }
	         });
	      }  
		function changedPosition(p1, p2){
			$.ajax({
		        type: "GET",
		        url: "/rest/changePosition/"+p1+"/"+p2+"/",
				contentType:"application/json",
		        complete: function(){
		        }
			});
		}
		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#regRestForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { registerRest(); }
		    });
		  
		    $("#regMenForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { registerMen(); }
		    });
		    
		    $("#regRestMenForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { registerRestMen(event); }
		    });
		    
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
</script>
</head>
<body>
<body background="images/Elegant_Background-7.jpg">
	<nav class="navbar navbar-inverse">
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
			<li><a href="#" id="restButton" onclick="rest()" role="presentation">Registrovanje restorana</a>
			</li>
			<li><a href="#" id="restManButton" onclick="restMen(event)" role="presentation">Registrovanje menadzera restorana</a>
			</li>
			<li><a href="#" id="manButton" onclick="men(event)" role="presentation" >Registrovanje menadzera sistema</a>
			</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
		    </ul>
		</div>
	</nav>
	</br></br>
	<div class=" col-md-6 col-md-offset-1 centered" id="regRestFormDiv" >
		<form id="regRestForm" role="form" focus="box-shadow" >
		  	<div class="form-group">
			    <label>Naziv:</label>
			    <input type="text" class="form-control taskNameValidation" name="name">
			</div>
			<div class="form-group">
		    	<label>Opis:</label>
		    	<textarea class="form-control taskNameValidation" rows="3" name="descr"></textarea>
		    </div>
		    <input type="submit" value="Registruj restoran" class="btn btn-default"><br><br>
		</form>
		
		<div id="map" style="width:100%;height:500px"></div>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" id="regRestMenFormDiv">
	<br></br>
		<form id="chooseRestForm" role="form" focus="box-shadow">
				<select type="text" name="name" class="form-control" >
					<c:forEach items="${restaurants}"  var="rest">
						<option><c:out value="${rest.name}"/></option>
					</c:forEach>	
				</select>
		</form>	
		<form id="regRestMenForm" role="form">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName">
			</div>
			<div class="form-group">
			    <label>Prezime:</label>
			    <input type="text" class="form-control taskNameValidation" name="lastName">
			</div>
			<div class="form-group has-feedback">
			    <label>Email:</label>
			    <input type="email" class="form-control taskNameValidation" name="email" >
			</div>
			<div class="form-group has-feedback">
			    <label>Velicina odece:</label>
			    <input type="number" name="clothesSize" class="form-control">
			</div>
			<div class="form-group has-feedback">
			    <label>Velicina obuce:</label>
			    <input type="number" name="shoesSize" class="form-control">
			</div>
			<input type="submit" value="Registruj menadzera restorana" class="btn btn-default"><br><br>
		</form>
		
	</div>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="regMenForm" role="form">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${userFirstName }">
			</div>
			<div class="form-group">
			    <label>Prezime:</label>
			    <input type="text" class="form-control taskNameValidation" name="lastName" value="${userLastName }">
			</div>
			<div class="form-group has-feedback">
			    <label>Email:</label>
			    <input type="email" class="form-control taskNameValidation" name="email" value="${userEmail }">
			</div>
		    <input type="submit" value="Registruj menadzera sistema" class="btn btn-default"><br><br>
		</form>
	</div>
	
<script>
var myCenter2; 
var myCenterNew;
function myMap() {
  var mapCanvas = document.getElementById("map");
  if (navigator.geolocation) {
	 
     	 navigator.geolocation.getCurrentPosition(showPosition);
	  
  } else { 
      alert("Geolocation is not supported by this browser.");
  }
  	var myCenter = myCenter2;
  
  var mapOptions = {center: myCenter, zoom: 15};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE,
    draggable: true
  });
  marker.setMap(map);
  google.maps.event.addListener(marker, 'dragend', function (evt) {
	  myCenter2 = new google.maps.LatLng(evt.latLng.lat().toFixed(3), evt.latLng.lng().toFixed(3));
	  changedPosition(evt.latLng.lat().toFixed(3), evt.latLng.lng().toFixed(3));
	  evt.preventDefault();
	});
}
function showPosition(position) {
	myCenter2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	myMap();
}

</script>	
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwYZIoAYNvudJgD8Tv3tpplWUt30C8hv0&callback=myMap"></script>
	<script language="javascript">
		function rest(){
			$.ajax({
		        type: "GET",
		        url: "/rest/restSet",
				contentType:"application/json",
		        success: function(){
					document.getElementById("regRestFormDiv").hidden=${mapaHidden};
					document.getElementById("regMenForm").hidden=true;
					document.getElementById("regRestMenFormDiv").hidden=true;
					location.reload();
		        }
			});
		}
		function restMen(e){
			e.preventDefault();
			$.ajax({
		        type: "GET",
		        url: "/rest/restMenSet",
				contentType:"application/json",
		        success: function(){
					document.getElementById("regRestFormDiv").hidden=true;
					document.getElementById("regMenForm").hidden=true;
					document.getElementById("regRestMenFormDiv").hidden=false;
		        }
			});
		}
		function men(e){
			e.preventDefault();
			$.ajax({
		        type: "GET",
		        url: "/rest/menSet",
				contentType:"application/json",
		        success: function(){
					document.getElementById("regRestFormDiv").hidden=true;
					document.getElementById("regMenForm").hidden=false;
					document.getElementById("regRestMenFormDiv").hidden=true;
		        }
			});
		}
	</script>
</body>

</html>