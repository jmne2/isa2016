<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Kuvar</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
	<link href="css/kalendar.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script  type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			if ("${userId }" === ""){
				window.location="index.jsp";
			}
			document.getElementById("kuvariForm").hidden=true;
			document.getElementById("kalendar").hidden=true;
			document.getElementById("porudzbine").hidden = true;
			
		});
		 
		function info(){
			document.getElementById("kuvariForm").hidden=false;
			document.getElementById("porudzbine").hidden = true;
			document.getElementById("kalendar").hidden=true;
			
		}
		
		function kuvarPocetna(){
			izlistajPorudzbine();
			document.getElementById("kuvariForm").hidden=true;
			document.getElementById("kalendar").hidden=true;
			document.getElementById("porudzbine").hidden = false;
			
		}
		 
		
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#kuvariForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		  
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
			
		function changeCook(){
			
			var $form = $("#kuvariForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/cookCont/changeCook1/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
						
		         }
	         });
			document.getElementById("kuvariForm").hidden=true;
			 alert("Uspesno ste izmenili svoje podatke!");
		}
		
		function izlistajPorudzbine(){
			$.ajax({
	             type: "GET",
	             url: "/cookCont/izlastajPorudzbine/",
	 			contentType:"application/json",
	             complete: function(){
	            	 alert("izlistao");
		         }
			});
		}
		
		function prikazRada(){
			$.ajax({
	             type: "GET",
	             url: "/cookCont/prikazRada/",
	 			contentType:"application/json",
	             complete: function(){
		         }
			});
			alert("izasaoizprikazarada")
		}
		
		function preuzetoJeloId(id){
			$.ajax({
		         type: "POST",
		         url: "/cookCont/izmeniStatusHrane/"+id+"/",
					contentType:"application/json",
		         complete: function(){
		         }
			});
		}
		
		function spremnoJeloId(id){
			$.ajax({
		         type: "POST",
		         url: "/cookCont/izmeniSpremnoJelo/"+id+"/",
					contentType:"application/json",
		         complete: function(){
		         }
			});
		}

		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}

		
		


</script>
  </head>
  <body background="images/Elegant_Background-7.jpg">
  
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" id = "kuvarButton" onclick = "kuvarPocetna()" href="#">Kuvar</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href=""  id="rasporedButton" onclick="prikazRada()" role="presentation">Raspored rada</a></li>
        <li><a href="#"  id="pocetnaButton" onclick="info()" role="presentation">Podaci</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
    </ul>
    </div>
  </div>
</nav>

   	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="kuvariForm" role="form" focus="box-shadow">
		  	<div class="form-group" >
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${LogCook.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName" value="${LogCook.lastName }">
		    </div>
			<div class="form-group">
		    	<label>Lozinka:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="password" value="${LogCook.password }">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="clothesSize" value="${LogCook.clothesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="shoesSize" value="${LogCook.shoesSize }">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeCookF" onclick="changeCook()" class="btn btn-default"><br><br>
		</form>
		</div>
		
		<div id = "porudzbine" >
			<table class="table">
				<tr>
					<th>Porudzbine:</th>
				</tr>
				<c:forEach items="${dishes}"  var="dish">
					<tr>	
						<td><c:out value="${dish.food.name}"/></td>				
						<td><a href="#" id="preuzetoJelo" onclick="preuzetoJeloId(${dish.id})" role="presentation">Preuzmi</a></td>
						<td><a href="#" id="spremnoJelo" onclick="spremnoJeloId(${dish.id })" role="presentation">Spremno</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	<div id = "kalendar">
			<div class="month"> 
			  <ul>
				<li>
				  <c:out value="${mesec}"/><br>
				  <span style="font-size:18px"></span>
				</li>
			  </ul>
			</div>
<!-- 
			<ul class="weekdays">
			  <li><b>Ponedeljak</b></li>
			  <li><b>Utorak</b></li>
			  <li><b>Sreda</b></li>
			  <li><b>Cetvrtak</b></li>
			  <li><b>Petak</b></li>
			  <li><b>Subota</b></li>
			  <li><b>Nedelja</b></li>
			</ul>
 -->
			<ul id = "dodati" class="days"> 
			<c:set var="currDay" scope="session" value="${danUMesecu}"/>
			<c:set var="weekStart" scope="session" value="${pocetakNedelje}"/>
			<c:set var="weekStartMonth" scope="session" value="${pocetakNedeljeMesec}"/>
			<c:set var="weekEnd" scope="session" value="${krajNedelje}"/>
			<c:set var="brojDanaUMesecu" scope="session" value="${brojDana}"/>
			<c:set var="trenutniMesec" scope="session" value="${mesecTrenutni}"/>
				<c:forEach var="i" begin="1" end="${brojDana}">
				<li>
				<c:if test="${i == currDay }"><span class="active">${i} </span> <br> 
					<c:if test="${(i>=weekStart) && (i<weekEnd) && (trenutniMesec == weekStartMonth)}">
						<b>Prva smena:</b>
						<br>
						<c:forEach items="${kuvariPrvaSmena}"  var="kuvarPrvaSmena1">
		   					<c:out value="${kuvarPrvaSmena1.firstName}"/><br>
		   				</c:forEach>
		   				<b>Druga smena:</b>
		   				<br>
						<c:forEach items="${kuvariDrugaSmena}"  var="kuvarDrugaSmena1">
		   					<c:out value="${kuvarDrugaSmena1.firstName}"/><br>
		   				</c:forEach>
					</c:if>
				</c:if>
				<c:if test="${!(i == currDay)}"><b><c:out value="${i}"/> </b> <br>
				<c:if test="${!(i>=weekStart) && (i<=brojDanaUMesecu)}">
					<br>
						<br>
						<c:forEach items="${kuvariPrvaSmena}"  var="kuvarPrvaSmena1">
		   					<br>
		   				</c:forEach>
		   				<br>
		   				<br>
						<c:forEach items="${kuvariDrugaSmena}"  var="kuvarDrugaSmena1">
		   				<br>
		   				</c:forEach>
		   				</c:if>
				
				<c:if test="${(i>=weekStart) && i<=brojDanaUMesecu}">
						<b>Prva smena:</b>
						<br>
						<c:forEach items="${kuvariPrvaSmena}"  var="kuvarPrvaSmena1">
		   					<c:out value="${kuvarPrvaSmena1.firstName}"/><br>
		   				</c:forEach>
		   				<b>Druga smena:</b>
		   				<br>
						<c:forEach items="${kuvariDrugaSmena}"  var="kuvarDrugaSmena1">
		   					<c:out value="${kuvarDrugaSmena1.firstName}"/><br>
		   				</c:forEach>
					</c:if>
				</li>
				
				</c:if>
				</c:forEach>
			</ul>
		</div>
	</body>
</html>