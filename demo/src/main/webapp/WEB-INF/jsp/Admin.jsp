<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html"charset="ISO-8859-1">
<%@ page isELIgnored="false" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<title>Menadzer sistema</title>
<script  type="text/javascript">


		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
	
		$(document).ready(function () {
			document.getElementById("regRestForm").hidden=true;
			document.getElementById("regMenForm").hidden=true;
		});

		
		
		function registerRest() {
			var $form = $("#regRestForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/rest/addRestaurant",
	             data: s,
	             contentType : "application/json",
	             success: function(data){
	            	 
	             }
	         });
	      }  
		
		function registerMen() {
			var $form = $("#regMenForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/user/addAdmin/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
	            	 if(datas.responseText === "CREATED"){
	            		 location.reload();
	             	}
	             }
	         });
	      }  
		
		//Chosen select plugin
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 });
		  //rules and messages objects
		  $("#regRestForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		  
		    $("#regMenForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        }
		    });
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		});
</script>

</head>
<body>
	</br>
	<p>
		${isHeadAdmin}
	</p>
	<div class="container-fluid">
		<ul class="nav nav-pills">
		<li><a href="#" id="restButton" onclick="rest()" role="presentation">Registrovanje restorana</a>
		</li>
		<li><a href="#" id="restManButton" onclick="restMen()" role="presentation">Registrovanje menadzera restorana</a>
		</li>
		<li><a href="#" id="manButton" onclick="men()" role="presentation">Registrovanje menadzera sistema</a>
		</li>
		</ul>
	</div>
	</br></br>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="regRestForm" role="form" focus="box-shadow">
		  	<div class="form-group">
			    <label>Naziv:</label>
			    <input type="text" class="form-control taskNameValidation" name="name">
			</div>
			<div class="form-group">
		    	<label>Opis:</label>
		    	<textarea class="form-control taskNameValidation" rows="3" name="descr"></textarea>
		    </div>
		    <input type="submit" value="Registruj restoran" onclick="registerRest()" class="btn btn-default"><br><br>
		</form>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="regMenForm" role="form">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName">
			</div>
			<div class="form-group">
			    <label>Prezime:</label>
			    <input type="text" class="form-control taskNameValidation" name="lastName">
			</div>
			<div class="form-group has-feedback">
			    <label>Email:</label>
			    <input type="email" class="form-control taskNameValidation" name="email" >
			</div>
			<div class="form-group">
		    	<label>Sifra:</label>
		    	<input type="password" class="form-control taskNameValidation" name="password">
		    </div>
		    <input type="submit" value="Registruj menadzera sistema" onclick="registerMen()" class="btn btn-default"><br><br>
		</form>
	</div>
	
	<script>
		function rest(){
			document.getElementById("regRestForm").hidden=false;
			document.getElementById("regMenForm").hidden=true;
		}
		function restMen(){
		
		}
		function men(){
			document.getElementById("regRestForm").hidden=true;
			document.getElementById("regMenForm").hidden=false;
		}
	</script>
</body>

</html>