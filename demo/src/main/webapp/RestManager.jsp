<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<title>Restaurant Manager</title>
<script  type="text/javascript">
	
	
	function getFormData($form){
	    var unindexed_array = $form.serializeArray();
	    var indexed_array = {};

	    $.map(unindexed_array, function(n, i){
	        indexed_array[n['name']] = n['value'];
	    });

	    return indexed_array;
	}

		$(document).ready(function () {
			document.getElementById("restInfoForm").hidden=${restHidden};
			document.getElementById("jelovnikDiv").hidden=${jelovnikHidden};
			document.getElementById("jelovnikForm").hidden=${hiddenJelovnik};
			document.getElementById("jelovnikForm2").hidden=${hiddenJelovnik2};
			document.getElementById("kartaDiv").hidden = ${kartaHidden};
			document.getElementById("kartaForm").hidden=${hiddenKarta};
			document.getElementById("kartaForm2").hidden=${hiddenKarta2};
			document.getElementById("stoloviDiv").hidden = ${stoloviHidden};
			document.getElementById("stoloviForm").hidden=${hiddenStolovi};
			document.getElementById("stoloviForm2").hidden=${hiddenStolovi2};
			document.getElementById("kuvariDiv").hidden = ${kuvariHidden};
			document.getElementById("kuvariForm").hidden=${hiddenKuvari};
			document.getElementById("kuvariForm2").hidden=${hiddenKuvari2};
			document.getElementById("konobariDiv").hidden = ${konobariHidden};
			document.getElementById("konobariForm").hidden=${hiddenKonobari};
			document.getElementById("konobariForm2").hidden=${hiddenKonobari2};
			document.getElementById("sankeriDiv").hidden = ${sankeriHidden};
			document.getElementById("sankeriForm").hidden=${hiddenSankeri};
			document.getElementById("sankeriForm2").hidden=${hiddenSankeri2};
			document.getElementById("currOrderDiv").hidden=${currOrderHidden};
			document.getElementById("newOrderDiv").hidden=${newOrderHidden};
			document.getElementById("ponudeDiv").hidden=${ponudeHidden};
			document.getElementById("regPonDiv").hidden=${regPonHidden};
			$.ajax({
				url: "/restMan/onload/",
				type:"POST",
				dataType:"json",
				success: function(data) {
						location.reload();
				}
			
			});	
		});
		
		function changeRest(){
			var $form = $("#restInfoForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeRest/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		
		function changeDish(){
			var $form = $("#jelovnikForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeDish/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addDish(){
			$("#jelovnikForm").validate();
			var $form = $("#jelovnikForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addDish/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
	     			location.reload();
		         }
	         });
	        
			
		}
		
		
		function changeDishId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeDishId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		function deleteDishId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/deleteDishId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function add(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addDishSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function changeDrink(){
			var $form = $("#kartaForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeDrink/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addDrink(){
			var $form = $("#kartaForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addDrink/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function changeDrinkId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeDrinkId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function deleteDrinkId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/deleteDrinkId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function addButton(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addDrinkSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function changeTable(){
			var $form = $("#stoloviForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeTable/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addTable(){
			var $form = $("#stoloviForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addTable/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function changeTableId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeTableId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function deleteTableId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/deleteTableId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function addTableButton(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addTableSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
	
		function changeCook(){
			var $form = $("#kuvariForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeCook/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addCook(){
			var $form = $("#kuvariForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addCook/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function changeCookId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeCookId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}

		function addCookButton(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addCookSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		
		
		
		function changeWaiter(){
			var $form = $("#konobariForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeWaiter/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addWaiter(){
			var $form = $("#konobariForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addWaiter/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function changeWaiterId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeWaiterId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function addWaiterButton(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addWaiterSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		
		function changeBartender(){
			var $form = $("#sankeriForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			
	        $.ajax({
	             type: "POST",
	             url: "/restMan/changeBartender/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addBartender(){
			var $form = $("#sankeriForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addBartender/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function changeBartenderId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/changeBartenderId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function addBartenderButton(){
			$.ajax({
	             type: "GET",
	             url: "/restMan/addBartenderSet/",
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		function deleteEmployeeId(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/deleteEmployeeId/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		
		function addOrderItem(){
			var $form = $("#newOrderForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/addOrderItem/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function addOrder(){
	        $.ajax({
	             type: "GET",
	             url: "/restMan/addOrder/",
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		
		function acceptOffer(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/acceptOffer/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		function declineOffer(id){
			$.ajax({
	             type: "GET",
	             url: "/restMan/declineOffer/",
	             data: {"id": id},
	 			contentType:"application/json",
	             complete: function(){
	            	 location.reload();
		         }
			});
		}
		
		function saveDate(){
			var $form = $("#saveDateForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/saveDate/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		
		function regPon(){
			var $form = $("#regPon");
			var data = getFormData($form);
			var s = JSON.stringify(data);
	        $.ajax({
	             type: "POST",
	             url: "/restMan/regPonudjaca/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
		            	location.reload();
		         }
	         });
		}
		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}
		
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 	});
		    $("#jelovnikForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addDish(); }
		    });
		    $("#kartaForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addDrink(); }
		    });
		    $("#stoloviForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addTable(); }
		    });
		    $("#kuvariForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addCook(); }
		    });
		    $("#konobariForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addWaiter(); }
		    });
		    $("#sankeriForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { addBartender(); }
		    });
		    
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });		
		});
		
</script>
</head>
<body>
 <body background="images/Elegant_Background-7.jpg">
  <nav class="navbar navbar-inverse">
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
		<li><a href="#" id="rest" onclick="rest()" role="presentation">Podaci o restoranu</a>
		</li>
		<li><a href="#" id="jelovnik" onclick="jelovnik()" role="presentation">Jelovnik</a>
		</li>
		<li><a href="#" id="karta" onclick="karta()" role="presentation">Karta pica</a>
		</li>
		<li><a href="#" id="stolovi" onclick="stolovi()" role="presentation">Konfiguracija stolova</a>
		</li>
		<li><a href="#" id="kuvari" onclick="kuvari()" role="presentation">Kuvari</a>
		</li>
		<li><a href="#" id="konobari" onclick="konobari()" role="presentation">Konobari</a>
		</li>
		<li><a href="#" id="sankeri" onclick="sankeri()" role="presentation">Sankeri</a>
		</li>
		<li><a href="#" id="porudzbina" onclick="porudzbina()" role="presentation">Porudzbina</a>
		</li>
		<li><a href="#" id="novaPorudzbina" onclick="novaPorudzbina()" role="presentation">Nova porudzbina</a>
		</li>
		<li><a href="#" id="ponude" onclick="ponude()" role="presentation">Ponude</a>
		</li>
		<li><a href="#" id="regPonudjaca" onclick="regPonudjaca()" role="presentation">Registruj ponudjaca</a>
		</li>
		<li><a href="#" id="izvestaji" onclick="izvestaji()" role="presentation">Izvestaji</a>
		</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
	        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
	    </ul>
	</div>
	</nav>
	</br></br>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="restInfoForm" role="form" focus="box-shadow">
		  	<div class="form-group">
			    <label>Naziv restorana:</label>
			    <input type="text" class="form-control taskNameValidation" name="name" value="${restaurant.name }">
			</div>
			<div class="form-group">
		    	<label>Opis restorana:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="descr" value="${restaurant.descr }">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="saveRest" onclick="changeRest()" class="btn btn-default"><br><br>
		</form>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="jelovnikDiv">
		<div >
			<table class="table">
				<tr>
					<th>Naziv jela</th>
					<th>Opis jela</th>
					<th>Cena</th>
					<th>Tip</th>
				</tr>
				<c:forEach items="${dishes}"  var="dish">
					<tr>	
						<td><c:out value="${dish.name}"/></td>				
						<td><c:out value="${dish.description}"/></td>
						<td><c:out value="${dish.price}"/></td>
						<td><c:out value="${dish.type }"/></td>
						<td><a href="#" id="changeDishId" onclick="changeDishId(${dish.id})" role="presentation">Izmeni jelo</a></td>
						<td><a href="#" id="deleteDishId" onclick="deleteDishId(${dish.id })" role="presentation">Obrisi jelo</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="add()">Dodaj novo jelo</button></br></br>
		</div>
		<div style=" overflow: hidden">
		<form id="jelovnikForm" role="form" focus="box-shadow" hidden=${hiddenJelovnik }>
		  	<div class="form-group" >
			    <label>Naziv jela:</label>
			    <input type="text" class="form-control taskNameValidation" name="name" >
			</div>
			<div class="form-group">
		    	<label>Opis jela:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="description">
		    </div>
		    <div class="form-group">
		    	<label>Cena jela:</label>
		    	 <input type="number" class="form-control taskNameValidation" name="price">
		    </div>
		    <div class="form-group">
		    	<label>Tip jela:</label>
		    	<select type="text" name="type" class="form-control">
		    		<option>Salata</option>
		    		<option>Kuvano jelo</option>
		    		<option>Peceno jelo</option>
		    		<option>Desert</option>
		    	</select>
		    </div>
		    <input type="submit" value="Dodaj jelo" id="addDishF"  class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="jelovnikForm2" role="form" focus="box-shadow" hidden=${hiddenJelovnik2 }>
		  	<div class="form-group" >
			    <label>Naziv jela:</label>
			    <input type="text" name="name" class="form-control" value="${changingDish.name }">
			</div>
			<div class="form-group">
		    	<label>Opis jela:</label>
		    	 <input type="text"  name="description" class="form-control" value="${changingDish.description }">
		    </div>
		    <div class="form-group">
		    	<label>Cena jela:</label>
		    	 <input type="number"  name="price" class="form-control" value="${changingDish.price }">
		    </div>
		    <div class="form-group">
		    	<label>Tip jela:</label>
		    	<select type="text" name="type" class="form-control" value="${changingDish.type }">
		    		<option>Salata</option>
		    		<option>Kuvano jelo</option>
		    		<option>Peceno jelo</option>
		    		<option>Desert</option>
		    	</select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeDishF" onclick="changeDish()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="kartaDiv">
		<div >
			<table class="table">
				<tr>
					<th>Naziv pica</th>
					<th>Opis pica</th>
					<th>Cena</th>
				</tr>
				<c:forEach items="${drinks}"  var="drink">
					<tr>	
						<td><c:out value="${drink.name}"/></td>				
						<td><c:out value="${drink.description}"/></td>
						<td><c:out value="${drink.price}"/></td>
						<td><a href="#" id="changeDrinkId" onclick="changeDrinkId(${drink.id})" role="presentation">Izmeni pice</a></td>
						<td><a href="#" id="deleteDrinkId" onclick="deleteDrinkId(${drink.id })" role="presentation">Obrisi pice</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="addButton()">Dodaj novo pice</button></br></br>
		</div>
		<div style=" overflow: hidden">
		<form id="kartaForm" role="form" focus="box-shadow" hidden=${hiddenKarta }>
		  	<div class="form-group" >
			    <label>Naziv pica:</label>
			    <input type="text" class="form-control taskNameValidation" name="name" >
			</div>
			<div class="form-group">
		    	<label>Opis pica:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="description">
		    </div>
		    <div class="form-group">
		    	<label>Cena pica:</label>
		    	 <input type="number" class="form-control taskNameValidation" name="price">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="addDrinkF" class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="kartaForm2" role="form" focus="box-shadow" hidden=${hiddenKarta2 }>
		  	<div class="form-group" >
			    <label>Naziv pica:</label>
			    <input type="text" name="name" class="form-control" value="${changingDrink.name }">
			</div>
			<div class="form-group">
		    	<label>Opis pica:</label>
		    	 <input type="text"  name="description" class="form-control" value="${changingDrink.description }">
		    </div>
		    <div class="form-group">
		    	<label>Cena pica:</label>
		    	 <input type="number"  name="price" class="form-control" value="${changingDrink.price }">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeDrinkF" onclick="changeDrink()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	<div class=" col-md-10 col-md-offset-1 centered" style="overflow: auto" id="kuvariDiv">
		<div >
			<table class="table">
				<tr>
					<th>Ime</th>
					<th>Prezime</th>
					<th>Email</th>
					<th>Datum rodjenja</th>
					<th>Konfekcijski broj</th>
					<th>Velicina obuce</th>
					<th>Profil</th>
					<th>Smena trenutno</th>
					<th>Smena sledeca</th>
					<th>id</th>
					<th></th>
				</tr>
				<c:forEach items="${cooks}"  var="cook">
					<tr>	
						<td><c:out value="${cook.firstName}"/></td>				
						<td><c:out value="${cook.lastName}"/></td>
						<td><c:out value="${cook.email}"/></td>
						<td><c:out value="${cook.birthDate}"/></td>
						<td><c:out value="${cook.clothesSize}"/></td>
						<td><c:out value="${cook.shoesSize}"/></td>
						<td><c:out value="${cook.role}"/></td>
						<td><c:out value="${cook.currShift}"/></td>
						<td><c:out value="${cook.nextShift}"/></td>	
						<td><c:out value="${cook.id}"/></td>					
						<td><a href="#" id="changeCookId" onclick="changeCookId(${cook.id})" role="presentation">Izmeni podatke</a></td>
						<td><a href="#" id="deleteEmployeeId" onclick="deleteEmployeeId(${cook.id })" role="presentation">Obrisi kuvara</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="addCookButton()">Dodaj novog kuvara</button></br></br>
		</div>
		<div style=" overflow: hidden">
		<form id="kuvariForm" role="form" focus="box-shadow" hidden=${hiddenKuvari }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" >
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email" class="form-control taskNameValidation" name="email">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" class="form-control taskNameValidation" name="birthDate">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="clothesSize">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="shoesSize">
		    </div>
		    <div class="form-group">
		    	<label>Uloga:</label>
		    	<select type="text" name="role" class="form-control">
		    		<option>Kuvar za salate</option>
		    		<option>Kuvar za kuvana jela</option>
		    		<option>Kuvar za pecena jela</option>
		    		<option>Kuvar za deserte</option>
		    	</select>
		    </div>
		    <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="addCookF"  class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="kuvariForm2" role="form" focus="box-shadow" hidden=${hiddenKuvari2 }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text"  name="firstName" class="form-control" value="${changingCook.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" name="lastName" class="form-control" value="${changingCook.lastName }">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" name="birthDate" class="form-control" value="${changingCook.birthDate }">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" name="clothesSize" class="form-control" value="${changingCook.clothesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" name="shoesSize" class="form-control" value="${changingCook.shoesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Uloga:</label>
		    	<select type="text" name="role" class="form-control" value="${changingCook.role }">
		    		<option>Kuvar za salate</option>
		    		<option>Kuvar za kuvana jela</option>
		    		<option>Kuvar za pecena jela</option>
		    		<option>Kuvar za deserte</option>
		    	</select>
		    </div>
		     <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control" value="${changingCook.currShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control" value="${changingCook.nextShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeCookF" onclick="changeCook()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	
	<!-- 
	KONOBARI
	 -->
	<div class=" col-md-10 col-md-offset-1 centered" style="overflow: auto" id="konobariDiv">
		<div >
			<table class="table">
				<tr>
					<th>Ime</th>
					<th>Prezime</th>
					<th>Email</th>
					<th>Datum rodjenja</th>
					<th>Konfekcijski broj</th>
					<th>Velicina obuce</th>
					<th>Reon</th>
					<th>id</th>
					<th>Trenutna smena</th>
					<th>Sledeca smena</th>
					<th></th>
				</tr>
				<c:forEach items="${waiters}"  var="waiter">
					<tr>	
						<td><c:out value="${waiter.firstName}"/></td>				
						<td><c:out value="${waiter.lastName}"/></td>
						<td><c:out value="${waiter.email}"/></td>
						<td><c:out value="${waiter.birthDate}"/></td>
						<td><c:out value="${waiter.clothesSize}"/></td>
						<td><c:out value="${waiter.shoesSize}"/></td>
						<td><c:out value="${waiter.reon}"/></td>
						<td><c:out value="${waiter.id}"/></td>
						<td><c:out value="${waiter.currShift}"/></td>
						<td><c:out value="${waiter.nextShift}"/></td>
						<td><a href="#" id="changeWaiterId" onclick="changeWaiterId(${waiter.id})" role="presentation">Izmeni podatke</a></td>
						<td><a href="#" id="deleteWaiterId" onclick="deleteEmployeeId(${waiter.id })" role="presentation">Obrisi konobara</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="addWaiterButton()">Dodaj novog konobara</button></br></br>
		</div>
		<div style=" overflow: hidden">
		<form id="konobariForm" role="form" focus="box-shadow" hidden=${hiddenKonobari }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" >
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email" class="form-control taskNameValidation" name="email">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" class="form-control taskNameValidation" name="birthDate">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="clothesSize">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="shoesSize">
		    </div>
		    <div class="form-group">
		    	<label>Reon:</label>
		    	<select type="text" name="reon" class="form-control">
		    		<option>Pusacki deo</option>
		    		<option>Nepusacki deo</option>
		    		<option>Basta</option>
		    	</select>
		    </div>
		     <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control" >
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control" >
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="addWaiterF"  class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="konobariForm2" role="form" focus="box-shadow" hidden=${hiddenKonobari2 }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text"  name="firstName" class="form-control"  value="${changingWaiter.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" name="lastName" class="form-control" value="${changingWaiter.lastName }">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" name="birthDate" class="form-control" value="${changingWaiter.birthDate }">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" name="clothesSize" class="form-control" value="${changingWaiter.clothesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" name="shoesSize" class="form-control" value="${changingWaiter.shoesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Reon:</label>
		    	<select type="text" name="reon" value="${changingWaiter.reon }">
		    		<option>Pusacki deo</option>
		    		<option>Nepusacki deo</option>
		    		<option>Basta</option>
		    	</select>
		    </div>
		     <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control" value="${changingWaiter.currShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control" value="${changingWaiter.nextShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeWaiterF" onclick="changeWaiter()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	
	<!--  SANKERI	 -->
	<div class=" col-md-10 col-md-offset-1 centered" style="overflow: auto" id="sankeriDiv">
		<div >
			<table class="table">
				<tr>
					<th>Ime</th>
					<th>Prezime</th>
					<th>Email</th>
					<th>Datum rodjenja</th>
					<th>Konfekcijski broj</th>
					<th>Velicina obuce</th>
					<th>id</th>
					<th>Trenutna smena</th>
					<th>Sledeca smena</th>
					<th></th>
				</tr>
				<c:forEach items="${bartenders}"  var="bartender">
					<tr>	
						<td><c:out value="${bartender.firstName}"/></td>				
						<td><c:out value="${bartender.lastName}"/></td>
						<td><c:out value="${bartender.email}"/></td>
						<td><c:out value="${bartender.birthDate}"/></td>
						<td><c:out value="${bartender.clothesSize}"/></td>
						<td><c:out value="${bartender.shoesSize}"/></td>
						<td><c:out value="${bartender.id}"/></td>
						<td><c:out value="${bartender.currShift}"/></td>
						<td><c:out value="${bartender.nextShift}"/></td>
						<td><a href="#" id="changeBartenderId" onclick="changeBartenderId(${bartender.id})" role="presentation">Izmeni podatke</a></td>
						<td><a href="#" id="deleteEmployeeId" onclick="deleteEmployeeId(${bartender.id })" role="presentation">Obrisi sankera</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="addBartenderButton()">Dodaj novog sankera</button></br></br>
		</div>
		<div style=" overflow: hidden">
		<form id="sankeriForm" role="form" focus="box-shadow" hidden=${hiddenSankeri }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" >
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email" class="form-control taskNameValidation" name="email">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" class="form-control taskNameValidation" name="birthDate">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="clothesSize">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="shoesSize">
		    </div>
		    <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control" >
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control" >
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="addBartenderF"  class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="sankeriForm2" role="form" focus="box-shadow" hidden=${hiddenSankeri2 }>
		  	<div class="form-group" >
			    <label>Ime</label>
			    <input type="text"  name="firstName" class="form-control" value="${changingBartender.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" name="lastName" class="form-control" value="${changingBartender.lastName }">
		    </div>
		    <div class="form-group">
		    	<label>Datum rodjenja:</label>
		    	 <input type="date" name="birthDate" class="form-control" value="${changingBartender.birthDate }">
		    </div>
		    <div class="form-group">
		    	<label>Konfekcijski broj:</label>
		    	 <input type="text" name="clothesSize" class="form-control" value="${changingBartender.clothesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Velicina obuce:</label>
		    	 <input type="text" name="shoesSize" class="form-control" value="${changingBartender.shoesSize }">
		    </div>
		    <div class="form-group">
		    	<label>Smena trenutne nedelje:</label>
		    	 <select type="number" name="currShift" class="form-control" value="${changingBartender.currShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <div class="form-group">
		    	<label>Smena sledece nedelje:</label>
		    	 <select type="number" name="nextShift" class="form-control" value="${changingBartender.nextShift }">
		    	 	<option>1</option>
		    	 	<option>2</option>
		    	 </select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeBartenderF" onclick="changeBartender()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	
	
	
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="stoloviDiv">
		<div >
			<table class="table">
				<tr>
					<th>Redni broj stola</th>
					<th>Broj stolica</th>
					<th>Segment</th>
				</tr>
				<c:forEach items="${tables}"  var="table">
					<tr>	
						<td><c:out value="${table.tableNum}"/></td>				
						<td><c:out value="${table.chairCount}"/></td>
						<td><c:out value="${table.segment}"/></td>
						<td><a href="#" id="changeTableId" onclick="changeTableId(${table.id})" role="presentation">Izmeni podatke</a></td>
						<td><a href="#" id="deleteTableId" onclick="deleteTableId(${table.id })" role="presentation">Obrisi sto</a></td>
					</tr>
				</c:forEach>
			</table>
			<button type="button" class="btn btn-default" onclick="addTableButton()">Dodaj novi sto</button></br></br>
		</div>
	<div style=" overflow: hidden">
		<form id="stoloviForm" role="form" focus="box-shadow" hidden=${hiddenStolovi }>
		  	<div class="form-group" >
			    <label>Redni broj stola</label>
			    <input type="text" class="form-control" name="tableNum" value="${nextTable }" disabled>
			</div>
			<div class="form-group">
		    	<label>Broj stolica:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="chairCount">
		    </div>
		    <div class="form-group">
		    	<label>Segment:</label>
		    	<select type="text" name="segment" class="form-control">
		    		<option>Pusacki deo</option>
		    		<option>Nepusacki deo</option>
		    		<option>Basta</option>
		    	</select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="addTableF"  class="btn btn-default"><br><br>
		</form>
		</div>
		<div style=" overflow: hidden">
		<form id="stoloviForm2" role="form" focus="box-shadow" hidden=${hiddenStolovi2 }>
		  	<div class="form-group" >
			    <label>Redni broj stola:</label>
			    <input type="text" name="tableNum" class="form-control" value="${changingTable.tableNum }" disabled>
			</div>
			<div class="form-group">
		    	<label>Broj stolica:</label>
		    	 <input type="text"  name="chairCount" class="form-control" value="${changingTable.chairCount }">
		    </div>
		    <div class="form-group">
		    	<label>Segment:</label>
		    	<select type="text" name="segment" class="form-control" value="${changingTable.segment }">
		    		<option>Pusacki deo</option>
		    		<option>Nepusacki deo</option>
		    		<option>Basta</option>
		    	</select>
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="changeTableF" onclick="changeTable()" class="btn btn-default"><br><br>
		</form>
		</div>
	</div>
	
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="currOrderDiv" hidden=${currOrderHidden }>
		<div >
			<table class="table">
				<tr>
					<th>Naziv</th>
					<th>Kolicina</th>
					<th>Mera</th>
				</tr>
				<c:forEach items="${orderItems}"  var="orderItem">
					<tr>	
						<td><c:out value="${orderItem.name}"/></td>				
						<td><c:out value="${orderItem.amount}"/></td>
						<td><c:out value="${orderItem.metrics}"/></td>
					</tr>
				</c:forEach>
			</table>
			<p>Porudzbina istice ${order.expirationDate }</p>
		</div>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="newOrderDiv" hidden=${newOrderHidden }>
		<form id="newOrderForm" role="form" focus="box-shadow" >
		  	<div class="form-group" >
			    <label>Naziv:</label>
			    <input type="text" name="name" class="form-control" >
			</div>
			<div class="form-group">
		    	<label>Kolicina:</label>
		    	 <input type="number"  name="amount" class="form-control">
		    </div>
		    <div class="form-group">
		    	<label>Mera:</label>
		    	 <input type="text"  name="metrics" class="form-control">
		    </div>
		    <input type="submit" value="Dodaj" id="makeOrderF" onclick="addOrderItem()" class="btn btn-default"><br><br>
		</form>
		</br></br>
		<form id="saveDateForm" role="form" focus="box-shadow">
			<div>
				<label>Datum isteka porudzbine:</label>
				<input type="date" name="expirationDate" class="btn btn-default">
			</div>
			<input type="submit" value="sacuvajDatum" onclick="saveDate()" class="btn btn-default">
		</form>
		<input type="button" onclick="addOrder()" value="Sacuvaj porudzbinu" class="btn btn-default">
		</br></br>
		<ul>
		<c:forEach items="${newOrderItems}"  var="newOrderItem">
					<li><c:out value="${newOrderItem.name}"/>				
					<c:out value="${newOrderItem.amount}"/>	
					<c:out value="${newOrderItem.metrics}"/></li>
		</c:forEach>
		</ul>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="ponudeDiv" hidden=${ponudeHidden }>
		<div >
			<table class="table">
				<tr>
					<th>Ponudjac</th>
					<th>Cena</th>
					<th>Rok isporuke</th>
					<th>Garancija</th>
					<th>Stanje</th>
				</tr>
				<c:forEach items="${offersRM}"  var="offer">
					<tr>	
						<td><c:out value="${offer.supplier.id}"/></td>				
						<td><c:out value="${offer.price}"/></td>
						<td><c:out value="${offer.deadline}"/></td>
						<td><c:out value="${offer.garanty}"/></td>
						<td><c:out value="${offer.offerState }"/></td>
						<td><a href="#" id="acceptOffer" onclick="acceptOffer(${offer.id})" role="presentation">Prihvati ponudu</a></td>
						<td><a href="#" id="declineOffer" onclick="declineOffer(${offer.id })" role="presentation">Odbij ponudu</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<div class=" col-md-6 col-md-offset-1 centered" style="overflow: auto" id="regPonDiv" hidden=${regPonHidden }>
		<form id="regPon" role="form" focus="box-shadow" >
		  	<div class="form-group" >
			    <label>Ime:</label>
			    <input type="text" name="firstName" class="form-control" >
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text"  name="lastName" class="form-control">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email"  name="email" class="form-control">
		    </div>
		    <input type="submit" value="Dodaj" id="regPonF" onclick="regPon()" class="btn btn-default"><br><br>
		</form>
	</div>
<script>	
	function rest(){
		$.ajax({
            type: "GET",
            url: "/restMan/restSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function jelovnik(){
		$.ajax({
            type: "GET",
            url: "/restMan/jelovnikSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function karta(){
		$.ajax({
            type: "GET",
            url: "/restMan/kartaSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function stolovi(){
		$.ajax({
            type: "GET",
            url: "/restMan/stoloviSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function kuvari(){
		$.ajax({
            type: "GET",
            url: "/restMan/kuvariSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function konobari(){
		$.ajax({
            type: "GET",
            url: "/restMan/konobariSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function sankeri(){
		$.ajax({
            type: "GET",
            url: "/restMan/sankeriSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function porudzbina(){
		$.ajax({
            type: "GET",
            url: "/restMan/porudzbinaSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	
	function novaPorudzbina(){
		$.ajax({
            type: "GET",
            url: "/restMan/novaPorudzbinaSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	
	function ponude(){
		$.ajax({
            type: "GET",
            url: "/restMan/ponudeSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function regPonudjaca(){
		$.ajax({
            type: "GET",
            url: "/restMan/regPonudjacaSet/",
			contentType:"application/json",
            complete: function(){
           	 location.reload();
	         }
		});
	}
	function izvestaji(){
		$.ajax({
            type: "POST",
            url: "/reports/redirect/",
			contentType:"application/json",
			complete: function(datas){
           	 var loc = datas.responseJSON.message;
           	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
           	 window.location = loc2;
	         }
		});
	}
</script>	
</body>
</html>