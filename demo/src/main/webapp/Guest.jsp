<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<!-- Koristi sockjs biblioteku da se konektuje na /stomp endpoint -->
<script type="text/javascript" src="//cdn.jsdelivr.net/sockjs/1.0.3/sockjs.min.js">
</script>
<!-- Koristi stomp biblioteku da se pretplati na brokerov /topic/message endpoint -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
<title>Gost</title>
<style>
.pending {
    display: inline-block;
    background: red;
    text-align: center;
    color: #FFF;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    line-height: 26px;
}
</style>
<script  type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
		var stompClient = null;
		var order = null;
		var type = null;
		$(document).ready(function () {
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("otherRestaurants").hidden = true;
			document.getElementById("allpendingResv").hidden = true;
			var messageDisp = $("#pendingPopup");
			var messageList = $("#pendingFriends");
			var reservDisp = $("#pendingResrvPopup");
			var reservList = $("#allpendingResv");
			var socket = new SockJS('/stomp');
			stompClient = Stomp.over(socket);
			stompClient.connect({}, function(frame) {
				stompClient.subscribe("/topic/${user.email}", function(data) {
					var message = JSON.parse(data.body);
					messageList.empty();
					messageDisp.empty();
					messageDisp.append(message.length);
					$.each(message, function(index, pr){
						var block = "<p>" + pr.requester_name + "</p>" 
						+ "<a href=\"\" id=\"requestYes\" name=" + pr.requester+" class=\"btn btn-lg btn-success\"> Prihvati</a>"
						+ "<a href=\"\" id=\"requestNo\" name=" + pr.requester+" class=\"btn btn-lg btn-danger\"> Odbij</a>";
						messageList.append(block);
						});
					});
				stompClient.subscribe("/topic/${user.email}/pendingInvitations", function(data) {
					var message = JSON.parse(data.body);
					reservDisp.empty();
					reservList.empty();
					reservDisp.append(message.length);
					$.each(message, function(index, pr){
						var block = "<p>"  + pr.invited_by_name + " Vas je pozvao u restoran " + pr.restaurant_name + "</p>" 
						+ "<a href="+ pr.restaurant_id+" id=\"invitationYes\" name=" + pr.invitation_id +" class=\"btn btn-lg btn-success\"> Prihvati</a>"
						+ "<a href=\"\" id=\"invitationNo\" name=" + pr.invitation_id +" class=\"btn btn-lg btn-danger\"> Odbij</a>";
						reservList.append(block);
						});
					});
				getInitMessages();
				getInitReserv();
				});
			
				$('#sortOrder input').on('change', function() {
					   order = $('input[name=sortOrderChoice]:checked', '#sortOrder').val();
					   type = $('input[name=sortTypeChoice]:checked', '#sortType').val();
					   if(type != null){
						   sortingFriends(order, type, '#friendList', false);
					   }
				});
				$('#sortType input').on('change', function() {
					   type = $('input[name=sortTypeChoice]:checked', '#sortType').val();
					   sortingFriends(order, type, '#friendList', false);
				});
				$('#sortOrderPpl input').on('change', function() {
					   order = $('input[name=sortOrderChoice]:checked', '#sortOrderPpl').val();
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypePpl').val();
					   if(type != null){
						   sortingFriends(order, type, '#pplList', false);
					   }
				});
				$('#sortTypePpl input').on('change', function() {
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypePpl').val();
					   sortingFriends(order, type, '#pplList', false);
				});
				$('#sortOrderRests input').on('change', function() {
					   order = $('input[name=sortOrderChoice]:checked', '#sortOrderRests').val();
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypeRests').val();
					   if(type != null){
						   sortingFriends(order, type, '#distRestTable tbody', true);
					   }
				});
				$('#sortTypeRests input').on('change', function() {
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypeRests').val();
					   sortingFriends(order, type, '#distRestTable tbody', true);
				});
				$('#sortOrderAllRests input').on('change', function() {
					   order = $('input[name=sortOrderChoice]:checked', '#sortOrderAllRests').val();
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypeAllRests').val();
					   if(type != null){
						   sortingFriends(order, type, '#allRestTable tbody', true);
					   }
				});
				$('#sortTypeAllRests input').on('change', function() {
					   type = $('input[name=sortTypeChoice]:checked', '#sortTypeAllRests').val();
					   sortingFriends(order, type, '#allRestTable tbody', true);
				});
				
			});
		function sortingFriends(order, type, idListe, isRestaurant){
			var mylist = $(idListe);
			   var listitems = mylist.children().get();
			   var toSortList = [];
			   if(order == null){
				   order = "asc";
			   }
			   if(isRestaurant){
				   if(type === "name"){
					   $.each(listitems, function(idx, itm){
						   var nesto = listitems[idx];
						   toSortList.push($(nesto).children().eq(0).get());
					   });
				   } else if(type === "grade"){
					   $.each(listitems, function(idx, itm){
						   var nesto = listitems[idx];
						   toSortList.push($(nesto).children().eq(2).get());
					   });
				   }   
			   } else {
				   if(type === "firstName"){
					   $.each(listitems, function(idx, itm){
						   var nesto = listitems[idx];
						   toSortList.push($(nesto).children().eq(0).get());
					   });
				   } else if(type === "lastName"){
					   $.each(listitems, function(idx, itm){
						   var nesto = listitems[idx];
						   toSortList.push($(nesto).children().eq(1).get());
					   });
				   }
			   }
			   toSortList.sort(function(a, b) {
			      return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
			   });
			   mylist.empty();
			   if(order === "desc"){
				   toSortList.reverse();   
			   }
			   var result = [];
			   if(isRestaurant){
				   if(type === "name"){
					   $.each(toSortList, function(idx, itm) {
						   var naziv = itm;
						   $.each(listitems, function(idx2, itm2){
							   var nesto = listitems[idx2];
							   var txt = $(nesto).children().eq(0).get();
							   if($(naziv).text() === $(txt).text()){
								   result.push(listitems[idx2]);   
							   }
						   }); 
					   });
				   } else if (type === "grade"){
					   $.each(toSortList, function(idx, itm) {
						   var naziv = itm;
						   $.each(listitems, function(idx2, itm2){
							   var nesto = listitems[idx2];
							   var txt = $(nesto).children().eq(2).get();
							   if($(naziv).text() === $(txt).text()){
								   result.push(nesto);   
							   }
						   }); 
					   });
				   }
			   } else {
				   if(type === "firstName"){
					   $.each(toSortList, function(idx, itm) {
						   var naziv = itm;
						   $.each(listitems, function(idx2, itm2){
							   var nesto = listitems[idx2];
							   var txt = $(nesto).children().eq(0).get();
							   if($(naziv).text() === $(txt).text()){
								   result.push(listitems[idx2]);   
							   }
						   }); 
					   });
				   } else if (type === "lastName"){
					   $.each(toSortList, function(idx, itm) {
						   var naziv = itm;
						   $.each(listitems, function(idx2, itm2){
							   var nesto = listitems[idx2];
							   var txt = $(nesto).children().eq(1).get();
							   if($(naziv).text() === $(txt).text()){
								   result.push(nesto);   
							   }
						   }); 
					   });
				   }
			   }
			   $.each(result, function(idx, itm) { mylist.append(itm); });
		}
		function getInitMessages(){
			$.ajax({
	             type: "GET",
	             url: "/topic/${user.email}/init",
	             data: {"param" : "${user.email}"},
	 			contentType:"application/json"
			});
		}
		function getInitReserv(){
			$.ajax({
	             type: "GET",
	             url: "/send/${user.email}/invitation",
	             data: {"param" : "${user.email}"},
	 			contentType:"application/json"
			});
		}
		$(document).on('click','#sviRestRez',function(e){
			e.preventDefault();
			var request = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "/guestMain/startReservation/",
	             data: request,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		});
		$(document).on('click','#blizuRestRez',function(e){
			e.preventDefault();
			var request = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "/guestMain/startReservation/",
	             data: request,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		});
		$(document).on('click','#zaprati',function(e){
			e.preventDefault();
			var email = $(this).attr("name");
			var urlTo = "/send/"+email+"/friendshippending";
			$.ajax({
	             type: "GET",
	             url: urlTo,
	             data: {"sender": "${user.email}"},
	 			contentType:"application/json"
			});
		});	
		
		$(document).on('click','#requestYes',function(e){
			e.preventDefault();
			var request = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "guestMain/acceptFriend/",
	             data: request,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	location.reload();
	         }
			});
		});
		$(document).on('click','#removeFr',function(e){
			e.preventDefault();
			var friend = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "guestMain/removeFriend/",
	             data: friend,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	location.reload();
	         }
			});
		});
		$(document).on('click','#requestNo',function(e){
			e.preventDefault();
			var request = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "guestMain/denyFriend/",
	             data: request,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	location.reload();
	         }
			});
		});
		$(document).on('click','#invitationYes',function(e){
			e.preventDefault();
			var friends = [];
			friends.push($(this).attr("name"));
			friends.push($(this).attr("href"));
			var invId = JSON.stringify({
				"prePrepared" : null,
				"restaurantId" : null,
				"dishes" : null,
				"drinks" : null,
				"friends" : friends,
				"reserverId" : null,
				"tableId" : null,
				"start" : null,
				"end" : null,
				"date" : null
			});
			
			$.ajax({
	             type: "POST",
	             url: "/guestMain/acceptInvitation/",
	             data: invId,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	window.location = "friendReservation.jsp";
		         }
			});
		});
		$(document).on('click','#invitationNo',function(e){
			e.preventDefault();
			var invId = $(this).attr("name");
			$.ajax({
	             type: "POST",
	             url: "/guestMain/denyInvitation/",
	             data: invId,
	 			contentType:"application/json",
	 			complete: function(datas){
	            	location.reload();
	         }
			});
		});
		
		$(document).on('click','#obrisiHranu',function(){
			var idButton = $(this).attr("name");
			
			$.ajax({
		         type: "POST",
		         url: "/cookCont/predjinaOcenjivanje/"+idButton+"/",
		         complete: function(datas){
		        	 var loc = datas.responseJSON.message;
	            	 if(loc === "null"){
	            		 alert("Uneti korisnik ne postoji u bazi.");
	            		 location.reload();
	            	 }
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		});
		function getPeople(){
			$.ajax({
	             type: "GET",
	             url: "/guestMain/getPeople/",
	 			contentType:"application/json",
			});
		}	
		function findRestaurants(){
			$.ajax({
	             type: "GET",
	             url: "/guestMain/getRestaurants/",
	             data: {"locX": ${user.locationX},
	            	 "locY": ${user.locationY}},
	 			contentType:"application/json",
			});
		}
		function findAllTheRestaurants(){
			$.ajax({
	             type: "GET",
	             url: "/guestMain/getAllRestaurants/",
	 			contentType:"application/json"
			});
		}
		function saveGuest() {
				var $form = $("#usrInfoForm");
				var data = getFormData($form);
				var s = JSON.stringify(data);
		        $.ajax({
		             type: "POST",
		             url: "/user/changeGuest/",
		             data: s,
		             contentType : "application/json",
		             complete: function(datas){
			            	location.reload();
			         }
		         });
	      }  
		function findFriends(){
			$.ajax({
	             type: "GET",
	             url: "guestMain/getFriends/",
	 			contentType:"application/json",
			});	
		}
		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 var loc = datas.responseJSON.message;
	            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
	            	 window.location = loc2;
		         }
			});
		}
		$(function () {
		    $.validator.setDefaults({
		        errorClass: 'text-danger',
		        errorPlacement: function (error, element) {
		                error.insertAfter(element);
		        }
		 	});
		    $("#usrInfoForm").validate({
		        highlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            }
		        },
		        unhighlight: function (element) {
		            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
		        },
		        submitHandler: function(form) { saveGuest(); }
		    });
		    $('.taskNameValidation').each(function () {
		        $(this).rules('add', {
		            required: true,
		            messages: {
		                required: "Polje ne sme biti prazno"
		            }
		        });
		    });
		    $('.taskPassValidation').each(function () {
		        $(this).rules('add', {
		            required: false,
		            minlength: 5,
		            messages: {
		                minlength: "Sifra mora imati najmanje 5 slova"
		            }
		        });
		    });
		    $('.taskPassConfValidation').each(function () {
		        $(this).rules('add', {
		            required: false,
		            minlength: 5,
		            equalTo: "#password",
		            messages: {
		                equalTo: "Niste uneli istu sifru"
		            }
		        });
		    });
		});
</script>
</head>
<body>
	<body background="images/Elegant_Background-7.jpg">
	<nav class="navbar navbar-inverse">
	<div class="class="collapse navbar-collapse"">
		<ul class="nav navbar-nav ">
		<li><button type="button" class="btn btn-lg btn-primary" disabled="disabled">"${user.firstName}"</button>
		</li>
		<li><a href="#" id="profile" onclick="usrInfo()" role="presentation">Profil</a>
		</li>
		<li><div id="pendingPopup" class="pending"></div><a href="#" id="friend" onclick="friends()" role="presentation" style=" display: inline-block;">
		Lista Prijatelja</a>
		</li>
		<li><a href="#" id="lookupFr" onclick="lookupFriends()" role="presentation">Pronadji ljude</a></li>
		<li><a href="#" id="reservationHist" onclick="allResv()" role="presentation">Prosle rezervacije</a>
		</li>
		<li><div id="pendingResrvPopup" class="pending"></div><a href="#" id="pendingResv" onclick="dispPendingResv()" role="presentation" style=" display: inline-block;">
		Lista poziva za rezervaciju</a>
		</li>
		<li><a href="#" id="allRest" onclick="allRests()" role="presentation">Restorani u blizini</a>
		</li>
		<li><a href="#" id="otherRest" onclick="otherRests()" role="presentation">Lista restorana</a>
		</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
		        <li><a href="#" onclick="logOut()"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
		    </ul>
	</div>
	</nav>
	</br></br>
	<div id="allResvDiv" class=" col-md-6 col-md-offset-1 centered">
		<div >
			<table class="table">
				<tr>
					<th>ID</th>
					<th>Datum</th>
					<th>Vreme pocetka</th>
					<th>Vreme isteka</th>
					<th>Restoran</th>
				</tr>
				<c:forEach items="${pastResv}"  var="reservation">
					<tr>	
						<td><c:out value="${reservation.id}"/></td>				
						<td><c:out value="${reservation.date}"/></td>
						<td><c:out value="${reservation.start}"/></td>
						<td><c:out value="${reservation.end}"/></td>
						<td><c:out value="${reservation.restaurant}"/></td>
						<td><input type="submit" value="Oceni" id="obrisiHranu" name ="${reservation.id}" class="btn btn-default"></td>
					</tr>
				</c:forEach>
			</table>
			</div>	
	</div>
	<div id="allpendingResv" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
	
	</div>
	<div class=" col-md-6 col-md-offset-1 centered">
		<form id="usrInfoForm" role="form" focus="box-shadow">
		  	<div class="form-group">
			    <label>Ime:</label>
			    <input type="text" class="form-control taskNameValidation" name="firstName" value="${user.firstName }">
			</div>
			<div class="form-group">
		    	<label>Prezime:</label>
		    	 <input type="text" class="form-control taskNameValidation" name="lastName" value="${user.lastName }">
		    </div>
		    <div class="form-group">
		    	<label>Email:</label>
		    	 <input type="email" class="form-control taskNameValidation" name="email" value="${user.email }">
		    </div>
		    <div class="form-group">
		    	<label>Nova sifra:</label>
		    	 <input type="password" class="form-control taskPassValidation" id="password" name="password" >
		    </div>
		    <div class="form-group">
		    	<label>Ponovi unos sifre:</label>
		    	 <input type="password" class="form-control taskPassConfValidation" name="sifra2">
		    </div>
		    <input type="submit" value="Sacuvaj izmene" id="saveGuest" class="btn btn-default"><br><br>
		</form>
	</div>
	<div id="allRestaurants" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
			<div class=" col-md-6 col-md centered" style="position: absolute;left:0px">
			<form id="sortOrderRests">
				<label class="btn btn-primary">
  					<input type="radio" name="sortOrderChoice" value="asc"/> Rastuce
  				</label>
  				<br />
				<label class="btn btn-primary">
					<input type="radio" name="sortOrderChoice" value="desc" /> Opadajuce
				</label>
				<br />
			</form>
			<br/>
			<form id="sortTypeRests">
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="name" /> Ime <br />
				</label><br />
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="grade" /> Oceni <br />
				</label><br />
			</form>
		</div>
		<div id="restsTable" class="col-md-6 col-md-offset-2 centered">
			<table id="distRestTable" class="table">
				<thead>
				<tr>
					<th>Naziv Restorana</th>
					<th>Opis Restorana</th>
					<th>Ocena</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${currRests}"  var="currRest">
						<tr>	
							<td><c:out value="${currRest.name}"/></td>				
							<td><c:out value="${currRest.descr}" /></td>
							<td><c:out value="${currRest.grade}" /></td>
							<!-- onclick="makeOffer(${currOrder.id}, event)" -->
							<td><button id="blizuRestRez" name="${currRest.id}" type="button" class="btn btn-info">Napravi rezervaciju</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
	</div>
	<div id="otherRestaurants" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
			<div class=" col-md-6 col-md centered" style="position: absolute;left:0px">
			<form id="sortOrderAllRests">
				<label class="btn btn-primary">
  					<input type="radio" name="sortOrderChoice" value="asc"/> Rastuce
  				</label>
  				<br />
				<label class="btn btn-primary">
					<input type="radio" name="sortOrderChoice" value="desc" /> Opadajuce
				</label>
				<br />
			</form>
			<br/>
			<form id="sortTypeAllRests">
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="name" /> Ime <br />
				</label><br />
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="grade" /> Oceni <br />
				</label><br />
			</form>
		</div>
		<div id="allRestsTable" class="col-md-6 col-md-offset-2 centered">
			<table id="allRestTable" class="table">
				<thead>
				<tr>
					<th>Naziv Restorana</th>
					<th>Opis Restorana</th>
					<th>Ocena</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${allRests}" var="rest">
						<tr>	
							<td><c:out value="${rest.name}"/></td>				
							<td><c:out value="${rest.descr}" /></td>
							<td><c:out value="${rest.grade}" /></td>
							<!-- onclick="makeOffer(${currOrder.id}, event)" -->
							<td><button id="sviRestRez" name="${rest.id}" type="button" class="btn btn-info">Napravi rezervaciju</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
	</div>
	<div id="allFriends" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
		<div class=" col-md-6 col-md centered" style="position: absolute;left:0px">
			<form id="sortOrder">
				<label class="btn btn-primary">
  					<input type="radio" name="sortOrderChoice" value="asc"/> Rastuce
  				</label>
  				<br />
				<label class="btn btn-primary">
					<input type="radio" name="sortOrderChoice" value="desc" /> Opadajuce
				</label>
				<br />
			</form>
			<br/>
			<form id="sortType">
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="firstName" /> Ime <br />
				</label><br />
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="lastName" /> Prezime <br />
				</label><br />
			</form>
		</div>
		<div id="friendList" class=" col-md-6 col-md-offset-2 centered">
			<c:forEach items="${friends}"  var="friend">
				<div class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${friend.firstName}" /></div>
					<div><c:out value="${friend.lastName}" /></div>
					<a href="" id="removeFr" name="${friend.email}" class="btn btn-lg btn-danger">Prekini prijateljstvo</a>
				</div>
			</c:forEach>
		</div>
		<div id="pendingFriends" class=" col-md-6 col-md-offset-5 centered">
	
		</div>
	</div>
	<div id="otherPeople" class=" col-md-6 col-md-offset-1 centered" style="overflow: auto">
		<div class=" col-md-6 col-md centered" style="position: absolute;left:0px">
			<form id="sortOrderPpl">
				<label class="btn btn-primary">
  					<input type="radio" name="sortOrderChoice" value="asc"/> Rastuce
  				</label>
  				<br />
				<label class="btn btn-primary">
					<input type="radio" name="sortOrderChoice" value="desc" /> Opadajuce
				</label>
				<br />
			</form>
			<br/>
			<form id="sortTypePpl">
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="firstName" /> Ime <br />
				</label><br />
				<label class="btn btn-primary">
					<input type="radio" name="sortTypeChoice" value="lastName" /> Prezime <br />
				</label><br />
			</form>
		</div>
		<div id="pplList" class=" col-md-6 col-md-offset-2 centered">
			<c:forEach items="${people}"  var="person">
				<div class=" col-md-6 col-md-offset-1 centered">
				<div><c:out value="${person.firstName} " /></div>
				<div><c:out value=" ${person.lastName}" /></div>
				<a href="" id="zaprati" name="${person.email}" class="btn btn-lg btn-primary"> Zaprati</a>
				</div>
			</c:forEach>
		</div>
	</div>
	<script type="text/javascript">
		
		function friends(){
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = false;
			document.getElementById("otherRestaurants").hidden = true;
			document.getElementById("allpendingResv").hidden = true;
			findFriends();
		}
		function usrInfo(){
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=false;
			document.getElementById("allpendingResv").hidden = true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = true;
		}
		function allResv(){
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=false;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = true;
			document.getElementById("allpendingResv").hidden = true;
		}
		
		function allRests(){
			findRestaurants();
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allpendingResv").hidden = true;
			document.getElementById("allRestaurants").hidden=false;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = true;
		}
		function otherRests(){
			findAllTheRestaurants();
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = false;
			document.getElementById("allpendingResv").hidden = true;
		}
		function lookupFriends(){
			getPeople();
			document.getElementById("otherPeople").hidden=false;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = true;
			document.getElementById("allpendingResv").hidden = true;
		}
		function dispPendingResv(){
			document.getElementById("otherPeople").hidden=true;
			document.getElementById("usrInfoForm").hidden=true;
			document.getElementById("allResvDiv").hidden=true;
			document.getElementById("allRestaurants").hidden=true;
			document.getElementById("allFriends").hidden = true;
			document.getElementById("otherRestaurants").hidden = true;
			document.getElementById("allpendingResv").hidden = false;
		}
	</script>
</body>
</html>