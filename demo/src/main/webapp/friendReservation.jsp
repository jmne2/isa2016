<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
<!-- Koristi sockjs biblioteku da se konektuje na /stomp endpoint -->
<script type="text/javascript" src="//cdn.jsdelivr.net/sockjs/1.0.3/sockjs.min.js">
</script>
<!-- Koristi stomp biblioteku da se pretplati na brokerov /topic/message endpoint -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
<title>Rezervacija</title>
<script  type="text/javascript">
		var stompClient = null;
		var dishes = [];
		var drinks = [];
		var friends = [];
		var tableId = [];
		var timestampData;
		var chairCnt = 0;
		$(document).ready(function () {
			document.getElementById("popUpDishDiv").hidden = true;
			document.getElementById("popUpDrinkDiv").hidden = true;
			document.getElementById("dishDiv").hidden = false;
			document.getElementById("drinkDiv").hidden = true;
			document.getElementById("sendDishReservation").hidden = false;
		});
		$(document).on('click','#closePopup1',function(e){
			e.preventDefault();
			document.getElementById("popUpDishDiv").hidden = true;
		});
		$(document).on('click','#closePopup2',function(e){
			e.preventDefault();
			document.getElementById("popUpDrinkDiv").hidden = true;
		});
		$(document).on('click','#addDishes',function(e){
			e.preventDefault();
			document.getElementById("popUpDishDiv").hidden = false;
		});
		$(document).on('click','#addDrinks',function(e){
			e.preventDefault();
			document.getElementById("popUpDrinkDiv").hidden = false;
		});
		$(document).on('click','#addDishTo',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#popUpDishDiv").find("#" + idDiva);
			$(div).find("#addDishTo").remove();
			var aaa = "<a href=\"\" id=\"removeDishFromList\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Ukloni</a>";
			$(div).append(aaa);
			$("#dishList").append(div);
		});
		$(document).on('click','#addDrinkTo',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#popUpDrinkDiv").find("#" + idDiva);
			$(div).find("#addDrinkTo").remove();
			var aaa = "<a href=\"\" id=\"removeDrinkFromList\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Ukloni</a>";
			$(div).append(aaa);
			$("#drinkList").append(div);
		});
		$(document).on('click','#removeDishFromList',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#dishList").find("#" + idDiva);
			$("#dishList").find(div).remove();
			$(div).find("#removeDishFromList").remove();
			var aaa = "<a href=\"\" id=\"addDishTo\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Dodaj</a>";
			$(div).append(aaa);
			$("#popUpDishDiv").append(div);
		});
		$(document).on('click','#removeDrinkFromList',function(e){
			e.preventDefault();
			var idDiva = $(this).attr("name");
			var div = $("#drinkList").find("#" + idDiva);
			$("#drinkList").find(div).remove();
			$(div).find("#removeDrinkFromList").remove();
			var aaa = "<a href=\"\" id=\"addDrinkTo\" name="+ idDiva +" class=\"btn btn-lg btn-primary\">Dodaj</a>";
			$(div).append(aaa);
			$("#popUpDrinkDiv").append(div);
		});
		$(document).on('click','#sendDrinkReservation',function(e){
			e.preventDefault();
			var mylist = $("#drinkList");
			var listitems = mylist.children().get();
			if(listitems != null){
			 $.each(listitems, function(idx, itm){
				   var nesto = listitems[idx];
				   drinks.push($(nesto).attr("id"));
			   });
			}
			var email = "${user.email}";
			var reserv = JSON.stringify({
					"prePrepared" : null,
					"restaurantId" : null,
					"dishes" : dishes,
					"drinks" : drinks,
					"friends" : friends,
					"reserverId" : email,
					"tableId" : tableId,
					"start" : null,
					"end" : null,
					"date" : null
				});
				$.ajax({
		             type: "POST",
		             url: "/guestMain/setFriendReservation/",
		             data: reserv,
		             contentType : "application/json",
		             complete: function(datas){
		            	 var loc = datas.responseJSON.message;
		            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
		            	 window.location = loc2;
			         }
		         });
		});
		$(document).on('click','#sendDishReservation',function(e){
			e.preventDefault();
			var mylist = $("#dishList");
			var listitems = mylist.children().get();
			if(listitems != null){
			 $.each(listitems, function(idx, itm){
				   var nesto = listitems[idx];
				   dishes.push($(nesto).attr("id"));
			   });
				document.getElementById("dishDiv").hidden = true;
				document.getElementById("drinkDiv").hidden = false;
				document.getElementById("sendDishReservation").hidden = true;
				var sendDrin = "<a href=\"\" id=\"sendDrinkReservation\" class=\"btn btn-lg btn-primary\" style=\"position: absolute; bottom:0px; left:10px\">Dalje</a>";
				$("body").append(sendDrin);
			}
		});
</script>
</head>
<body>
	</br>
	<div class="container-fluid">
		<ul class="nav nav-pills">
		<li><button type="button" class="btn btn-lg btn-primary" disabled="disabled">${user.firstName} ${user.lastName}</button>
		</li>
		<li><button type="button" class="btn btn-lg btn-warning" disabled="disabled">${restaurant_name}</button>
		</li>
		</ul>
	</div>
	<div id="dishDiv" class=" col-md-6 col-md-offset-1 centered">
		<a href="" id="addDishes" class="btn btn-lg btn-primary" >Dodaj jela</a>
		<h3>Spisak jela</h3>
		<div id="dishList" class=" col-md-6 col-md-offset-2 centered">
		</div>
	</div>
	<div id="drinkDiv" class=" col-md-6 col-md-offset-1 centered">
		<a href="" id="addDrinks" class="btn btn-lg btn-primary" >Dodaj pica</a>
		<h3>Spisak pica</h3>
		<div id="drinkList" class=" col-md-6 col-md-offset-2 centered">
		</div>
	</div>
	
	<div id="popUpDishDiv" class=" col-md-6 col-md-offset-1 centered" style="z-index:100; position: absolute; left:500px;width: 300px;
    height: 420px;background-color:#FFFFFF;border:1px solid #999999;">
	    <c:forEach items="${dishes}" var="dish">
				<div id="${dish.id}" class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${dish.name}" /></div>
					<div><c:out value="${dish.price}" /></div>
					<a href="" id="addDishTo" name="${dish.id}" class="btn btn-lg btn-primary">Dodaj</a>
				</div>
		</c:forEach>
		<a href="" id="closePopup1" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; right:10px">Zavrsi</a>
	</div>
	<div id="popUpDrinkDiv" class=" col-md-6 col-md-offset-1 centered" style="z-index:100; position: absolute; left:500px;width: 300px;
    height: 420px;background-color:#FFFFFF;border:1px solid #999999;">
	    <c:forEach items="${drinks}" var="drink">
				<div id="${drink.id}" class=" col-md-6 col-md-offset-2 centered" >
					<div><c:out value="${drink.name}" /></div>
					<div><c:out value="${drink.price}" /></div>
					<a href="" id="addDrinkTo" name="${drink.id}" class="btn btn-lg btn-primary">Dodaj</a>
				</div>
		</c:forEach>
		<a href="" id="closePopup2" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; right:10px">Zavrsi</a>
	</div>
	<a href="" id="sendDishReservation" class="btn btn-lg btn-primary" style="position: absolute; bottom:0px; left:10px">Dalje</a>	
</body>
</html>